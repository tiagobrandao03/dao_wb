<?php
require_once 'header.php';
// define('PATH', realpath(__DIR__.''));
?>
<main class="white">
<section style="width:900px;margin:10px auto;">
<div class="container">
    <!-- local -->
    <div class="row" style="margin-top: 40px;">
        <div class="col s12 m10">
            <h5>ADICIONAR LOCAL</h5>
        </div>
    </div>
    <div class="row" style="margin-top: 40px;">
        <div class="col s12 m10">
    <form method="POST" action="adicionar_action.php">
        <div class="row">
            <div class="col s12 m6">
                <label >
                    LOCAL: </br>
                    <input type="text" name="nome"/>
                </label><br/><br/>
            </div>
        </div>
        <div class="row">
            <div class="col s12 m4">
                <input class="btn" type="submit" value="Adicionar"/>
            </div>
        </div>
        
    </form>
        </div>
    </div>
    <!-- circuito -->
    <div class="row" style="margin-top: 40px;">
        <div class="col s12 m10">
            <h5>ADICIONAR CIRCUITO</h5>
        </div>
    </div>
    <div class="row" style="margin-top: 40px;">
        <div class="col s12 m10">
            <form method="POST" action="adicionar_action.php">
                <div class="row">
                    <div class="col s12 m6">
                        <label>
                            CIRCUITO: </br>
                            <input type="text" name="circ_nome"/>
                        </label><br/><br/>
                    </div>
                </div>
            
                <div class="row">
                    <div class="col s12 m4">
                        <input class="btn" type="submit" value="Adicionar"/>
                    </div>
                </div>
                
            </form>
        </div>
    </div>
    <!-- area -->
    <div class="row" style="margin-top: 40px;">
        <div class="col s12 m10">
            <h5>ADICIONAR AREA</h5>
        </div>
    </div>
    <div class="row" style="margin-top: 40px;">
        <div class="col s12 m10">
            <form method="POST" action="adicionar_action.php">
                <div class="row">
                    <div class="col s12 m6">
                        <label>
                            area: </br>
                            <input type="text" name="area_nome"/>
                        </label><br/><br/>
                    </div>
                </div>
            
                <div class="row">
                    <div class="col s12 m4">
                        <input class="btn" type="submit" value="Adicionar"/>
                    </div>
                </div>
                
            </form>
        </div>
    </div>
    <!-- equipamento -->area_nome
    <div class="row" style="margin-top: 40px;">
        <div class="col s12 m10">
            <h5>ADICIONAR equipamento</h5>
        </div>
    </div>
    <div class="row" style="margin-top: 40px;">
        <div class="col s12 m10">
            <form method="POST" action="adicionar_action.php">
                <div class="row">
                    <div class="col s12 m6">
                        <label>
                            equipamento: </br>
                            <input type="text" name="equip_nome"/>
                        </label><br/><br/>
                    </div>
                </div>
            
                <div class="row">
                    <div class="col s12 m4">
                        <input class="btn" type="submit" value="Adicionar"/>
                    </div>
                </div>
                
            </form>
        </div>
    </div>
    <!-- conjunto -->
    <div class="row" style="margin-top: 40px;">
        <div class="col s12 m10">
            <h5>ADICIONAR conjunto</h5>
        </div>
    </div>
    <div class="row" style="margin-top: 40px;">
        <div class="col s12 m10">
            <form method="POST" action="adicionar_action.php">
                <div class="row">
                    <div class="col s12 m6">
                        <label>
                           conjunto: </br>
                            <input type="text" name="conj_nome"/>
                        </label><br/><br/>
                    </div>
                </div>
            
                <div class="row">
                    <div class="col s12 m4">
                        <input class="btn" type="submit" value="Adicionar"/>
                    </div>
                </div>
                
            </form>
        </div>
    </div>
    <!-- subconjunto -->
    <div class="row" style="margin-top: 40px;">
        <div class="col s12 m10">
            <h5>ADICIONAR subconjunto</h5>
        </div>
    </div>
    <div class="row" style="margin-top: 40px;">
        <div class="col s12 m10">
            <form method="POST" action="adicionar_action.php">
                <div class="row">
                    <div class="col s12 m6">
                        <label>
                            subconjunto: </br>
                            <input type="text" name="subconjunto"/>
                        </label><br/><br/>
                    </div>
                </div>
            
                <div class="row">
                    <div class="col s12 m4">
                        <input class="btn" type="submit" value="Adicionar"/>
                    </div>
                </div>
                
            </form>
        </div>
    </div>
    <!-- componente -->
    <div class="row" style="margin-top: 40px;">
        <div class="col s12 m10">
            <h5>ADICIONAR componente</h5>
        </div>
    </div>
    <div class="row" style="margin-top: 40px;">
        <div class="col s12 m10">
            <form method="POST" action="adicionar_action.php">
                <div class="row">
                    <div class="col s12 m6">
                        <label>
                            componente: </br>
                            <input type="text" name="componente"/>
                        </label><br/><br/>
                    </div>
                </div>
            
                <div class="row">
                    <div class="col s12 m4">
                        <input class="btn" type="submit" value="Adicionar"/>
                    </div>
                </div>
                
            </form>
        </div>
    </div>
</div>
</section>
</main>
<?php

include 'footer.php';
?>