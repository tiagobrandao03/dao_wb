<?php

require 'config.php';
require_once 'dao/TblocalDaoMySql.php';
    require_once 'dao/CircuitoDaoSql.php';
    require_once 'dao/AreaDaoSql.php';
    require_once 'dao/EquipamentoDaoMySql.php';

    require_once 'dao/SubconjuntoDaoMySql.php';
    require_once 'dao/ComponenteDaoMySql.php';


$tblocalDao = new TblocalDaoMySQL($pdo);
$areaDao = new AreaDaoSql($pdo);
$circuitoDao = new CircuitoDaoSql($pdo);
$equipamentoDao= new EquipamentoDaoMysql($pdo);


$nome = filter_input(INPUT_POST, 'nome');

if($nome){
    if($tblocalDao->findByNome($nome)=== false){
        $novoTblocal= new Tblocal();
        $novoTblocal->setNome($nome);

        $tblocalDao->add($novoTblocal);

        header("Location: index.php");
        exit;
    }else{
        header("Location: adicionar.php");
        exit;
    }
}else{
    header("Location: adicionar.php"); 

}
$area_nome = filter_input(INPUT_POST, 'area_nome');

if($area_nome){
    if($areaDao->findByAreaNome($area_nome)=== false){
        $novoArea= new Area();
        $novoArea->setAreaNome($area_nome);

        $areaDao->add($novoArea);

        header("Location: index.php");
        exit;
    }else{
        header("Location: adicionar.php");
        exit;
    }
}else{
    header("Location: adicionar.php"); 

}
$circ_nome = filter_input(INPUT_POST, 'circ_nome');
if($circ_nome){
        if($circuitoDao->findByCircuitoNome($circ_nome)=== false){
            $novoCircuito= new Circuito();
            $novoCircuito->setCircuitoNome($circ_nome);
    
            $circuitoDao->add($novoCircuito);
    
            // header("Location: index.php");
            // exit;
        }else{
            // header("Location: adicionar.php");
            // exit;
        }
    }else{
        // header("Location: adicionar.php"); 
        // exit;
}

$equip_nome = filter_input(INPUT_POST, 'equip_nome');

if($equip_nome){
    if($equipamentoDao->findByEquipamentoNome($equip_nome)=== false){
        $novoEquipamento= new Equipamento();
        $novoEquipamento->setEquipNome($equip_nome);

        $equipamentoDao->add($novoEquipamento);

        header("Location: index.php");
        exit;
    }else{
        header("Location: adicionar.php");
        exit;
    }
}else{
    header("Location: adicionar.php"); 
    exit;
}

$conj_nome = filter_input(INPUT_POST, 'conj_nome');

if($conj_nome){
    if($conjuntoDao->findByConjNome($conj_nome)=== false){
        $novoConjunto= new Conjunto();
        $novoConjunto->setConjNome($conj_nome);

        $conjuntoDao->add($novoConjunto);

        header("Location: index.php");
        exit;
    }else{
        header("Location: adicionar.php");
        exit;
    }
}else{
    header("Location: adicionar.php"); 
    exit;
}
$conj_nome = filter_input(INPUT_POST, 'conj_nome');

if($conj_nome){
    if($conjuntoDao->findByConjNome($conj_nome)=== false){
        $novoConjunto= new Conjunto();
        $novoConjunto->setConjNome($conj_nome);

        $conjuntoDao->add($novoConjunto);

        header("Location: index.php");
        exit;
    }else{
        header("Location: adicionar.php");
        exit;
    }
}else{
    header("Location: adicionar.php"); 
    exit;
}
$subconjunto = filter_input(INPUT_POST, 'subconjunto');

if($subconjunto){
    if($subconjuntoDao->findBySubconjunto($subconjunto)=== false){
        $novoSubconjunto= new Subconjunto();
        $novoSubconjunto->setSubconjunto($subconjunto);

        $conjuntoDao->add($novoSubconjunto);

        header("Location: index.php");
        exit;
    }else{
        header("Location: adicionar.php");
        exit;
    }
}else{
    header("Location: adicionar.php"); 
    exit;
}
$componente = filter_input(INPUT_POST, 'componente');

if($componente){
    if($componenteDao->findByComponente($componente)=== false){
        $novoComponente= new Componente();
        $novoComponente->setComponente($componente);

        $conjuntoDao->add($novoComponente);

        header("Location: index.php");
        exit;
    }else{
        header("Location: adicionar.php");
        exit;
    }
}else{
    header("Location: adicionar.php"); 
    exit;
}

