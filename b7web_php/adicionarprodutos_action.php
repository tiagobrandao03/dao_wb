<?php
require 'config.php';
require 'dao/ProdutosDaoMySql.php';

$produtosDao=new ProdutosDaoMySql($pdo);

$nome = filter_input(INPUT_POST, 'name');
$quantidade=filter_input(INPUT_POST,'quantidade');
$valor=filter_input(INPUT_POST,'valor');
$campo_categoria=filter_input(INPUT_POST,'campo_categoria');

if($nome && $quantidade && $valor && $campo_categoria){
    if($produtosDao->findByCampoCategoria($campo_categoria)=== false){
        $novoProduto= new Produtos();
        $novoProduto->setNome($nome);
        $novoProduto->setQuantidade($quantidade);
        $novoProduto->setValor($valor);
        $novoProduto->setCampoCategoria($campo_categoria);

        $produtosDao-> add($novoProduto);

        header("Location: index.php");
        exit;
    }else{
        header("Location: adicionarproduto.php");
        exit;
    }
}else{
    header("Location: adicionarproduto.php"); 
    exit;
}
