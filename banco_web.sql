/*
Navicat MySQL Data Transfer

Source Server         : webjato_con
Source Server Version : 100419
Source Host           : localhost:3306
Source Database       : banco_web

Target Server Type    : MYSQL
Target Server Version : 100419
File Encoding         : 65001

Date: 2021-07-21 15:36:41
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for area
-- ----------------------------
DROP TABLE IF EXISTS `area`;
CREATE TABLE `area` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `area` varchar(120) DEFAULT NULL,
  `id_circuito` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of area
-- ----------------------------
INSERT INTO `area` VALUES ('1', 'VIRADOR DE VAGÕES', '1');
INSERT INTO `area` VALUES ('2', 'TRANSPORTADORES DESCARGA', '1');
INSERT INTO `area` VALUES ('3', 'TRANSPORTADORES PÁTIO', '1');
INSERT INTO `area` VALUES ('4', 'TRANSPORTADORES PÁTIO', '2');
INSERT INTO `area` VALUES ('5', 'TRANSPORTADORES EMBARQUE', '2');
INSERT INTO `area` VALUES ('6', 'PÍER', '2');
INSERT INTO `area` VALUES ('7', 'MÁQUINAS DO PÁTIO', '1');
INSERT INTO `area` VALUES ('8', 'MÁQUINAS DO PÁTIO', '2');
INSERT INTO `area` VALUES ('9', 'DESSALINIZAÇÃO', '3');
INSERT INTO `area` VALUES ('10', 'DECANTAÇÃO', '3');
INSERT INTO `area` VALUES ('11', 'COMBATE A INCÊNDIO', '3');
INSERT INTO `area` VALUES ('12', 'BOMBAS PIER', '3');
INSERT INTO `area` VALUES ('13', 'AMOSTRAGEM 01', '2');
INSERT INTO `area` VALUES ('14', 'AMOSTRAGEM 02', '2');

-- ----------------------------
-- Table structure for circuito
-- ----------------------------
DROP TABLE IF EXISTS `circuito`;
CREATE TABLE `circuito` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `circuito` varchar(120) NOT NULL,
  `Id_local` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of circuito
-- ----------------------------
INSERT INTO `circuito` VALUES ('1', 'DESCARGA', '1');
INSERT INTO `circuito` VALUES ('2', 'EMBARQUE', '1');
INSERT INTO `circuito` VALUES ('3', 'TRATAMENTO DE ÁGUA', '1');

-- ----------------------------
-- Table structure for componente
-- ----------------------------
DROP TABLE IF EXISTS `componente`;
CREATE TABLE `componente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_subconjunto` int(11) DEFAULT NULL,
  `componente` varchar(120) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=493 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of componente
-- ----------------------------
INSERT INTO `componente` VALUES ('2', '1', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('3', '1', 'REDUTOR PLANETÁRIO\r\n');
INSERT INTO `componente` VALUES ('4', '2', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('5', '2', 'REDUTOR PLANETÁRIO\r\n');
INSERT INTO `componente` VALUES ('6', '3', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('7', '3', 'REDUTOR PLANETÁRIO\r\n');
INSERT INTO `componente` VALUES ('8', '4', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('9', '4', 'REDUTOR PLANETÁRIO\r\n');
INSERT INTO `componente` VALUES ('10', '5', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('11', '5', 'REDUTOR PLANETÁRIO\r\n');
INSERT INTO `componente` VALUES ('12', '6', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('13', '6', 'REDUTOR PLANETÁRIO\r\n');
INSERT INTO `componente` VALUES ('14', '7', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('15', '7', 'REDUTOR PLANETÁRIO\r\n');
INSERT INTO `componente` VALUES ('16', '8', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('17', '8', 'REDUTOR PLANETÁRIO\r\n');
INSERT INTO `componente` VALUES ('18', '9', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('19', '10', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('20', '11', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('21', '11', 'BOMBA\r\n');
INSERT INTO `componente` VALUES ('22', '12', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('23', '12', 'BOMBA\r\n');
INSERT INTO `componente` VALUES ('24', '13', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('25', '13', 'REDUTOR\r\n');
INSERT INTO `componente` VALUES ('26', '13', 'MANCAIS DO PINHÃO\r\n');
INSERT INTO `componente` VALUES ('27', '14', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('28', '14', 'REDUTOR\r\n');
INSERT INTO `componente` VALUES ('29', '14', 'MANCAIS DO PINHÃO\r\n');
INSERT INTO `componente` VALUES ('30', '15', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('31', '15', 'BOMBA HID. 4 UNIDADES\r\n');
INSERT INTO `componente` VALUES ('32', '16', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('33', '16', 'BOMBA HID. 4 UNIDADES\r\n');
INSERT INTO `componente` VALUES ('34', '17', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('35', '18', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('36', '18', 'REDUTOR\r\n');
INSERT INTO `componente` VALUES ('37', '19', 'MANCAIS RODA MOTRIZ\r\n');
INSERT INTO `componente` VALUES ('38', '20', 'MANCAIS RODA MOVIDA\r\n');
INSERT INTO `componente` VALUES ('39', '21', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('40', '21', 'REDUTOR\r\n');
INSERT INTO `componente` VALUES ('41', '22', 'MANCAIS RODA MOTRIZ\r\n');
INSERT INTO `componente` VALUES ('42', '23', 'MANCAIS RODA MOVIDA\r\n');
INSERT INTO `componente` VALUES ('43', '24', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('44', '24', 'REDUTOR\r\n');
INSERT INTO `componente` VALUES ('45', '25', 'MANCAIS RODA MOTRIZ\r\n');
INSERT INTO `componente` VALUES ('46', '26', 'MANCAIS RODA MOVIDA\r\n');
INSERT INTO `componente` VALUES ('47', '27', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('48', '27', 'REDUTOR\r\n');
INSERT INTO `componente` VALUES ('49', '28', 'MANCAIS RODA MOTRIZ\r\n');
INSERT INTO `componente` VALUES ('50', '29', 'MANCAIS RODA MOVIDA\r\n');
INSERT INTO `componente` VALUES ('51', '30', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('52', '30', 'REDUTOR\r\n');
INSERT INTO `componente` VALUES ('53', '31', 'MANCAIS DO ROLO ACIONADO\r\n');
INSERT INTO `componente` VALUES ('54', '32', 'MANCAIS DO ROLO MOVIDO\r\n');
INSERT INTO `componente` VALUES ('55', '33', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('56', '33', 'REDUTOR\r\n');
INSERT INTO `componente` VALUES ('57', '34', 'MANCAIS DO ROLO ACIONADO\r\n');
INSERT INTO `componente` VALUES ('58', '35', 'MANCAIS DO ROLO MOVIDO\r\n');
INSERT INTO `componente` VALUES ('59', '36', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('60', '36', 'REDUTOR\r\n');
INSERT INTO `componente` VALUES ('61', '37', 'TAMBOR 1 - ACIONAMENTO\r\n');
INSERT INTO `componente` VALUES ('62', '38', 'TAMBOR 2 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('63', '39', 'TAMBOR 3 - ESTICAMENTO\r\n');
INSERT INTO `componente` VALUES ('64', '38', 'TAMBOR 4 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('65', '40', 'TAMBOR 5 - RETORNO\r\n');
INSERT INTO `componente` VALUES ('66', '41', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('67', '41', 'REDUTOR\r\n');
INSERT INTO `componente` VALUES ('68', '42', 'TAMBOR 1 - ACIONAMENTO\r\n');
INSERT INTO `componente` VALUES ('69', '43', 'TAMBOR 2 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('70', '44', 'TAMBOR 3 - ESTICAMENTO\r\n');
INSERT INTO `componente` VALUES ('71', '43', 'TAMBOR 4 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('72', '45', 'TAMBOR 5 - RETORNO\r\n');
INSERT INTO `componente` VALUES ('73', '46', 'MOTOR ELÉTRICO LD - 1\r\n');
INSERT INTO `componente` VALUES ('74', '46', 'REDUTOR LD - 1\r\n');
INSERT INTO `componente` VALUES ('75', '47', 'MOTOR ELÉTRICO LE - 2\r\n');
INSERT INTO `componente` VALUES ('76', '47', 'REDUTOR LE - 2\r\n');
INSERT INTO `componente` VALUES ('77', '48', 'TAMBOR 1 - ACIONAMENTO\r\n');
INSERT INTO `componente` VALUES ('78', '49', 'TAMBOR 2 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('79', '49', 'TAMBOR 3 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('80', '50', 'TAMBOR 4 - ESTICAMENTO\r\n');
INSERT INTO `componente` VALUES ('81', '49', 'TAMBOR 5 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('82', '49', 'TAMBOR 6 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('83', '51', 'TAMBOR 7 - RETORNO\r\n');
INSERT INTO `componente` VALUES ('84', '52', 'TAMBOR 8 - DESCARGA\r\n');
INSERT INTO `componente` VALUES ('85', '49', 'TAMBOR 9 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('86', '49', 'TAMBOR 10 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('87', '53', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('88', '53', 'REDUTOR\r\n');
INSERT INTO `componente` VALUES ('89', '54', 'TAMBOR 1 - ACIONAMENTO\r\n');
INSERT INTO `componente` VALUES ('90', '55', 'TAMBOR 2 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('91', '56', 'TAMBOR 3 - ESTICAMENTO\r\n');
INSERT INTO `componente` VALUES ('92', '55', 'TAMBOR 4 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('93', '57', 'TAMBOR 5 - RETORNO\r\n');
INSERT INTO `componente` VALUES ('94', '58', 'MOTOR ELÉTRICO LD - 1\r\n');
INSERT INTO `componente` VALUES ('95', '58', 'REDUTOR LD - 1\r\n');
INSERT INTO `componente` VALUES ('96', '59', 'MOTOR ELÉTRICO LE - 2\r\n');
INSERT INTO `componente` VALUES ('97', '59', 'REDUTOR LE - 2\r\n');
INSERT INTO `componente` VALUES ('98', '60', 'TAMBOR 1 - ACIONAMENTO\r\n');
INSERT INTO `componente` VALUES ('99', '61', 'TAMBOR 2 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('100', '61', 'TAMBOR 3 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('101', '62', 'TAMBOR 4 - ESTICAMENTO\r\n');
INSERT INTO `componente` VALUES ('102', '61', 'TAMBOR 5 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('103', '61', 'TAMBOR 6 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('104', '63', 'TAMBOR 7 - RETORNO\r\n');
INSERT INTO `componente` VALUES ('105', '64', 'TAMBOR 8 - DESCARGA\r\n');
INSERT INTO `componente` VALUES ('106', '61', 'TAMBOR 9 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('107', '61', 'TAMBOR 10 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('108', '65', 'MOTOR ELÉTRICO LE - 1\r\n');
INSERT INTO `componente` VALUES ('109', '65', 'REDUTOR LE - 1\r\n');
INSERT INTO `componente` VALUES ('110', '66', 'MOTOR ELÉTRICO LD - 2\r\n');
INSERT INTO `componente` VALUES ('111', '66', 'REDUTOR LD - 2\r\n');
INSERT INTO `componente` VALUES ('112', '67', 'TAMBOR 1 - ACIONAMENTO\r\n');
INSERT INTO `componente` VALUES ('113', '68', 'TAMBOR 2 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('114', '69', 'TAMBOR 3 - ESTICAMENTO/RETORNO\r\n');
INSERT INTO `componente` VALUES ('115', '68', 'TAMBOR 4 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('116', '70', 'TAMBOR 5 - BRAKE - LD\r\n');
INSERT INTO `componente` VALUES ('117', '70', 'TAMBOR 5 - BRAKE - LE\r\n');
INSERT INTO `componente` VALUES ('118', '68', 'TAMBOR 6 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('119', '71', 'TAMBOR 7 - DESCARGA\r\n');
INSERT INTO `componente` VALUES ('120', '68', 'TAMBOR 8 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('121', '68', 'TAMBOR 9 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('122', '68', 'TAMBOR 10 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('123', '68', 'TAMBOR 11 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('124', '68', 'TAMBOR 12 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('125', '72', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('126', '72', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('127', '73', 'REDUTOR\r\n\r\n');
INSERT INTO `componente` VALUES ('128', '74', 'TAMBOR 1 - ACIONAMENTO\r\n\r\n\r\n\r\n');
INSERT INTO `componente` VALUES ('129', '75', 'TAMBOR 2 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('130', '74', 'TAMBOR 3 - ESTICAMENTO\r\n');
INSERT INTO `componente` VALUES ('131', '76', 'TAMBOR 4 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('132', '77', 'TAMBOR 5 - RETORNO\r\n');
INSERT INTO `componente` VALUES ('133', '77', 'MOTOR ELÉTRICO LE - 1\r\n');
INSERT INTO `componente` VALUES ('134', '78', 'REDUTOR LE - 1\r\n');
INSERT INTO `componente` VALUES ('135', '78', 'MOTOR ELÉTRICO LD - 2\r\n');
INSERT INTO `componente` VALUES ('136', '79', 'REDUTOR LD - 2\r\n');
INSERT INTO `componente` VALUES ('137', '80', 'TAMBOR 1 - ACIONAMENTO\r\n');
INSERT INTO `componente` VALUES ('138', '81', 'TAMBOR 2 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('139', '80', 'TAMBOR 3 - ESTICAMENTO/RETORNO\r\n');
INSERT INTO `componente` VALUES ('140', '82', 'TAMBOR 4 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('141', '82', 'TAMBOR 5 - BRAKE - LD\r\n');
INSERT INTO `componente` VALUES ('142', '80', 'TAMBOR 5 - BRAKE - LE\r\n');
INSERT INTO `componente` VALUES ('143', '83', 'TAMBOR 6 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('144', '80', 'TAMBOR 7 - DESCARGA\r\n');
INSERT INTO `componente` VALUES ('145', '80', 'TAMBOR 8 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('146', '80', 'TAMBOR 9 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('147', '80', 'TAMBOR 10 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('148', '80', 'TAMBOR 11 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('149', '84', 'TAMBOR 12 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('150', '84', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('151', '85', 'REDUTOR\r\n');
INSERT INTO `componente` VALUES ('152', '85', 'TAMBOR 1 - ACIONAMENTO LD\r\n');
INSERT INTO `componente` VALUES ('153', '86', 'TAMBOR 1 - ACIONAMENTO LE\r\n');
INSERT INTO `componente` VALUES ('154', '87', 'TAMBOR 2 - RETORNO/ESTICAMENTO\r\n');
INSERT INTO `componente` VALUES ('155', '87', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('156', '88', 'REDUTOR\r\n');
INSERT INTO `componente` VALUES ('157', '89', 'TAMBOR 1 - ACIONAMENTO\r\n');
INSERT INTO `componente` VALUES ('158', '90', 'TAMBOR 2 - RETORNO/ESTICAMENTO\r\n');
INSERT INTO `componente` VALUES ('159', '90', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('160', '91', 'REDUTOR\r\n');
INSERT INTO `componente` VALUES ('161', '92', 'TAMBOR 1 - ACIONAMENTO\r\n');
INSERT INTO `componente` VALUES ('162', '93', 'TAMBOR 2 - RETORNO/ESTICAMENTO\r\n');
INSERT INTO `componente` VALUES ('163', '93', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('164', '94', 'REDUTOR\r\n');
INSERT INTO `componente` VALUES ('165', '95', 'TAMBOR 1 - ACIONAMENTO\r\n');
INSERT INTO `componente` VALUES ('166', '96', 'TAMBOR 2 - RETORNO/ESTICAMENTO\r\n');
INSERT INTO `componente` VALUES ('167', '96', 'REDUTOR\r\n');
INSERT INTO `componente` VALUES ('168', '97', 'TAMBOR 1 - ACIONAMENTO\r\n');
INSERT INTO `componente` VALUES ('169', '98', 'TAMBOR 2 - RETORNO/ESTICAMENTO\r\n');
INSERT INTO `componente` VALUES ('170', '99', 'MOTOR ELÉTRICO 1\r\n');
INSERT INTO `componente` VALUES ('171', '99', 'REDUTOR 1\r\n');
INSERT INTO `componente` VALUES ('172', '100', 'MOTOR ELÉTRICO 2\r\n');
INSERT INTO `componente` VALUES ('173', '100', 'REDUTOR 2\r\n');
INSERT INTO `componente` VALUES ('174', '101', 'TAMBOR 1 - ACIONAMENTO 2\r\n');
INSERT INTO `componente` VALUES ('175', '102', 'TAMBOR 2 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('176', '101', 'TAMBOR 3 - ACIONAMENTO 1\r\n');
INSERT INTO `componente` VALUES ('177', '103', 'TAMBOR 4 - ESTICAMENTO\r\n');
INSERT INTO `componente` VALUES ('178', '102', 'TAMBOR 5 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('179', '102', 'TAMBOR 6 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('180', '104', 'TAMBOR 7 - RETORNO/BRAKE\r\n');
INSERT INTO `componente` VALUES ('181', '105', 'TAMBOR 8 - DESCARGA\r\n');
INSERT INTO `componente` VALUES ('182', '102', 'TAMBOR 9 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('183', '102', 'TAMBOR 10 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('184', '102', 'TAMBOR 11 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('185', '102', 'TAMBOR 12 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('186', '102', 'TAMBOR 13 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('187', '102', 'TAMBOR 14 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('188', '106', 'MOTOR ELÉTRICO 1\r\n');
INSERT INTO `componente` VALUES ('189', '106', 'REDUTOR 1\r\n');
INSERT INTO `componente` VALUES ('190', '107', 'MOTOR ELÉTRICO 2\r\n');
INSERT INTO `componente` VALUES ('191', '107', 'REDUTOR 2\r\n');
INSERT INTO `componente` VALUES ('192', '108', 'TAMBOR 1 - ACIONAMENTO 1\r\n');
INSERT INTO `componente` VALUES ('193', '109', 'TAMBOR 2 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('194', '108', 'TAMBOR 3 - ACIONAMENTO 2\r\n');
INSERT INTO `componente` VALUES ('195', '110', 'TAMBOR 4 - ESTICAMENTO\r\n');
INSERT INTO `componente` VALUES ('196', '109', 'TAMBOR 5 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('197', '109', 'TAMBOR 6 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('198', '111', 'TAMBOR 7 - RETORNO/BRAKE\r\n');
INSERT INTO `componente` VALUES ('199', '112', 'TAMBOR 8 - DESCARGA\r\n');
INSERT INTO `componente` VALUES ('200', '109', 'TAMBOR 9 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('201', '109', 'TAMBOR 10 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('202', '109', 'TAMBOR 11 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('203', '109', 'TAMBOR 12 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('204', '109', 'TAMBOR 13 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('205', '109', 'TAMBOR 13 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('206', '113', 'TAMBOR 14 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('207', '113', 'MOTOR ELÉTRICO REFRIGERAÇÃO\r\n');
INSERT INTO `componente` VALUES ('208', '113', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('209', '114', 'REDUTOR\r\n');
INSERT INTO `componente` VALUES ('210', '115', 'TAMBOR 1 - ACIONAMENTO\r\n');
INSERT INTO `componente` VALUES ('211', '116', 'TAMBOR ABRAÇAMENTO AC\r\n');
INSERT INTO `componente` VALUES ('212', '117', 'TAMBOR 2 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('213', '118', 'TAMBOR 3 - ESTICAMENTO\r\n');
INSERT INTO `componente` VALUES ('214', '119', 'TAMBOR 4 - RETORNO\r\n');
INSERT INTO `componente` VALUES ('215', '115', 'TAMBOR 5 - DESCARGA\r\n');
INSERT INTO `componente` VALUES ('216', '120', 'TAMBOR ABRAÇAMENTO DESCARGA\r\n');
INSERT INTO `componente` VALUES ('217', '116', 'ROLO VELOCIDADE - MC P213\r\n');
INSERT INTO `componente` VALUES ('218', '116', 'TAMBOR 6 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('219', '121', 'TAMBOR 7 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('220', '121', 'MOTOR ELÉTRICO REFRIGERAÇÃO\r\n');
INSERT INTO `componente` VALUES ('221', '121', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('222', '122', 'REDUTOR\r\n');
INSERT INTO `componente` VALUES ('223', '123', 'TAMBOR 1 - ACIONAMENTO\r\n');
INSERT INTO `componente` VALUES ('224', '124', 'TAMBOR ABRAÇAMENTO AC\r\n');
INSERT INTO `componente` VALUES ('225', '125', 'TAMBOR 2 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('226', '126', 'TAMBOR 3 - ESTICAMENTO\r\n');
INSERT INTO `componente` VALUES ('227', '127', 'TAMBOR 4 - RETORNO\r\n');
INSERT INTO `componente` VALUES ('228', '123', 'TAMBOR 5 - DESCARGA\r\n');
INSERT INTO `componente` VALUES ('229', '128', 'TAMBOR ABRAÇAMENTO DESCARGA\r\n');
INSERT INTO `componente` VALUES ('230', '124', 'ROLO VELOCIDADE - MC P213\r\n');
INSERT INTO `componente` VALUES ('231', '124', 'TAMBOR 6 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('232', '129', 'TAMBOR 7 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('233', '130', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('234', '131', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('235', '131', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('236', '132', 'REDUTOR PLANETÁRIO\r\n');
INSERT INTO `componente` VALUES ('237', '132', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('238', '133', 'REDUTOR PLANETÁRIO\r\n');
INSERT INTO `componente` VALUES ('239', '133', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('240', '134', 'REDUTOR PLANETÁRIO\r\n');
INSERT INTO `componente` VALUES ('241', '134', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('242', '135', 'REDUTOR\r\n');
INSERT INTO `componente` VALUES ('243', '136', 'TAMBOR 1 - ACIONAMENTO\r\n');
INSERT INTO `componente` VALUES ('244', '137', 'TAMBOR 2 - RETORNO\r\n');
INSERT INTO `componente` VALUES ('245', '137', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('246', '138', 'REDUTOR\r\n');
INSERT INTO `componente` VALUES ('247', '138', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('248', '139', 'REDUTOR\r\n');
INSERT INTO `componente` VALUES ('249', '140', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('250', '141', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('251', '141', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('252', '142', 'REDUTOR PLANETÁRIO\r\n');
INSERT INTO `componente` VALUES ('253', '142', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('254', '143', 'REDUTOR PLANETÁRIO\r\n');
INSERT INTO `componente` VALUES ('255', '143', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('256', '144', 'REDUTOR\r\n');
INSERT INTO `componente` VALUES ('257', '144', 'TAMBOR 1 - ACIONAMENTO\r\n');
INSERT INTO `componente` VALUES ('258', '145', 'TAMBOR 2 - RETORNO\r\n');
INSERT INTO `componente` VALUES ('259', '146', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('260', '147', 'REDUTOR\r\n');
INSERT INTO `componente` VALUES ('261', '147', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('262', '148', 'REDUTOR\r\n');
INSERT INTO `componente` VALUES ('263', '148', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('264', '149', 'BOMBA HIDRÁULICA\r\n');
INSERT INTO `componente` VALUES ('265', '149', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('266', '150', 'BOMBA HIDRÁULICA\r\n');
INSERT INTO `componente` VALUES ('267', '150', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('268', '151', 'BOMBA HIDRÁULICA\r\n');
INSERT INTO `componente` VALUES ('269', '151', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('270', '152', 'BOMBA HIDRÁULICA\r\n');
INSERT INTO `componente` VALUES ('271', '152', 'MOTOR HIDRÁULICO\r\n');
INSERT INTO `componente` VALUES ('272', '153', 'REDUTOR PLANETÁRIO\r\n');
INSERT INTO `componente` VALUES ('273', '153', 'MANCAL EXTERNO\r\n');
INSERT INTO `componente` VALUES ('274', '154', 'MANCAL INTERNO\r\n');
INSERT INTO `componente` VALUES ('275', '154', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('276', '155', 'REDUTOR PLANETÁRIO\r\n');
INSERT INTO `componente` VALUES ('277', '155', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('278', '156', 'REDUTOR PLANETÁRIO\r\n');
INSERT INTO `componente` VALUES ('279', '156', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('280', '157', 'REDUTOR PLANETÁRIO\r\n');
INSERT INTO `componente` VALUES ('281', '157', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('282', '158', 'REDUTOR\r\n');
INSERT INTO `componente` VALUES ('283', '158', 'REDUTOR\r\n');
INSERT INTO `componente` VALUES ('284', '159', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('285', '159', 'REDUTOR\r\n');
INSERT INTO `componente` VALUES ('286', '160', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('287', '160', 'REDUTOR\r\n');
INSERT INTO `componente` VALUES ('288', '161', 'TAMBOR 1 - ACIONAMENTO\r\n');
INSERT INTO `componente` VALUES ('289', '162', 'TAMBOR 2 - RETORNO LD\r\n');
INSERT INTO `componente` VALUES ('290', '162', 'TAMBOR 2 - RETORNO LE\r\n');
INSERT INTO `componente` VALUES ('291', '163', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('292', '164', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('293', '164', 'BOMBA HIDRÁULICA\r\n');
INSERT INTO `componente` VALUES ('294', '165', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('295', '165', 'BOMBA HIDRÁULICA\r\n');
INSERT INTO `componente` VALUES ('296', '166', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('297', '166', 'BOMBA HIDRÁULICA\r\n');
INSERT INTO `componente` VALUES ('298', '167', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('299', '167', 'MOTOR HIDRÁULICO\r\n');
INSERT INTO `componente` VALUES ('300', '168', 'REDUTOR PLANETÁRIO\r\n');
INSERT INTO `componente` VALUES ('301', '168', 'MANCAL EXTERNO\r\n');
INSERT INTO `componente` VALUES ('302', '169', 'MANCAL INTERNO\r\n');
INSERT INTO `componente` VALUES ('303', '169', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('304', '170', 'REDUTOR PLANETÁRIO\r\n');
INSERT INTO `componente` VALUES ('305', '170', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('306', '171', 'REDUTOR PLANETÁRIO\r\n');
INSERT INTO `componente` VALUES ('307', '171', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('308', '172', 'REDUTOR PLANETÁRIO\r\n');
INSERT INTO `componente` VALUES ('309', '172', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('310', '173', 'REDUTOR\r\n');
INSERT INTO `componente` VALUES ('311', '173', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('312', '174', 'REDUTOR\r\n');
INSERT INTO `componente` VALUES ('313', '174', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('314', '175', 'REDUTOR\r\n');
INSERT INTO `componente` VALUES ('315', '175', 'TAMBOR 1 - ACIONAMENTO\r\n');
INSERT INTO `componente` VALUES ('316', '176', 'TAMBOR 2 - RETORNO LD\r\n');
INSERT INTO `componente` VALUES ('317', '177', 'TAMBOR 2 - RETORNO LE\r\n');
INSERT INTO `componente` VALUES ('318', '177', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('319', '178', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('320', '179', 'REDUTOR PLANETÁRIO\r\n');
INSERT INTO `componente` VALUES ('321', '179', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('322', '180', 'REDUTOR PLANETÁRIO\r\n');
INSERT INTO `componente` VALUES ('323', '180', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('324', '181', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('325', '181', 'REDUTOR PLANETÁRIO\r\n');
INSERT INTO `componente` VALUES ('326', '182', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('327', '182', 'BOMBA HIDRÁULICA\r\n');
INSERT INTO `componente` VALUES ('328', '183', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('329', '183', 'BOMBA HIDRÁULICA\r\n');
INSERT INTO `componente` VALUES ('330', '184', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('331', '184', 'MOTOR ELÉTRICO LE - 1\r\n');
INSERT INTO `componente` VALUES ('332', '185', 'REDUTOR LE - 1\r\n');
INSERT INTO `componente` VALUES ('333', '186', 'MOTOR ELÉTRICO LD - 2\r\n');
INSERT INTO `componente` VALUES ('334', '186', 'REDUTOR LD - 2\r\n');
INSERT INTO `componente` VALUES ('335', '187', 'TAMBOR 1 - ACIONAMENTO\r\n');
INSERT INTO `componente` VALUES ('336', '187', 'TAMBOR 2 - DESCARGA/RETORNO\r\n');
INSERT INTO `componente` VALUES ('337', '188', 'TAMBOR 3 - ESTICAMENTO\r\n');
INSERT INTO `componente` VALUES ('338', '189', 'TAMBOR 4 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('339', '190', 'TAMBOR 5 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('340', '191', 'TAMBOR 6 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('341', '191', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('342', '191', 'BOMBA HIDRÁULICA\r\n');
INSERT INTO `componente` VALUES ('343', '192', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('344', '192', 'BOMBA HIDRÁULICA\r\n');
INSERT INTO `componente` VALUES ('345', '193', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('346', '193', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('347', '194', 'REDUTOR PLANETÁRIO\r\n');
INSERT INTO `componente` VALUES ('348', '195', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('349', '195', 'REDUTOR PLANETÁRIO\r\n');
INSERT INTO `componente` VALUES ('350', '196', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('351', '196', 'REDUTOR PLANETÁRIO\r\n');
INSERT INTO `componente` VALUES ('352', '197', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('353', '197', 'REDUTOR PLANETÁRIO\r\n');
INSERT INTO `componente` VALUES ('354', '198', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('355', '198', 'REDUTOR PLANETÁRIO\r\n');
INSERT INTO `componente` VALUES ('356', '199', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('357', '199', 'REDUTOR PLANETÁRIO\r\n');
INSERT INTO `componente` VALUES ('358', '200', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('359', '200', 'REDUTOR PLANETÁRIO\r\n');
INSERT INTO `componente` VALUES ('360', '201', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('361', '201', 'REDUTOR PLANETÁRIO\r\n');
INSERT INTO `componente` VALUES ('362', '202', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('363', '202', 'BOMBA HIDRÁULICA\r\n');
INSERT INTO `componente` VALUES ('364', '203', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('365', '203', 'BOMBA HIDRÁULICA\r\n');
INSERT INTO `componente` VALUES ('366', '204', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('367', '204', 'MOTOR ELÉTRICO LE\r\n');
INSERT INTO `componente` VALUES ('368', '205', 'REDUTOR LE\r\n');
INSERT INTO `componente` VALUES ('369', '206', 'MOTOR ELÉTRICO LD\r\n');
INSERT INTO `componente` VALUES ('370', '206', 'MOTOR ELÉTRICO LD\r\n');
INSERT INTO `componente` VALUES ('371', '207', 'REDUTOR LD\r\n');
INSERT INTO `componente` VALUES ('372', '207', 'TAMBOR 1 - ACIONAMENTO\r\n');
INSERT INTO `componente` VALUES ('373', '208', 'TAMBOR 2 - DESCARGA/RETORNO\r\n');
INSERT INTO `componente` VALUES ('374', '209', 'TAMBOR 3 - ESTICAMENTO\r\n');
INSERT INTO `componente` VALUES ('375', '210', 'TAMBOR 4 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('376', '211', 'TAMBOR 5 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('377', '211', 'TAMBOR 6 - DESVIO\r\n');
INSERT INTO `componente` VALUES ('378', '211', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('379', '212', 'BOMBA HIDRÁULICA\r\n');
INSERT INTO `componente` VALUES ('380', '212', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('381', '213', 'BOMBA HIDRÁULICA\r\n');
INSERT INTO `componente` VALUES ('382', '213', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('383', '214', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('384', '215', 'REDUTOR PLANETÁRIO\r\n');
INSERT INTO `componente` VALUES ('385', '215', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('386', '216', 'REDUTOR PLANETÁRIO\r\n');
INSERT INTO `componente` VALUES ('387', '216', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('388', '217', 'REDUTOR PLANETÁRIO\r\n');
INSERT INTO `componente` VALUES ('389', '217', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('390', '218', 'REDUTOR PLANETÁRIO\r\n');
INSERT INTO `componente` VALUES ('391', '218', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('392', '219', 'REDUTOR\r\n');
INSERT INTO `componente` VALUES ('393', '219', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('394', '220', 'REDUTOR\r\n');
INSERT INTO `componente` VALUES ('395', '220', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('396', '221', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('397', '221', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('398', '222', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('399', '223', 'BOMBA\r\n');
INSERT INTO `componente` VALUES ('400', '224', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('401', '224', 'BOMBA\r\n');
INSERT INTO `componente` VALUES ('402', '225', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('403', '225', 'BOMBA\r\n');
INSERT INTO `componente` VALUES ('404', '226', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('405', '226', 'BOMBA\r\n');
INSERT INTO `componente` VALUES ('406', '227', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('407', '227', 'BOMBA\r\n');
INSERT INTO `componente` VALUES ('408', '228', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('409', '228', 'BOMBA\r\n');
INSERT INTO `componente` VALUES ('410', '229', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('411', '229', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('412', '230', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('413', '231', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('414', '232', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('415', '233', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('416', '234', 'BOMBA\r\n');
INSERT INTO `componente` VALUES ('417', '235', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('418', '235', 'BOMBA\r\n');
INSERT INTO `componente` VALUES ('419', '236', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('420', '236', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('421', '237', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('422', '238', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('423', '239', 'BOMBA\r\n');
INSERT INTO `componente` VALUES ('424', '240', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('425', '240', 'BOMBA\r\n');
INSERT INTO `componente` VALUES ('426', '241', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('427', '241', 'BOMBA\r\n');
INSERT INTO `componente` VALUES ('428', '242', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('429', '242', 'BOMBA\r\n');
INSERT INTO `componente` VALUES ('430', '243', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('431', '243', 'BOMBA\r\n');
INSERT INTO `componente` VALUES ('432', '244', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('433', '244', 'MOTOR DIESEL\r\n');
INSERT INTO `componente` VALUES ('434', '245', 'BOMBA\r\n');
INSERT INTO `componente` VALUES ('435', '246', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('436', '246', 'BOMBA\r\n');
INSERT INTO `componente` VALUES ('437', '247', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('438', '247', 'BOMBA\r\n');
INSERT INTO `componente` VALUES ('439', '248', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('440', '248', 'BOMBA\r\n');
INSERT INTO `componente` VALUES ('441', '249', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('442', '249', 'BOMBA\r\n');
INSERT INTO `componente` VALUES ('443', '250', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('444', '250', 'BOMBA\r\n');
INSERT INTO `componente` VALUES ('445', '251', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('446', '251', 'BOMBA\r\n');
INSERT INTO `componente` VALUES ('447', '252', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('448', '252', 'BOMBA\r\n');
INSERT INTO `componente` VALUES ('449', '253', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('450', '253', 'BOMBA\r\n');
INSERT INTO `componente` VALUES ('451', '254', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('452', '254', 'BOMBA\r\n');
INSERT INTO `componente` VALUES ('453', '255', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('454', '255', 'BOMBA\r\n');
INSERT INTO `componente` VALUES ('455', '256', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('456', '256', 'REDUTOR\r\n');
INSERT INTO `componente` VALUES ('457', '257', 'TAMBOR 1 - ACIONAMENTO\r\n');
INSERT INTO `componente` VALUES ('458', '257', 'TAMBOR 2\r\n');
INSERT INTO `componente` VALUES ('459', '258', 'TAMBOR 3\r\n');
INSERT INTO `componente` VALUES ('460', '259', 'TAMBOR 4\r\n');
INSERT INTO `componente` VALUES ('461', '259', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('462', '259', 'REDUTOR\r\n');
INSERT INTO `componente` VALUES ('463', '260', 'TAMBOR 1 - ACIONAMENTO\r\n');
INSERT INTO `componente` VALUES ('464', '260', 'TAMBOR 2\r\n');
INSERT INTO `componente` VALUES ('465', '261', 'TAMBOR 3\r\n');
INSERT INTO `componente` VALUES ('466', '262', 'TAMBOR 4\r\n');
INSERT INTO `componente` VALUES ('467', '262', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('468', '262', 'REDUTOR\r\n');
INSERT INTO `componente` VALUES ('469', '263', 'TAMBOR 1 - ACIONAMENTO\r\n');
INSERT INTO `componente` VALUES ('470', '263', 'TAMBOR 2\r\n');
INSERT INTO `componente` VALUES ('471', '264', 'TAMBOR 3\r\n');
INSERT INTO `componente` VALUES ('472', '265', 'TAMBOR 4\r\n');
INSERT INTO `componente` VALUES ('473', '265', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('474', '265', 'REDUTOR\r\n');
INSERT INTO `componente` VALUES ('475', '266', 'TAMBOR 1 - ACIONAMENTO\r\n');
INSERT INTO `componente` VALUES ('476', '266', 'TAMBOR 2 - RETORNO\r\n');
INSERT INTO `componente` VALUES ('477', '267', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('478', '268', 'REDUTOR\r\n');
INSERT INTO `componente` VALUES ('479', '269', 'TAMBOR 1 - ACIONAMENTO\r\n');
INSERT INTO `componente` VALUES ('480', '269', 'TAMBOR 2 - RETORNO\r\n');
INSERT INTO `componente` VALUES ('481', '270', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('482', '271', 'MANCAIS DO BRITADOR\r\n');
INSERT INTO `componente` VALUES ('483', '272', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('484', '273', 'REDUTOR\r\n');
INSERT INTO `componente` VALUES ('485', '274', 'TAMBOR 1 - ACIONAMENTO\r\n');
INSERT INTO `componente` VALUES ('486', '274', 'TAMBOR 2 - RETORNO\r\n');
INSERT INTO `componente` VALUES ('487', '275', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('488', '276', 'REDUTOR\r\n');
INSERT INTO `componente` VALUES ('489', '277', 'TAMBOR 1 - ACIONAMENTO\r\n');
INSERT INTO `componente` VALUES ('490', '277', 'TAMBOR 2 - RETORNO\r\n');
INSERT INTO `componente` VALUES ('491', '278', 'MOTOR ELÉTRICO\r\n');
INSERT INTO `componente` VALUES ('492', '279', 'MANCAIS DO BRITADOR\r\n');

-- ----------------------------
-- Table structure for conjunto
-- ----------------------------
DROP TABLE IF EXISTS `conjunto`;
CREATE TABLE `conjunto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `conjunto` varchar(120) NOT NULL,
  `id_equipamento` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=111 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of conjunto
-- ----------------------------
INSERT INTO `conjunto` VALUES ('1', 'CARRO POSICIONADOR\r\n', '1');
INSERT INTO `conjunto` VALUES ('2', 'SISTEMA DE ASPERSÃO\r\n', '1');
INSERT INTO `conjunto` VALUES ('3', 'BARRIL\r\n', '1');
INSERT INTO `conjunto` VALUES ('4', 'GRAMPOS DO BARRIL\r\n', '1');
INSERT INTO `conjunto` VALUES ('5', 'FREIO DE SAÍDA\r\n', '1');
INSERT INTO `conjunto` VALUES ('6', 'ALIMENTADOR DE SAPATA - 01\r\n', '1');
INSERT INTO `conjunto` VALUES ('7', 'ALIMENTADOR DE SAPATA - 02\r\n', '1');
INSERT INTO `conjunto` VALUES ('8', 'TRANSPORTADOR DE FINOS - 01\r\n', '1');
INSERT INTO `conjunto` VALUES ('9', 'TRANSPORTADOR DE FINOS - 02\r\n', '1');
INSERT INTO `conjunto` VALUES ('10', 'TRANSPORTADOR\r\n', '2');
INSERT INTO `conjunto` VALUES ('11', 'TRANSPORTADOR\r\n', '3');
INSERT INTO `conjunto` VALUES ('12', 'TRANSPORTADOR\r\n', '4');
INSERT INTO `conjunto` VALUES ('13', 'TRANSPORTADOR\r\n', '5');
INSERT INTO `conjunto` VALUES ('14', 'TRANSPORTADOR\r\n', '6');
INSERT INTO `conjunto` VALUES ('15', 'TRANSPORTADOR\r\n', '7');
INSERT INTO `conjunto` VALUES ('16', 'TRANSPORTADOR\r\n', '8');
INSERT INTO `conjunto` VALUES ('17', 'TRANSPORTADOR\r\n', '9');
INSERT INTO `conjunto` VALUES ('18', 'TRANSPORTADOR\r\n', '10');
INSERT INTO `conjunto` VALUES ('19', 'TRANSPORTADOR\r\n', '11');
INSERT INTO `conjunto` VALUES ('20', 'TRANSPORTADOR\r\n', '12');
INSERT INTO `conjunto` VALUES ('21', 'TRANSPORTADOR\r\n', '13');
INSERT INTO `conjunto` VALUES ('22', 'TRANSPORTADOR\r\n', '14');
INSERT INTO `conjunto` VALUES ('23', 'TRANSPORTADOR\r\n', '15');
INSERT INTO `conjunto` VALUES ('24', 'TRANSPORTADOR\r\n', '16');
INSERT INTO `conjunto` VALUES ('25', 'ELEVAÇÃO DA LANÇA\r\n', '17');
INSERT INTO `conjunto` VALUES ('26', 'GIRO\r\n', '18');
INSERT INTO `conjunto` VALUES ('27', 'TRANSPORTADOR DA LANÇA\r\n', '19');
INSERT INTO `conjunto` VALUES ('28', 'MANGUEIRA DE ÁGUA\r\n', '19');
INSERT INTO `conjunto` VALUES ('29', 'MANGUERA ELÉTRICA\r\n', '19');
INSERT INTO `conjunto` VALUES ('30', 'ELEVAÇÃO DA LANÇA\r\n', '19');
INSERT INTO `conjunto` VALUES ('31', 'GIRO\r\n', '19');
INSERT INTO `conjunto` VALUES ('32', 'TRANSPORTADOR DA LANÇA\r\n', '20');
INSERT INTO `conjunto` VALUES ('33', 'TRANSPORTADOR DA LANÇA\r\n', '20');
INSERT INTO `conjunto` VALUES ('34', 'MANGUEIRA DE ÁGUA\r\n', '20');
INSERT INTO `conjunto` VALUES ('35', 'MANGUERA ELÉTRICA\r\n', '20');
INSERT INTO `conjunto` VALUES ('36', 'ELEVAÇÃO DA LANÇA\r\n', '20');
INSERT INTO `conjunto` VALUES ('37', 'RODA DE CAÇAMBAS\r\n', '21');
INSERT INTO `conjunto` VALUES ('38', 'GIRO\r\n', '21');
INSERT INTO `conjunto` VALUES ('39', 'MANGUEIRA DE ÁGUA\r\n', '21');
INSERT INTO `conjunto` VALUES ('40', 'MANGUERA ELÉTRICA\r\n', '21');
INSERT INTO `conjunto` VALUES ('41', 'TRANSPORTADOR DA LANÇA\r\n', '21');
INSERT INTO `conjunto` VALUES ('42', 'TROCADOR DE CALOR\r\n', '21');
INSERT INTO `conjunto` VALUES ('43', 'TROCADOR DE CALOR\r\n', '21');
INSERT INTO `conjunto` VALUES ('44', 'ELEVAÇÃO DA LANÇA\r\n', '22');
INSERT INTO `conjunto` VALUES ('45', 'RODA DE CAÇAMBAS\r\n', '22');
INSERT INTO `conjunto` VALUES ('46', 'GIRO\r\n', '22');
INSERT INTO `conjunto` VALUES ('47', 'MANGUEIRA DE ÁGUA\r\n', '22');
INSERT INTO `conjunto` VALUES ('48', 'MANGUERA ELÉTRICA\r\n', '22');
INSERT INTO `conjunto` VALUES ('49', 'TRANSPORTADOR DA LANÇA\r\n', '22');
INSERT INTO `conjunto` VALUES ('50', 'TROCADOR DE CALOR\r\n', '22');
INSERT INTO `conjunto` VALUES ('51', 'GIRO\r\n', '23');
INSERT INTO `conjunto` VALUES ('52', 'ELEVAÇÃO DA LANÇA\r\n', '23');
INSERT INTO `conjunto` VALUES ('53', 'TRANSPORTADOR DA LANÇA\r\n', '23');
INSERT INTO `conjunto` VALUES ('54', 'SISTEMA DE ASPERSÃO\r\n', '23');
INSERT INTO `conjunto` VALUES ('55', 'LANÇA\r\n', '23');
INSERT INTO `conjunto` VALUES ('56', 'GIRO\r\n', '24');
INSERT INTO `conjunto` VALUES ('57', 'ELEVAÇÃO DA LANÇA\r\n', '24');
INSERT INTO `conjunto` VALUES ('58', 'TRANSPORTADOR DA LANÇA\r\n', '24');
INSERT INTO `conjunto` VALUES ('59', 'SISTEMA DE ASPERSÃO\r\n', '24');
INSERT INTO `conjunto` VALUES ('60', 'LANÇA\r\n', '24');
INSERT INTO `conjunto` VALUES ('61', 'AGITADOR\r\n', '25');
INSERT INTO `conjunto` VALUES ('62', 'AGITADOR\r\n', '26');
INSERT INTO `conjunto` VALUES ('63', 'AGITADOR\r\n', '27');
INSERT INTO `conjunto` VALUES ('64', 'BOMBA DE ÁGUA\r\n', '28');
INSERT INTO `conjunto` VALUES ('65', 'BOMBA DE ÁGUA\r\n', '29');
INSERT INTO `conjunto` VALUES ('66', 'BOMBA DE ÁGUA\r\n', '30');
INSERT INTO `conjunto` VALUES ('67', 'BOMBA DE ÁGUA\r\n', '31');
INSERT INTO `conjunto` VALUES ('68', 'BOMBA DE ÁGUA\r\n', '32');
INSERT INTO `conjunto` VALUES ('69', 'BOMBA DE ÁGUA\r\n', '33');
INSERT INTO `conjunto` VALUES ('70', 'BOMBA DE ÁGUA\r\n', '34');
INSERT INTO `conjunto` VALUES ('71', 'BOMBA DE ÁGUA\r\n', '35');
INSERT INTO `conjunto` VALUES ('72', 'BOMBA DE ÁGUA\r\n', '36');
INSERT INTO `conjunto` VALUES ('73', 'BOMBA DE ÁGUA\r\n', '37');
INSERT INTO `conjunto` VALUES ('74', 'BOMBA DE ÁGUA\r\n', '38');
INSERT INTO `conjunto` VALUES ('75', 'BOMBA DE ÁGUA\r\n', '39');
INSERT INTO `conjunto` VALUES ('76', 'BOMBA DE ÁGUA\r\n', '40');
INSERT INTO `conjunto` VALUES ('77', 'BOMBA DE ÁGUA\r\n', '41');
INSERT INTO `conjunto` VALUES ('78', 'BOMBA DE ÁGUA\r\n', '42');
INSERT INTO `conjunto` VALUES ('79', 'BOMBA DE ÁGUA\r\n', '43');
INSERT INTO `conjunto` VALUES ('80', 'BOMBA DE ÁGUA\r\n', '44');
INSERT INTO `conjunto` VALUES ('81', 'BOMBA DE ÁGUA\r\n', '45');
INSERT INTO `conjunto` VALUES ('82', 'BOMBA DE ÁGUA\r\n', '46');
INSERT INTO `conjunto` VALUES ('83', 'BOMBA DE ÁGUA\r\n', '47');
INSERT INTO `conjunto` VALUES ('84', 'BOMBA DE ÁGUA\r\n', '48');
INSERT INTO `conjunto` VALUES ('85', 'BOMBA DE ÁGUA\r\n', '49');
INSERT INTO `conjunto` VALUES ('86', 'BOMBA DE ÁGUA\r\n', '50');
INSERT INTO `conjunto` VALUES ('87', 'BOMBA DE ÁGUA\r\n', '51');
INSERT INTO `conjunto` VALUES ('88', 'BOMBA DE ÁGUA\r\n', '52');
INSERT INTO `conjunto` VALUES ('89', 'BOMBA DE ÁGUA\r\n', '53');
INSERT INTO `conjunto` VALUES ('90', 'BOMBA DE ÁGUA\r\n', '54');
INSERT INTO `conjunto` VALUES ('91', 'BOMBA DE ÁGUA\r\n', '55');
INSERT INTO `conjunto` VALUES ('92', 'BOMBA DE ÁGUA\r\n', '56');
INSERT INTO `conjunto` VALUES ('93', 'BOMBA DE ÁGUA\r\n', '57');
INSERT INTO `conjunto` VALUES ('94', 'BOMBA DE ÁGUA\r\n', '58');
INSERT INTO `conjunto` VALUES ('95', 'BOMBA DE ÁGUA\r\n', '59');
INSERT INTO `conjunto` VALUES ('96', 'BOMBA DE ÁGUA\r\n', '60');
INSERT INTO `conjunto` VALUES ('97', 'BOMBA DE ÁGUA\r\n', '61');
INSERT INTO `conjunto` VALUES ('98', 'BOMBA DE ÁGUA\r\n', '62');
INSERT INTO `conjunto` VALUES ('102', 'EXTRATOR DE SUCATA\r\n', '2');
INSERT INTO `conjunto` VALUES ('103', 'EXTRATOR DE SUCATA\r\n', '7');
INSERT INTO `conjunto` VALUES ('104', 'EXTRATOR DE SUCATA\r\n', '9');
INSERT INTO `conjunto` VALUES ('105', 'TRANSPORTADOR\r\n', '63');
INSERT INTO `conjunto` VALUES ('106', 'TRANSPORTADOR\r\n', '64');
INSERT INTO `conjunto` VALUES ('107', 'BRITADOR\r\n', '65');
INSERT INTO `conjunto` VALUES ('108', 'TRANSPORTADOR\r\n', '66');
INSERT INTO `conjunto` VALUES ('109', 'TRANSPORTADOR\r\n', '67');
INSERT INTO `conjunto` VALUES ('110', 'BRITADOR\r\n', '68');

-- ----------------------------
-- Table structure for equipamento
-- ----------------------------
DROP TABLE IF EXISTS `equipamento`;
CREATE TABLE `equipamento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `equipamento` varchar(120) DEFAULT NULL,
  `id_area` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of equipamento
-- ----------------------------
INSERT INTO `equipamento` VALUES ('1', 'VV-3120NA-01\r\n', '1');
INSERT INTO `equipamento` VALUES ('2', 'TR-3140NA-01\r\n', '2');
INSERT INTO `equipamento` VALUES ('3', 'TR-3140NA-02\r\n', '2');
INSERT INTO `equipamento` VALUES ('4', 'TR-3140NA-03\r\n', '3');
INSERT INTO `equipamento` VALUES ('5', 'TR-3140NA-04\r\n', '2');
INSERT INTO `equipamento` VALUES ('6', 'TR-3140NA-05\r\n', '3');
INSERT INTO `equipamento` VALUES ('7', 'TR-3140NA-06\r\n', '4');
INSERT INTO `equipamento` VALUES ('8', 'TR-3140NA-07\r\n', '5');
INSERT INTO `equipamento` VALUES ('9', 'TR-3140NA-08\r\n', '4');
INSERT INTO `equipamento` VALUES ('10', 'TR-3140NA-09\r\n', '2');
INSERT INTO `equipamento` VALUES ('11', 'TR-3140NA-10\r\n', '5');
INSERT INTO `equipamento` VALUES ('12', 'TR-3140NA-11\r\n', '5');
INSERT INTO `equipamento` VALUES ('13', 'TR-3140NA-12\r\n', '5');
INSERT INTO `equipamento` VALUES ('14', 'TR-3220NA-01\r\n', '5');
INSERT INTO `equipamento` VALUES ('15', 'TR-3220NA-02\r\n', '6');
INSERT INTO `equipamento` VALUES ('16', 'TR-3220NA-03\r\n', '6');
INSERT INTO `equipamento` VALUES ('17', 'BOMBA DE ALTA PRESSÃO 01\r\n', '6');
INSERT INTO `equipamento` VALUES ('18', 'BOMBA DE ALTA PRESSÃO 02\r\n', '6');
INSERT INTO `equipamento` VALUES ('19', 'BOMBA BACKWASH 01\r\n', '7');
INSERT INTO `equipamento` VALUES ('20', 'BOMBA BACKWASH 02\r\n', '7');
INSERT INTO `equipamento` VALUES ('21', 'BOMBA BACKWASH 03\r\n', '8');
INSERT INTO `equipamento` VALUES ('22', 'BOMBA BACKWASH 04\r\n', '8');
INSERT INTO `equipamento` VALUES ('23', 'BOMBA BOOSTER 01\r\n', '6');
INSERT INTO `equipamento` VALUES ('24', 'BB 01 - ÁGUA FILTRADA\r\n', '6');
INSERT INTO `equipamento` VALUES ('25', 'BB 02 - ÁGUA FILTRADA\r\n', '9');
INSERT INTO `equipamento` VALUES ('26', 'BB 03 - ÁGUA FILTRADA\r\n', '9');
INSERT INTO `equipamento` VALUES ('27', 'FLUSH PUMP\r\n', '9');
INSERT INTO `equipamento` VALUES ('28', 'CIP PUMP\r\n', '9');
INSERT INTO `equipamento` VALUES ('29', 'BA-3180NA-01\r\n', '9');
INSERT INTO `equipamento` VALUES ('30', 'BA-3180NA-02\r\n', '9');
INSERT INTO `equipamento` VALUES ('31', 'BB 01 - ÁGUA TRATADA\r\n', '9');
INSERT INTO `equipamento` VALUES ('32', 'BB 02 - ÁGUA TRATADA\r\n', '9');
INSERT INTO `equipamento` VALUES ('33', 'BB 03 - ÁGUA TRATADA\r\n', '9');
INSERT INTO `equipamento` VALUES ('34', 'BOMBA 007\r\n', '9');
INSERT INTO `equipamento` VALUES ('35', 'BOMBA 008\r\n', '9');
INSERT INTO `equipamento` VALUES ('36', 'BOMBA 009\r\n', '9');
INSERT INTO `equipamento` VALUES ('37', 'BOMBA 010\r\n', '9');
INSERT INTO `equipamento` VALUES ('38', 'BOMBA 01\r\n', '9');
INSERT INTO `equipamento` VALUES ('39', 'BOMBA DE COMPENSAÇÃO\r\n', '9');
INSERT INTO `equipamento` VALUES ('40', 'BOMBA DIESEL\r\n', '9');
INSERT INTO `equipamento` VALUES ('41', 'BA-3180NA-09\r\n', '9');
INSERT INTO `equipamento` VALUES ('42', 'BA-3180NA-10\r\n', '9');
INSERT INTO `equipamento` VALUES ('43', 'BA-3180NA-11\r\n', '9');
INSERT INTO `equipamento` VALUES ('44', 'BA-3180NA-12\r\n', '9');
INSERT INTO `equipamento` VALUES ('45', 'BA-3210NA-01\r\n', '9');
INSERT INTO `equipamento` VALUES ('46', 'BA-3210NA-02\r\n', '10');
INSERT INTO `equipamento` VALUES ('47', 'BA-3210NA-03\r\n', '10');
INSERT INTO `equipamento` VALUES ('48', 'BA-3210NA-04\r\n', '10');
INSERT INTO `equipamento` VALUES ('49', 'BA-3210NA-05\r\n', '10');
INSERT INTO `equipamento` VALUES ('50', 'BA-3210NA-06\r\n', '11');
INSERT INTO `equipamento` VALUES ('51', 'TR-3145NA-01\r\n', '11');
INSERT INTO `equipamento` VALUES ('52', 'TR-3145NA-03\r\n', '11');
INSERT INTO `equipamento` VALUES ('53', 'BR-3145NA-01\r\n', '10');
INSERT INTO `equipamento` VALUES ('54', 'TR-3145NA-02\r\n', '10');
INSERT INTO `equipamento` VALUES ('55', 'TR-3145NA-04\r\n', '10');
INSERT INTO `equipamento` VALUES ('56', 'BR-3145NA-02\r\n', '10');

-- ----------------------------
-- Table structure for local
-- ----------------------------
DROP TABLE IF EXISTS `local`;
CREATE TABLE `local` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `local` varchar(120) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of local
-- ----------------------------
INSERT INTO `local` VALUES ('1', 'PORTO\r\n');

-- ----------------------------
-- Table structure for subconjunto
-- ----------------------------
DROP TABLE IF EXISTS `subconjunto`;
CREATE TABLE `subconjunto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subconjunto` varchar(120) DEFAULT NULL,
  `conjunto` varchar(120) DEFAULT NULL,
  `circuito` varchar(120) DEFAULT NULL,
  `area` varchar(120) DEFAULT NULL,
  `equipamento` varchar(120) DEFAULT NULL,
  `id_conjunto` int(11) DEFAULT NULL,
  `circuito_area_equipamento_conjunto_subconjunto` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=283 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of subconjunto
-- ----------------------------
INSERT INTO `subconjunto` VALUES ('1', 'TRANSLAÇÃO - ACIONAMENTO - 1\r\n', 'CARRO POSICIONADOR', 'DESCARGA', 'VIRADOR DE VAGÕES', 'VV-3120NA-01', '1', 'DESCARGAVIRADOR DE VAGÕESVV-3120NA-01CARRO POSICIONADORTRANSLAÇÃO - ACIONAMENTO - 1');
INSERT INTO `subconjunto` VALUES ('2', 'TRANSLAÇÃO - ACIONAMENTO - 2\r\n', 'CARRO POSICIONADOR', 'DESCARGA', 'VIRADOR DE VAGÕES', 'VV-3120NA-01', '1', 'DESCARGAVIRADOR DE VAGÕESVV-3120NA-01CARRO POSICIONADORTRANSLAÇÃO - ACIONAMENTO - 2');
INSERT INTO `subconjunto` VALUES ('3', 'TRANSLAÇÃO - ACIONAMENTO - 3\r\n', 'CARRO POSICIONADOR', 'DESCARGA', 'VIRADOR DE VAGÕES', 'VV-3120NA-01', '1', 'DESCARGAVIRADOR DE VAGÕESVV-3120NA-01CARRO POSICIONADORTRANSLAÇÃO - ACIONAMENTO - 3');
INSERT INTO `subconjunto` VALUES ('4', 'TRANSLAÇÃO - ACIONAMENTO - 4\r\n', 'CARRO POSICIONADOR', 'DESCARGA', 'VIRADOR DE VAGÕES', 'VV-3120NA-01', '1', 'DESCARGAVIRADOR DE VAGÕESVV-3120NA-01CARRO POSICIONADORTRANSLAÇÃO - ACIONAMENTO - 4');
INSERT INTO `subconjunto` VALUES ('5', 'TRANSLAÇÃO - ACIONAMENTO - 5\r\n', 'CARRO POSICIONADOR', 'DESCARGA', 'VIRADOR DE VAGÕES', 'VV-3120NA-01', '1', 'DESCARGAVIRADOR DE VAGÕESVV-3120NA-01CARRO POSICIONADORTRANSLAÇÃO - ACIONAMENTO - 5');
INSERT INTO `subconjunto` VALUES ('6', 'TRANSLAÇÃO - ACIONAMENTO - 6\r\n', 'CARRO POSICIONADOR', 'DESCARGA', 'VIRADOR DE VAGÕES', 'VV-3120NA-01', '1', 'DESCARGAVIRADOR DE VAGÕESVV-3120NA-01CARRO POSICIONADORTRANSLAÇÃO - ACIONAMENTO - 6');
INSERT INTO `subconjunto` VALUES ('7', 'TRANSLAÇÃO - ACIONAMENTO - 7\r\n', 'CARRO POSICIONADOR', 'DESCARGA', 'VIRADOR DE VAGÕES', 'VV-3120NA-01', '1', 'DESCARGAVIRADOR DE VAGÕESVV-3120NA-01CARRO POSICIONADORTRANSLAÇÃO - ACIONAMENTO - 7');
INSERT INTO `subconjunto` VALUES ('8', 'TRANSLAÇÃO - ACIONAMENTO - 8\r\n', 'CARRO POSICIONADOR', 'DESCARGA', 'VIRADOR DE VAGÕES', 'VV-3120NA-01', '1', 'DESCARGAVIRADOR DE VAGÕESVV-3120NA-01CARRO POSICIONADORTRANSLAÇÃO - ACIONAMENTO - 8');
INSERT INTO `subconjunto` VALUES ('9', 'SISTEMA HIDRÁULICO - BOMBA HIDRÁULICA - 250 S/M\r\n', 'CARRO POSICIONADOR', 'DESCARGA', 'VIRADOR DE VAGÕES', 'VV-3120NA-01', '1', 'DESCARGAVIRADOR DE VAGÕESVV-3120NA-01CARRO POSICIONADORSISTEMA HIDRÁULICO - BOMBA HIDRÁULICA - 250 S/M');
INSERT INTO `subconjunto` VALUES ('10', 'SISTEMA HIDRÁULICO - BOMBA HIDRÁULICA - 132 S\r\n', 'CARRO POSICIONADOR', 'DESCARGA', 'VIRADOR DE VAGÕES', 'VV-3120NA-01', '1', 'DESCARGAVIRADOR DE VAGÕESVV-3120NA-01CARRO POSICIONADORSISTEMA HIDRÁULICO - BOMBA HIDRÁULICA - 132 S');
INSERT INTO `subconjunto` VALUES ('11', 'BOMBA PRIMÁRIA\r\n', 'SISTEMA DE ASPERSÃO', 'DESCARGA', 'VIRADOR DE VAGÕES', 'VV-3120NA-01', '2', 'DESCARGAVIRADOR DE VAGÕESVV-3120NA-01SISTEMA DE ASPERSÃOBOMBA PRIMÁRIA');
INSERT INTO `subconjunto` VALUES ('12', 'BOMBA SECUNDÁRIA\r\n', 'SISTEMA DE ASPERSÃO', 'DESCARGA', 'VIRADOR DE VAGÕES', 'VV-3120NA-01', '2', 'DESCARGAVIRADOR DE VAGÕESVV-3120NA-01SISTEMA DE ASPERSÃOBOMBA SECUNDÁRIA');
INSERT INTO `subconjunto` VALUES ('13', 'ACIONAMENTO DO GIRO - ENTRADA\r\n', 'BARRIL', 'DESCARGA', 'VIRADOR DE VAGÕES', 'VV-3120NA-01', '3', 'DESCARGAVIRADOR DE VAGÕESVV-3120NA-01BARRILACIONAMENTO DO GIRO - ENTRADA');
INSERT INTO `subconjunto` VALUES ('14', 'ACIONAMENTO DO GIRO - SAÍDA\r\n', 'BARRIL', 'DESCARGA', 'VIRADOR DE VAGÕES', 'VV-3120NA-01', '3', 'DESCARGAVIRADOR DE VAGÕESVV-3120NA-01BARRILACIONAMENTO DO GIRO - SAÍDA');
INSERT INTO `subconjunto` VALUES ('15', 'SISTEMA HIDRÁULICO - BOMBA HIDRÁULICA - A\r\n', 'GRAMPOS DO BARRIL', 'DESCARGA', 'VIRADOR DE VAGÕES', 'VV-3120NA-01', '4', 'DESCARGAVIRADOR DE VAGÕESVV-3120NA-01GRAMPOS DO BARRILSISTEMA HIDRÁULICO - BOMBA HIDRÁULICA - A');
INSERT INTO `subconjunto` VALUES ('16', 'SISTEMA HIDRÁULICO - BOMBA HIDRÁULICA - B\r\n', 'GRAMPOS DO BARRIL', 'DESCARGA', 'VIRADOR DE VAGÕES', 'VV-3120NA-01', '4', 'DESCARGAVIRADOR DE VAGÕESVV-3120NA-01GRAMPOS DO BARRILSISTEMA HIDRÁULICO - BOMBA HIDRÁULICA - B');
INSERT INTO `subconjunto` VALUES ('17', 'SISTEMA HIDRÁULICO - BOMBA HIDRÁULICA\r\n', 'FREIO DE SAÍDA', 'DESCARGA', 'VIRADOR DE VAGÕES', 'VV-3120NA-01', '5', 'DESCARGAVIRADOR DE VAGÕESVV-3120NA-01FREIO DE SAÍDASISTEMA HIDRÁULICO - BOMBA HIDRÁULICA');
INSERT INTO `subconjunto` VALUES ('18', 'ACIONAMENTO - 1.1\r\n', 'ALIMENTADOR DE SAPATA - 01', 'DESCARGA', 'VIRADOR DE VAGÕES', 'VV-3120NA-01', '6', 'DESCARGAVIRADOR DE VAGÕESVV-3120NA-01ALIMENTADOR DE SAPATA - 01ACIONAMENTO - 1.1');
INSERT INTO `subconjunto` VALUES ('19', 'RODA DE ACIONAMENTO - 1.1\r\n', 'ALIMENTADOR DE SAPATA - 01', 'DESCARGA', 'VIRADOR DE VAGÕES', 'VV-3120NA-01', '6', 'DESCARGAVIRADOR DE VAGÕESVV-3120NA-01ALIMENTADOR DE SAPATA - 01RODA DE ACIONAMENTO - 1.1');
INSERT INTO `subconjunto` VALUES ('20', 'RODA DE RETORNO - 1.1\r\n', 'ALIMENTADOR DE SAPATA - 01', 'DESCARGA', 'VIRADOR DE VAGÕES', 'VV-3120NA-01', '6', 'DESCARGAVIRADOR DE VAGÕESVV-3120NA-01ALIMENTADOR DE SAPATA - 01RODA DE RETORNO - 1.1');
INSERT INTO `subconjunto` VALUES ('21', 'ACIONAMENTO - 1.2\r\n', 'ALIMENTADOR DE SAPATA - 01', 'DESCARGA', 'VIRADOR DE VAGÕES', 'VV-3120NA-01', '6', 'DESCARGAVIRADOR DE VAGÕESVV-3120NA-01ALIMENTADOR DE SAPATA - 01ACIONAMENTO - 1.2');
INSERT INTO `subconjunto` VALUES ('22', 'RODA DE ACIONAMENTO - 1.2\r\n', 'ALIMENTADOR DE SAPATA - 01', 'DESCARGA', 'VIRADOR DE VAGÕES', 'VV-3120NA-01', '6', 'DESCARGAVIRADOR DE VAGÕESVV-3120NA-01ALIMENTADOR DE SAPATA - 01RODA DE ACIONAMENTO - 1.2');
INSERT INTO `subconjunto` VALUES ('23', 'RODA DE RETORNO - 1.2\r\n', 'ALIMENTADOR DE SAPATA - 01', 'DESCARGA', 'VIRADOR DE VAGÕES', 'VV-3120NA-01', '6', 'DESCARGAVIRADOR DE VAGÕESVV-3120NA-01ALIMENTADOR DE SAPATA - 01RODA DE RETORNO - 1.2');
INSERT INTO `subconjunto` VALUES ('24', 'ACIONAMENTO - 2.1\r\n', 'ALIMENTADOR DE SAPATA - 02', 'DESCARGA', 'VIRADOR DE VAGÕES', 'VV-3120NA-01', '7', 'DESCARGAVIRADOR DE VAGÕESVV-3120NA-01ALIMENTADOR DE SAPATA - 02ACIONAMENTO - 2.1');
INSERT INTO `subconjunto` VALUES ('25', 'RODA DE ACIONAMENTO - 2.1\r\n', 'ALIMENTADOR DE SAPATA - 02', 'DESCARGA', 'VIRADOR DE VAGÕES', 'VV-3120NA-01', '7', 'DESCARGAVIRADOR DE VAGÕESVV-3120NA-01ALIMENTADOR DE SAPATA - 02RODA DE ACIONAMENTO - 2.1');
INSERT INTO `subconjunto` VALUES ('26', 'RODA DE RETORNO - 2.1\r\n', 'ALIMENTADOR DE SAPATA - 02', 'DESCARGA', 'VIRADOR DE VAGÕES', 'VV-3120NA-01', '7', 'DESCARGAVIRADOR DE VAGÕESVV-3120NA-01ALIMENTADOR DE SAPATA - 02RODA DE RETORNO - 2.1');
INSERT INTO `subconjunto` VALUES ('27', 'ACIONAMENTO - 2.2\r\n', 'ALIMENTADOR DE SAPATA - 02', 'DESCARGA', 'VIRADOR DE VAGÕES', 'VV-3120NA-01', '7', 'DESCARGAVIRADOR DE VAGÕESVV-3120NA-01ALIMENTADOR DE SAPATA - 02ACIONAMENTO - 2.2');
INSERT INTO `subconjunto` VALUES ('28', 'RODA DE ACIONAMENTO - 2.2\r\n', 'ALIMENTADOR DE SAPATA - 02', 'DESCARGA', 'VIRADOR DE VAGÕES', 'VV-3120NA-01', '7', 'DESCARGAVIRADOR DE VAGÕESVV-3120NA-01ALIMENTADOR DE SAPATA - 02RODA DE ACIONAMENTO - 2.2');
INSERT INTO `subconjunto` VALUES ('29', 'RODA DE RETORNO - 2.2\r\n', 'ALIMENTADOR DE SAPATA - 02', 'DESCARGA', 'VIRADOR DE VAGÕES', 'VV-3120NA-01', '7', 'DESCARGAVIRADOR DE VAGÕESVV-3120NA-01ALIMENTADOR DE SAPATA - 02RODA DE RETORNO - 2.2');
INSERT INTO `subconjunto` VALUES ('30', 'ACIONAMENTO\r\n', 'TRANSPORTADOR DE FINOS - 01', 'DESCARGA', 'VIRADOR DE VAGÕES', 'VV-3120NA-01', '8', 'DESCARGAVIRADOR DE VAGÕESVV-3120NA-01TRANSPORTADOR DE FINOS - 01ACIONAMENTO');
INSERT INTO `subconjunto` VALUES ('31', 'RODA DE ACIONAMENTO\r\n', 'TRANSPORTADOR DE FINOS - 01', 'DESCARGA', 'VIRADOR DE VAGÕES', 'VV-3120NA-01', '8', 'DESCARGAVIRADOR DE VAGÕESVV-3120NA-01TRANSPORTADOR DE FINOS - 01RODA DE ACIONAMENTO');
INSERT INTO `subconjunto` VALUES ('32', 'RODA DE RETORNO\r\n', 'TRANSPORTADOR DE FINOS - 01', 'DESCARGA', 'VIRADOR DE VAGÕES', 'VV-3120NA-01', '8', 'DESCARGAVIRADOR DE VAGÕESVV-3120NA-01TRANSPORTADOR DE FINOS - 01RODA DE RETORNO');
INSERT INTO `subconjunto` VALUES ('33', 'ACIONAMENTO\r\n', 'TRANSPORTADOR DE FINOS - 02', 'DESCARGA', 'VIRADOR DE VAGÕES', 'VV-3120NA-01', '9', 'DESCARGAVIRADOR DE VAGÕESVV-3120NA-01TRANSPORTADOR DE FINOS - 02ACIONAMENTO');
INSERT INTO `subconjunto` VALUES ('34', 'RODA DE ACIONAMENTO\r\n', 'TRANSPORTADOR DE FINOS - 02', 'DESCARGA', 'VIRADOR DE VAGÕES', 'VV-3120NA-01', '9', 'DESCARGAVIRADOR DE VAGÕESVV-3120NA-01TRANSPORTADOR DE FINOS - 02RODA DE ACIONAMENTO');
INSERT INTO `subconjunto` VALUES ('35', 'RODA DE RETORNO\r\n', 'TRANSPORTADOR DE FINOS - 02', 'DESCARGA', 'VIRADOR DE VAGÕES', 'VV-3120NA-01', '9', 'DESCARGAVIRADOR DE VAGÕESVV-3120NA-01TRANSPORTADOR DE FINOS - 02RODA DE RETORNO');
INSERT INTO `subconjunto` VALUES ('36', 'ACIONAMENTO\r\n', 'TRANSPORTADOR', 'DESCARGA', 'TRANSPORTADORES DESCARGA', 'TR-3120NA-01', '10', 'DESCARGATRANSPORTADORES DESCARGATR-3120NA-01TRANSPORTADORACIONAMENTO');
INSERT INTO `subconjunto` VALUES ('37', 'TAMBOR DE ACIONAMENTO E DESCARGA\r\n', 'TRANSPORTADOR', 'DESCARGA', 'TRANSPORTADORES DESCARGA', 'TR-3120NA-01', '10', 'DESCARGATRANSPORTADORES DESCARGATR-3120NA-01TRANSPORTADORTAMBOR DE ACIONAMENTO E DESCARGA');
INSERT INTO `subconjunto` VALUES ('38', 'TAMBOR DE DESVIO\r\n', 'TRANSPORTADOR', 'DESCARGA', 'TRANSPORTADORES DESCARGA', 'TR-3120NA-01', '10', 'DESCARGATRANSPORTADORES DESCARGATR-3120NA-01TRANSPORTADORTAMBOR DE DESVIO');
INSERT INTO `subconjunto` VALUES ('39', 'TAMBOR DE ESTICAMENTO\r\n', 'TRANSPORTADOR', 'DESCARGA', 'TRANSPORTADORES DESCARGA', 'TR-3120NA-01', '10', 'DESCARGATRANSPORTADORES DESCARGATR-3120NA-01TRANSPORTADORTAMBOR DE ESTICAMENTO');
INSERT INTO `subconjunto` VALUES ('40', 'TAMBOR DE RETORNO\r\n', 'TRANSPORTADOR', 'DESCARGA', 'TRANSPORTADORES DESCARGA', 'TR-3120NA-01', '10', 'DESCARGATRANSPORTADORES DESCARGATR-3120NA-01TRANSPORTADORTAMBOR DE RETORNO');
INSERT INTO `subconjunto` VALUES ('41', 'ACIONAMENTO\r\n', 'TRANSPORTADOR', 'DESCARGA', 'TRANSPORTADORES DESCARGA', 'TR-3140NA-01', '11', 'DESCARGATRANSPORTADORES DESCARGATR-3140NA-01TRANSPORTADORACIONAMENTO');
INSERT INTO `subconjunto` VALUES ('42', 'TAMBOR DE ACIONAMENTO E DESCARGA\r\n', 'TRANSPORTADOR', 'DESCARGA', 'TRANSPORTADORES DESCARGA', 'TR-3140NA-01', '11', 'DESCARGATRANSPORTADORES DESCARGATR-3140NA-01TRANSPORTADORTAMBOR DE ACIONAMENTO E DESCARGA');
INSERT INTO `subconjunto` VALUES ('43', 'TAMBOR DE DESVIO\r\n', 'TRANSPORTADOR', 'DESCARGA', 'TRANSPORTADORES DESCARGA', 'TR-3140NA-01', '11', 'DESCARGATRANSPORTADORES DESCARGATR-3140NA-01TRANSPORTADORTAMBOR DE DESVIO');
INSERT INTO `subconjunto` VALUES ('44', 'TAMBOR DE ESTICAMENTO\r\n', 'TRANSPORTADOR', 'DESCARGA', 'TRANSPORTADORES DESCARGA', 'TR-3140NA-01', '11', 'DESCARGATRANSPORTADORES DESCARGATR-3140NA-01TRANSPORTADORTAMBOR DE ESTICAMENTO');
INSERT INTO `subconjunto` VALUES ('45', 'TAMBOR DE RETORNO\r\n', 'TRANSPORTADOR', 'DESCARGA', 'TRANSPORTADORES DESCARGA', 'TR-3140NA-01', '11', 'DESCARGATRANSPORTADORES DESCARGATR-3140NA-01TRANSPORTADORTAMBOR DE RETORNO');
INSERT INTO `subconjunto` VALUES ('46', 'ACIONAMENTO 1\r\n', 'TRANSPORTADOR', 'DESCARGA', 'TRANSPORTADORES PÁTIO', 'TR-3140NA-02', '12', 'DESCARGATRANSPORTADORES PÁTIOTR-3140NA-02TRANSPORTADORACIONAMENTO 1');
INSERT INTO `subconjunto` VALUES ('47', 'ACIONAMENTO 2\r\n', 'TRANSPORTADOR', 'DESCARGA', 'TRANSPORTADORES PÁTIO', 'TR-3140NA-02', '12', 'DESCARGATRANSPORTADORES PÁTIOTR-3140NA-02TRANSPORTADORACIONAMENTO 2');
INSERT INTO `subconjunto` VALUES ('48', 'TAMBOR DE ACIONAMENTO\r\n', 'TRANSPORTADOR', 'DESCARGA', 'TRANSPORTADORES PÁTIO', 'TR-3140NA-02', '12', 'DESCARGATRANSPORTADORES PÁTIOTR-3140NA-02TRANSPORTADORTAMBOR DE ACIONAMENTO');
INSERT INTO `subconjunto` VALUES ('49', 'TAMBOR DE DESVIO\r\n', 'TRANSPORTADOR', 'DESCARGA', 'TRANSPORTADORES PÁTIO', 'TR-3140NA-02', '12', 'DESCARGATRANSPORTADORES PÁTIOTR-3140NA-02TRANSPORTADORTAMBOR DE DESVIO');
INSERT INTO `subconjunto` VALUES ('50', 'TAMBOR DE ESTICAMENTO\r\n', 'TRANSPORTADOR', 'DESCARGA', 'TRANSPORTADORES PÁTIO', 'TR-3140NA-02', '12', 'DESCARGATRANSPORTADORES PÁTIOTR-3140NA-02TRANSPORTADORTAMBOR DE ESTICAMENTO');
INSERT INTO `subconjunto` VALUES ('51', 'TAMBOR DE RETORNO\r\n', 'TRANSPORTADOR', 'DESCARGA', 'TRANSPORTADORES PÁTIO', 'TR-3140NA-02', '12', 'DESCARGATRANSPORTADORES PÁTIOTR-3140NA-02TRANSPORTADORTAMBOR DE RETORNO');
INSERT INTO `subconjunto` VALUES ('52', 'TAMBOR DE DESCARGA\r\n', 'TRANSPORTADOR', 'DESCARGA', 'TRANSPORTADORES PÁTIO', 'TR-3140NA-02', '12', 'DESCARGATRANSPORTADORES PÁTIOTR-3140NA-02TRANSPORTADORTAMBOR DE DESCARGA');
INSERT INTO `subconjunto` VALUES ('53', 'ACIONAMENTO\r\n', 'TRANSPORTADOR', 'DESCARGA', 'TRANSPORTADORES DESCARGA', 'TR-3140NA-03', '13', 'DESCARGATRANSPORTADORES DESCARGATR-3140NA-03TRANSPORTADORACIONAMENTO');
INSERT INTO `subconjunto` VALUES ('54', 'TAMBOR DE ACIONAMENTO E DESCARGA\r\n', 'TRANSPORTADOR', 'DESCARGA', 'TRANSPORTADORES DESCARGA', 'TR-3140NA-03', '13', 'DESCARGATRANSPORTADORES DESCARGATR-3140NA-03TRANSPORTADORTAMBOR DE ACIONAMENTO E DESCARGA');
INSERT INTO `subconjunto` VALUES ('55', 'TAMBOR DE DESVIO\r\n', 'TRANSPORTADOR', 'DESCARGA', 'TRANSPORTADORES DESCARGA', 'TR-3140NA-03', '13', 'DESCARGATRANSPORTADORES DESCARGATR-3140NA-03TRANSPORTADORTAMBOR DE DESVIO');
INSERT INTO `subconjunto` VALUES ('56', 'TAMBOR DE ESTICAMENTO\r\n', 'TRANSPORTADOR', 'DESCARGA', 'TRANSPORTADORES DESCARGA', 'TR-3140NA-03', '13', 'DESCARGATRANSPORTADORES DESCARGATR-3140NA-03TRANSPORTADORTAMBOR DE ESTICAMENTO');
INSERT INTO `subconjunto` VALUES ('57', 'TAMBOR DE RETORNO\r\n', 'TRANSPORTADOR', 'DESCARGA', 'TRANSPORTADORES DESCARGA', 'TR-3140NA-03', '13', 'DESCARGATRANSPORTADORES DESCARGATR-3140NA-03TRANSPORTADORTAMBOR DE RETORNO');
INSERT INTO `subconjunto` VALUES ('58', 'ACIONAMENTO 1\r\n', 'TRANSPORTADOR', 'DESCARGA', 'TRANSPORTADORES PÁTIO', 'TR-3140NA-04', '14', 'DESCARGATRANSPORTADORES PÁTIOTR-3140NA-04TRANSPORTADORACIONAMENTO 1');
INSERT INTO `subconjunto` VALUES ('59', 'ACIONAMENTO 2\r\n', 'TRANSPORTADOR', 'DESCARGA', 'TRANSPORTADORES PÁTIO', 'TR-3140NA-04', '14', 'DESCARGATRANSPORTADORES PÁTIOTR-3140NA-04TRANSPORTADORACIONAMENTO 2');
INSERT INTO `subconjunto` VALUES ('60', 'TAMBOR DE ACIONAMENTO\r\n', 'TRANSPORTADOR', 'DESCARGA', 'TRANSPORTADORES PÁTIO', 'TR-3140NA-04', '14', 'DESCARGATRANSPORTADORES PÁTIOTR-3140NA-04TRANSPORTADORTAMBOR DE ACIONAMENTO');
INSERT INTO `subconjunto` VALUES ('61', 'TAMBOR DE DESVIO\r\n', 'TRANSPORTADOR', 'DESCARGA', 'TRANSPORTADORES PÁTIO', 'TR-3140NA-04', '14', 'DESCARGATRANSPORTADORES PÁTIOTR-3140NA-04TRANSPORTADORTAMBOR DE DESVIO');
INSERT INTO `subconjunto` VALUES ('62', 'TAMBOR DE ESTICAMENTO\r\n', 'TRANSPORTADOR', 'DESCARGA', 'TRANSPORTADORES PÁTIO', 'TR-3140NA-04', '14', 'DESCARGATRANSPORTADORES PÁTIOTR-3140NA-04TRANSPORTADORTAMBOR DE ESTICAMENTO');
INSERT INTO `subconjunto` VALUES ('63', 'TAMBOR DE RETORNO\r\n', 'TRANSPORTADOR', 'DESCARGA', 'TRANSPORTADORES PÁTIO', 'TR-3140NA-04', '14', 'DESCARGATRANSPORTADORES PÁTIOTR-3140NA-04TRANSPORTADORTAMBOR DE RETORNO');
INSERT INTO `subconjunto` VALUES ('64', 'TAMBOR DE DESCARGA\r\n', 'TRANSPORTADOR', 'DESCARGA', 'TRANSPORTADORES PÁTIO', 'TR-3140NA-04', '14', 'DESCARGATRANSPORTADORES PÁTIOTR-3140NA-04TRANSPORTADORTAMBOR DE DESCARGA');
INSERT INTO `subconjunto` VALUES ('65', 'ACIONAMENTO 1\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'TRANSPORTADORES PÁTIO', 'TR-3140NA-05', '15', 'EMBARQUETRANSPORTADORES PÁTIOTR-3140NA-05TRANSPORTADORACIONAMENTO 1');
INSERT INTO `subconjunto` VALUES ('66', 'ACIONAMENTO 2\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'TRANSPORTADORES PÁTIO', 'TR-3140NA-05', '15', 'EMBARQUETRANSPORTADORES PÁTIOTR-3140NA-05TRANSPORTADORACIONAMENTO 2');
INSERT INTO `subconjunto` VALUES ('67', 'TAMBOR DE ACIONAMENTO\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'TRANSPORTADORES PÁTIO', 'TR-3140NA-05', '15', 'EMBARQUETRANSPORTADORES PÁTIOTR-3140NA-05TRANSPORTADORTAMBOR DE ACIONAMENTO');
INSERT INTO `subconjunto` VALUES ('68', 'TAMBOR DE DESVIO\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'TRANSPORTADORES PÁTIO', 'TR-3140NA-05', '15', 'EMBARQUETRANSPORTADORES PÁTIOTR-3140NA-05TRANSPORTADORTAMBOR DE DESVIO');
INSERT INTO `subconjunto` VALUES ('69', 'TAMBOR DE ESTICAMENTO E RETORNO\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'TRANSPORTADORES PÁTIO', 'TR-3140NA-05', '15', 'EMBARQUETRANSPORTADORES PÁTIOTR-3140NA-05TRANSPORTADORTAMBOR DE ESTICAMENTO E RETORNO');
INSERT INTO `subconjunto` VALUES ('70', 'TAMBOR DE FREIO\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'TRANSPORTADORES PÁTIO', 'TR-3140NA-05', '15', 'EMBARQUETRANSPORTADORES PÁTIOTR-3140NA-05TRANSPORTADORTAMBOR DE FREIO');
INSERT INTO `subconjunto` VALUES ('71', 'TAMBOR DE DESCARGA\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'TRANSPORTADORES PÁTIO', 'TR-3140NA-05', '15', 'EMBARQUETRANSPORTADORES PÁTIOTR-3140NA-05TRANSPORTADORTAMBOR DE DESCARGA');
INSERT INTO `subconjunto` VALUES ('72', 'ACIONAMENTO\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'TRANSPORTADORES EMBARQUE', 'TR-3140NA-06', '16', 'EMBARQUETRANSPORTADORES EMBARQUETR-3140NA-06TRANSPORTADORACIONAMENTO');
INSERT INTO `subconjunto` VALUES ('73', 'TAMBOR DE ACIONAMENTO E DESCARGA\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'TRANSPORTADORES EMBARQUE', 'TR-3140NA-06', '16', 'EMBARQUETRANSPORTADORES EMBARQUETR-3140NA-06TRANSPORTADORTAMBOR DE ACIONAMENTO E DESCARGA');
INSERT INTO `subconjunto` VALUES ('74', 'TAMBOR DE DESVIO\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'TRANSPORTADORES EMBARQUE', 'TR-3140NA-06', '16', 'EMBARQUETRANSPORTADORES EMBARQUETR-3140NA-06TRANSPORTADORTAMBOR DE DESVIO');
INSERT INTO `subconjunto` VALUES ('75', 'TAMBOR DE ESTICAMENTO\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'TRANSPORTADORES EMBARQUE', 'TR-3140NA-06', '16', 'EMBARQUETRANSPORTADORES EMBARQUETR-3140NA-06TRANSPORTADORTAMBOR DE ESTICAMENTO');
INSERT INTO `subconjunto` VALUES ('76', 'TAMBOR DE RETORNO\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'TRANSPORTADORES EMBARQUE', 'TR-3140NA-06', '16', 'EMBARQUETRANSPORTADORES EMBARQUETR-3140NA-06TRANSPORTADORTAMBOR DE RETORNO');
INSERT INTO `subconjunto` VALUES ('77', 'ACIONAMENTO 1\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'TRANSPORTADORES PÁTIO', 'TR-3140NA-07', '17', 'EMBARQUETRANSPORTADORES PÁTIOTR-3140NA-07TRANSPORTADORACIONAMENTO 1');
INSERT INTO `subconjunto` VALUES ('78', 'ACIONAMENTO 2\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'TRANSPORTADORES PÁTIO', 'TR-3140NA-07', '17', 'EMBARQUETRANSPORTADORES PÁTIOTR-3140NA-07TRANSPORTADORACIONAMENTO 2');
INSERT INTO `subconjunto` VALUES ('79', 'TAMBOR DE ACIONAMENTO\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'TRANSPORTADORES PÁTIO', 'TR-3140NA-07', '17', 'EMBARQUETRANSPORTADORES PÁTIOTR-3140NA-07TRANSPORTADORTAMBOR DE ACIONAMENTO');
INSERT INTO `subconjunto` VALUES ('81', 'TAMBOR DE DESVIO\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'TRANSPORTADORES PÁTIO', 'TR-3140NA-07', '17', 'EMBARQUETRANSPORTADORES PÁTIOTR-3140NA-07TRANSPORTADORTAMBOR DE DESVIO');
INSERT INTO `subconjunto` VALUES ('82', 'TAMBOR DE ESTICAMENTO E RETORNO\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'TRANSPORTADORES PÁTIO', 'TR-3140NA-07', '17', 'EMBARQUETRANSPORTADORES PÁTIOTR-3140NA-07TRANSPORTADORTAMBOR DE ESTICAMENTO E RETORNO');
INSERT INTO `subconjunto` VALUES ('83', 'TAMBOR DE FREIO\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'TRANSPORTADORES PÁTIO', 'TR-3140NA-07', '17', 'EMBARQUETRANSPORTADORES PÁTIOTR-3140NA-07TRANSPORTADORTAMBOR DE FREIO');
INSERT INTO `subconjunto` VALUES ('84', 'TAMBOR DE DESCARGA\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'TRANSPORTADORES PÁTIO', 'TR-3140NA-07', '17', 'EMBARQUETRANSPORTADORES PÁTIOTR-3140NA-07TRANSPORTADORTAMBOR DE DESCARGA');
INSERT INTO `subconjunto` VALUES ('85', 'ACIONAMENTO\r\n', 'TRANSPORTADOR', 'DESCARGA', 'TRANSPORTADORES DESCARGA', 'TR-3140NA-08', '18', 'DESCARGATRANSPORTADORES DESCARGATR-3140NA-08TRANSPORTADORACIONAMENTO');
INSERT INTO `subconjunto` VALUES ('86', 'TAMBOR DE ACIONAMENTO\r\n', 'TRANSPORTADOR', 'DESCARGA', 'TRANSPORTADORES DESCARGA', 'TR-3140NA-08', '18', 'DESCARGATRANSPORTADORES DESCARGATR-3140NA-08TRANSPORTADORTAMBOR DE ACIONAMENTO');
INSERT INTO `subconjunto` VALUES ('87', 'TAMBOR DE RETORNO E ESTICAMENTO\r\n', 'TRANSPORTADOR', 'DESCARGA', 'TRANSPORTADORES DESCARGA', 'TR-3140NA-08', '18', 'DESCARGATRANSPORTADORES DESCARGATR-3140NA-08TRANSPORTADORTAMBOR DE RETORNO E ESTICAMENTO');
INSERT INTO `subconjunto` VALUES ('88', 'ACIONAMENTO\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'TRANSPORTADORES EMBARQUE', 'TR-3140NA-09', '19', 'EMBARQUETRANSPORTADORES EMBARQUETR-3140NA-09TRANSPORTADORACIONAMENTO');
INSERT INTO `subconjunto` VALUES ('89', 'TAMBOR DE ACIONAMENTO\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'TRANSPORTADORES EMBARQUE', 'TR-3140NA-09', '19', 'EMBARQUETRANSPORTADORES EMBARQUETR-3140NA-09TRANSPORTADORTAMBOR DE ACIONAMENTO');
INSERT INTO `subconjunto` VALUES ('90', 'TAMBOR DE RETORNO E ESTICAMENTO\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'TRANSPORTADORES EMBARQUE', 'TR-3140NA-09', '19', 'EMBARQUETRANSPORTADORES EMBARQUETR-3140NA-09TRANSPORTADORTAMBOR DE RETORNO E ESTICAMENTO');
INSERT INTO `subconjunto` VALUES ('91', 'ACIONAMENTO\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'TRANSPORTADORES EMBARQUE', 'TR-3140NA-10', '20', 'EMBARQUETRANSPORTADORES EMBARQUETR-3140NA-10TRANSPORTADORACIONAMENTO');
INSERT INTO `subconjunto` VALUES ('92', 'TAMBOR DE ACIONAMENTO\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'TRANSPORTADORES EMBARQUE', 'TR-3140NA-10', '20', 'EMBARQUETRANSPORTADORES EMBARQUETR-3140NA-10TRANSPORTADORTAMBOR DE ACIONAMENTO');
INSERT INTO `subconjunto` VALUES ('93', 'TAMBOR DE RETORNO E ESTICAMENTO\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'TRANSPORTADORES EMBARQUE', 'TR-3140NA-10', '20', 'EMBARQUETRANSPORTADORES EMBARQUETR-3140NA-10TRANSPORTADORTAMBOR DE RETORNO E ESTICAMENTO');
INSERT INTO `subconjunto` VALUES ('94', 'ACIONAMENTO\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'TRANSPORTADORES EMBARQUE', 'TR-3140NA-11', '21', 'EMBARQUETRANSPORTADORES EMBARQUETR-3140NA-11TRANSPORTADORACIONAMENTO');
INSERT INTO `subconjunto` VALUES ('95', 'TAMBOR DE ACIONAMENTO\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'TRANSPORTADORES EMBARQUE', 'TR-3140NA-11', '21', 'EMBARQUETRANSPORTADORES EMBARQUETR-3140NA-11TRANSPORTADORTAMBOR DE ACIONAMENTO');
INSERT INTO `subconjunto` VALUES ('96', 'TAMBOR DE RETORNO E ESTICAMENTO\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'TRANSPORTADORES EMBARQUE', 'TR-3140NA-11', '21', 'EMBARQUETRANSPORTADORES EMBARQUETR-3140NA-11TRANSPORTADORTAMBOR DE RETORNO E ESTICAMENTO');
INSERT INTO `subconjunto` VALUES ('97', 'ACIONAMENTO\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'TRANSPORTADORES EMBARQUE', 'TR-3140NA-12', '22', 'EMBARQUETRANSPORTADORES EMBARQUETR-3140NA-12TRANSPORTADORACIONAMENTO');
INSERT INTO `subconjunto` VALUES ('98', 'TAMBOR DE ACIONAMENTO\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'TRANSPORTADORES EMBARQUE', 'TR-3140NA-12', '22', 'EMBARQUETRANSPORTADORES EMBARQUETR-3140NA-12TRANSPORTADORTAMBOR DE ACIONAMENTO');
INSERT INTO `subconjunto` VALUES ('99', 'TAMBOR DE RETORNO E ESTICAMENTO\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'TRANSPORTADORES EMBARQUE', 'TR-3140NA-12', '22', 'EMBARQUETRANSPORTADORES EMBARQUETR-3140NA-12TRANSPORTADORTAMBOR DE RETORNO E ESTICAMENTO');
INSERT INTO `subconjunto` VALUES ('100', 'ACIONAMENTO 1\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'PÍER', 'TR-3220NA-01', '23', 'EMBARQUEPÍERTR-3220NA-01TRANSPORTADORACIONAMENTO 1');
INSERT INTO `subconjunto` VALUES ('101', 'ACIONAMENTO 2\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'PÍER', 'TR-3220NA-01', '23', 'EMBARQUEPÍERTR-3220NA-01TRANSPORTADORACIONAMENTO 2');
INSERT INTO `subconjunto` VALUES ('102', 'TAMBOR DE ACIONAMENTO\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'PÍER', 'TR-3220NA-01', '23', 'EMBARQUEPÍERTR-3220NA-01TRANSPORTADORTAMBOR DE ACIONAMENTO');
INSERT INTO `subconjunto` VALUES ('103', 'TAMBOR DE DESVIO\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'PÍER', 'TR-3220NA-01', '23', 'EMBARQUEPÍERTR-3220NA-01TRANSPORTADORTAMBOR DE DESVIO');
INSERT INTO `subconjunto` VALUES ('104', 'TAMBOR DE ESTICAMENTO\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'PÍER', 'TR-3220NA-01', '23', 'EMBARQUEPÍERTR-3220NA-01TRANSPORTADORTAMBOR DE ESTICAMENTO');
INSERT INTO `subconjunto` VALUES ('105', 'TAMBOR DE RETORNO E FREIO\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'PÍER', 'TR-3220NA-01', '23', 'EMBARQUEPÍERTR-3220NA-01TRANSPORTADORTAMBOR DE RETORNO E FREIO');
INSERT INTO `subconjunto` VALUES ('106', 'TAMBOR DE DESCARGA\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'PÍER', 'TR-3220NA-01', '23', 'EMBARQUEPÍERTR-3220NA-01TRANSPORTADORTAMBOR DE DESCARGA');
INSERT INTO `subconjunto` VALUES ('107', 'ACIONAMENTO 1\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'PÍER', 'TR-3220NA-02', '24', 'EMBARQUEPÍERTR-3220NA-02TRANSPORTADORACIONAMENTO 1');
INSERT INTO `subconjunto` VALUES ('108', 'ACIONAMENTO 2\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'PÍER', 'TR-3220NA-02', '24', 'EMBARQUEPÍERTR-3220NA-02TRANSPORTADORACIONAMENTO 2');
INSERT INTO `subconjunto` VALUES ('109', 'TAMBOR DE ACIONAMENTO\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'PÍER', 'TR-3220NA-02', '24', 'EMBARQUEPÍERTR-3220NA-02TRANSPORTADORTAMBOR DE ACIONAMENTO');
INSERT INTO `subconjunto` VALUES ('110', 'TAMBOR DE DESVIO\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'PÍER', 'TR-3220NA-02', '24', 'EMBARQUEPÍERTR-3220NA-02TRANSPORTADORTAMBOR DE DESVIO');
INSERT INTO `subconjunto` VALUES ('111', 'TAMBOR DE ESTICAMENTO\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'PÍER', 'TR-3220NA-02', '24', 'EMBARQUEPÍERTR-3220NA-02TRANSPORTADORTAMBOR DE ESTICAMENTO');
INSERT INTO `subconjunto` VALUES ('112', 'TAMBOR DE RETORNO E FREIO\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'PÍER', 'TR-3220NA-02', '24', 'EMBARQUEPÍERTR-3220NA-02TRANSPORTADORTAMBOR DE RETORNO E FREIO');
INSERT INTO `subconjunto` VALUES ('113', 'TAMBOR DE DESCARGA\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'PÍER', 'TR-3220NA-02', '24', 'EMBARQUEPÍERTR-3220NA-02TRANSPORTADORTAMBOR DE DESCARGA');
INSERT INTO `subconjunto` VALUES ('114', 'ACIONAMENTO\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'PÍER', 'TR-3220NA-03', '25', 'EMBARQUEPÍERTR-3220NA-03TRANSPORTADORACIONAMENTO');
INSERT INTO `subconjunto` VALUES ('115', 'TAMBOR DE ACIONAMENTO\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'PÍER', 'TR-3220NA-03', '25', 'EMBARQUEPÍERTR-3220NA-03TRANSPORTADORTAMBOR DE ACIONAMENTO');
INSERT INTO `subconjunto` VALUES ('116', 'TAMBOR DE ABRAÇAMENTO\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'PÍER', 'TR-3220NA-03', '25', 'EMBARQUEPÍERTR-3220NA-03TRANSPORTADORTAMBOR DE ABRAÇAMENTO');
INSERT INTO `subconjunto` VALUES ('117', 'TAMBOR DE DESVIO\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'PÍER', 'TR-3220NA-03', '25', 'EMBARQUEPÍERTR-3220NA-03TRANSPORTADORTAMBOR DE DESVIO');
INSERT INTO `subconjunto` VALUES ('118', 'TAMBOR DE ESTICAMENTO\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'PÍER', 'TR-3220NA-03', '25', 'EMBARQUEPÍERTR-3220NA-03TRANSPORTADORTAMBOR DE ESTICAMENTO');
INSERT INTO `subconjunto` VALUES ('119', 'TAMBOR DE RETORNO\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'PÍER', 'TR-3220NA-03', '25', 'EMBARQUEPÍERTR-3220NA-03TRANSPORTADORTAMBOR DE RETORNO');
INSERT INTO `subconjunto` VALUES ('120', 'TAMBOR DE DESCARGA\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'PÍER', 'TR-3220NA-03', '25', 'EMBARQUEPÍERTR-3220NA-03TRANSPORTADORTAMBOR DE DESCARGA');
INSERT INTO `subconjunto` VALUES ('121', 'ROLO DE VELOCIDADE\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'PÍER', 'TR-3220NA-03', '25', 'EMBARQUEPÍERTR-3220NA-03TRANSPORTADORROLO DE VELOCIDADE');
INSERT INTO `subconjunto` VALUES ('122', 'ACIONAMENTO\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'PÍER', 'TR-3220NA-04', '26', 'EMBARQUEPÍERTR-3220NA-04TRANSPORTADORACIONAMENTO');
INSERT INTO `subconjunto` VALUES ('123', 'TAMBOR DE ACIONAMENTO\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'PÍER', 'TR-3220NA-04', '26', 'EMBARQUEPÍERTR-3220NA-04TRANSPORTADORTAMBOR DE ACIONAMENTO');
INSERT INTO `subconjunto` VALUES ('124', 'TAMBOR DE ABRAÇAMENTO\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'PÍER', 'TR-3220NA-04', '26', 'EMBARQUEPÍERTR-3220NA-04TRANSPORTADORTAMBOR DE ABRAÇAMENTO');
INSERT INTO `subconjunto` VALUES ('125', 'TAMBOR DE DESVIO\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'PÍER', 'TR-3220NA-04', '26', 'EMBARQUEPÍERTR-3220NA-04TRANSPORTADORTAMBOR DE DESVIO');
INSERT INTO `subconjunto` VALUES ('126', 'TAMBOR DE ESTICAMENTO\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'PÍER', 'TR-3220NA-04', '26', 'EMBARQUEPÍERTR-3220NA-04TRANSPORTADORTAMBOR DE ESTICAMENTO');
INSERT INTO `subconjunto` VALUES ('127', 'TAMBOR DE RETORNO\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'PÍER', 'TR-3220NA-04', '26', 'EMBARQUEPÍERTR-3220NA-04TRANSPORTADORTAMBOR DE RETORNO');
INSERT INTO `subconjunto` VALUES ('128', 'TAMBOR DE DESCARGA\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'PÍER', 'TR-3220NA-04', '26', 'EMBARQUEPÍERTR-3220NA-04TRANSPORTADORTAMBOR DE DESCARGA');
INSERT INTO `subconjunto` VALUES ('129', 'ROLO DE VELOCIDADE\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'PÍER', 'TR-3220NA-04', '26', 'EMBARQUEPÍERTR-3220NA-04TRANSPORTADORROLO DE VELOCIDADE');
INSERT INTO `subconjunto` VALUES ('130', 'SISTEMA HIDRÁULICO - BOMBA - 01\r\n', 'ELEVAÇÃO DA LANÇA', 'DESCARGA', 'MÁQUINAS DO PÁTIO', 'EP-3140NA-01', '27', 'DESCARGAMÁQUINAS DO PÁTIOEP-3140NA-01ELEVAÇÃO DA LANÇASISTEMA HIDRÁULICO - BOMBA - 01');
INSERT INTO `subconjunto` VALUES ('131', 'SISTEMA HIDRÁULICO - BOMBA - 02\r\n', 'ELEVAÇÃO DA LANÇA', 'DESCARGA', 'MÁQUINAS DO PÁTIO', 'EP-3140NA-01', '27', 'DESCARGAMÁQUINAS DO PÁTIOEP-3140NA-01ELEVAÇÃO DA LANÇASISTEMA HIDRÁULICO - BOMBA - 02');
INSERT INTO `subconjunto` VALUES ('132', 'ACIONAMENTO - 01\r\n', 'GIRO', 'DESCARGA', 'MÁQUINAS DO PÁTIO', 'EP-3140NA-01', '28', 'DESCARGAMÁQUINAS DO PÁTIOEP-3140NA-01GIROACIONAMENTO - 01');
INSERT INTO `subconjunto` VALUES ('133', 'ACIONAMENTO - 02\r\n', 'GIRO', 'DESCARGA', 'MÁQUINAS DO PÁTIO', 'EP-3140NA-01', '28', 'DESCARGAMÁQUINAS DO PÁTIOEP-3140NA-01GIROACIONAMENTO - 02');
INSERT INTO `subconjunto` VALUES ('134', 'ACIONAMENTO - 03\r\n', 'GIRO', 'DESCARGA', 'MÁQUINAS DO PÁTIO', 'EP-3140NA-01', '28', 'DESCARGAMÁQUINAS DO PÁTIOEP-3140NA-01GIROACIONAMENTO - 03');
INSERT INTO `subconjunto` VALUES ('135', 'ACIONAMENTO\r\n', 'TRANSPORTADOR DA LANÇA', 'DESCARGA', 'MÁQUINAS DO PÁTIO', 'EP-3140NA-01', '29', 'DESCARGAMÁQUINAS DO PÁTIOEP-3140NA-01TRANSPORTADOR DA LANÇAACIONAMENTO');
INSERT INTO `subconjunto` VALUES ('136', 'TAMBOR DE ACIONAMENTO\r\n', 'TRANSPORTADOR DA LANÇA', 'DESCARGA', 'MÁQUINAS DO PÁTIO', 'EP-3140NA-01', '29', 'DESCARGAMÁQUINAS DO PÁTIOEP-3140NA-01TRANSPORTADOR DA LANÇATAMBOR DE ACIONAMENTO');
INSERT INTO `subconjunto` VALUES ('137', 'TAMBOR DE RETORNO\r\n', 'TRANSPORTADOR DA LANÇA', 'DESCARGA', 'MÁQUINAS DO PÁTIO', 'EP-3140NA-01', '29', 'DESCARGAMÁQUINAS DO PÁTIOEP-3140NA-01TRANSPORTADOR DA LANÇATAMBOR DE RETORNO');
INSERT INTO `subconjunto` VALUES ('138', 'ACIONAMENTO\r\n', 'MANGUEIRA DE ÁGUA', 'DESCARGA', 'MÁQUINAS DO PÁTIO', 'EP-3140NA-01', '30', 'DESCARGAMÁQUINAS DO PÁTIOEP-3140NA-01MANGUEIRA DE ÁGUAACIONAMENTO');
INSERT INTO `subconjunto` VALUES ('139', 'ACIONAMENTO\r\n', 'MANGUERA ELÉTRICA', 'DESCARGA', 'MÁQUINAS DO PÁTIO', 'EP-3140NA-01', '31', 'DESCARGAMÁQUINAS DO PÁTIOEP-3140NA-01MANGUERA ELÉTRICAACIONAMENTO');
INSERT INTO `subconjunto` VALUES ('140', 'SISTEMA HIDRÁULICO - BOMBA - 01\r\n', 'ELEVAÇÃO DA LANÇA', 'DESCARGA', 'MÁQUINAS DO PÁTIO', 'EP-3140NA-02', '32', 'DESCARGAMÁQUINAS DO PÁTIOEP-3140NA-02ELEVAÇÃO DA LANÇASISTEMA HIDRÁULICO - BOMBA - 01');
INSERT INTO `subconjunto` VALUES ('141', 'SISTEMA HIDRÁULICO - BOMBA - 02\r\n', 'ELEVAÇÃO DA LANÇA', 'DESCARGA', 'MÁQUINAS DO PÁTIO', 'EP-3140NA-02', '32', 'DESCARGAMÁQUINAS DO PÁTIOEP-3140NA-02ELEVAÇÃO DA LANÇASISTEMA HIDRÁULICO - BOMBA - 02');
INSERT INTO `subconjunto` VALUES ('142', 'ACIONAMENTO - 01\r\n', 'GIRO', 'DESCARGA', 'MÁQUINAS DO PÁTIO', 'EP-3140NA-02', '33', 'DESCARGAMÁQUINAS DO PÁTIOEP-3140NA-02GIROACIONAMENTO - 01');
INSERT INTO `subconjunto` VALUES ('143', 'ACIONAMENTO - 02\r\n', 'GIRO', 'DESCARGA', 'MÁQUINAS DO PÁTIO', 'EP-3140NA-02', '33', 'DESCARGAMÁQUINAS DO PÁTIOEP-3140NA-02GIROACIONAMENTO - 02');
INSERT INTO `subconjunto` VALUES ('144', 'ACIONAMENTO - 02\r\n', 'GIRO', 'DESCARGA', 'MÁQUINAS DO PÁTIO', 'EP-3140NA-02', '33', 'DESCARGAMÁQUINAS DO PÁTIOEP-3140NA-02GIROACIONAMENTO - 03');
INSERT INTO `subconjunto` VALUES ('145', 'ACIONAMENTO - 03\r\n', 'TRANSPORTADOR DA LANÇA', 'DESCARGA', 'MÁQUINAS DO PÁTIO', 'EP-3140NA-02', '34', 'DESCARGAMÁQUINAS DO PÁTIOEP-3140NA-02TRANSPORTADOR DA LANÇAACIONAMENTO');
INSERT INTO `subconjunto` VALUES ('146', 'ACIONAMENTO\r\n', 'TRANSPORTADOR DA LANÇA', 'DESCARGA', 'MÁQUINAS DO PÁTIO', 'EP-3140NA-02', '34', 'DESCARGAMÁQUINAS DO PÁTIOEP-3140NA-02TRANSPORTADOR DA LANÇATAMBOR DE ACIONAMENTO');
INSERT INTO `subconjunto` VALUES ('147', 'TAMBOR DE ACIONAMENTO\r\n', 'TRANSPORTADOR DA LANÇA', 'DESCARGA', 'MÁQUINAS DO PÁTIO', 'EP-3140NA-02', '34', 'DESCARGAMÁQUINAS DO PÁTIOEP-3140NA-02TRANSPORTADOR DA LANÇATAMBOR DE RETORNO');
INSERT INTO `subconjunto` VALUES ('148', 'TAMBOR DE RETORNO\r\n', 'MANGUEIRA DE ÁGUA', 'DESCARGA', 'MÁQUINAS DO PÁTIO', 'EP-3140NA-02', '35', 'DESCARGAMÁQUINAS DO PÁTIOEP-3140NA-02MANGUEIRA DE ÁGUAACIONAMENTO');
INSERT INTO `subconjunto` VALUES ('149', 'ACIONAMENTO\r\n', 'MANGUERA ELÉTRICA', 'DESCARGA', 'MÁQUINAS DO PÁTIO', 'EP-3140NA-02', '36', 'DESCARGAMÁQUINAS DO PÁTIOEP-3140NA-02MANGUERA ELÉTRICAACIONAMENTO');
INSERT INTO `subconjunto` VALUES ('150', 'ACIONAMENTO\r\n', 'ELEVAÇÃO DA LANÇA', 'EMBARQUE', 'MÁQUINAS DO PÁTIO', 'RC-3140NA-01', '37', 'EMBARQUEMÁQUINAS DO PÁTIORC-3140NA-01ELEVAÇÃO DA LANÇASISTEMA HIDRÁULICO - BOMBA - 01');
INSERT INTO `subconjunto` VALUES ('151', 'SISTEMA HIDRÁULICO - BOMBA - 01\r\n', 'ELEVAÇÃO DA LANÇA', 'EMBARQUE', 'MÁQUINAS DO PÁTIO', 'RC-3140NA-01', '37', 'EMBARQUEMÁQUINAS DO PÁTIORC-3140NA-01ELEVAÇÃO DA LANÇASISTEMA HIDRÁULICO - BOMBA - 02');
INSERT INTO `subconjunto` VALUES ('152', 'SISTEMA HIDRÁULICO - BOMBA - 02\r\n', 'RODA DE CAÇAMBAS', 'EMBARQUE', 'MÁQUINAS DO PÁTIO', 'RC-3140NA-01', '38', 'EMBARQUEMÁQUINAS DO PÁTIORC-3140NA-01RODA DE CAÇAMBASSISTEMA HIDRÁULICO - BOMBA - 01');
INSERT INTO `subconjunto` VALUES ('153', 'SISTEMA HIDRÁULICO - BOMBA - 01\r\n', 'RODA DE CAÇAMBAS', 'EMBARQUE', 'MÁQUINAS DO PÁTIO', 'RC-3140NA-01', '38', 'EMBARQUEMÁQUINAS DO PÁTIORC-3140NA-01RODA DE CAÇAMBASSISTEMA HIDRÁULICO - BOMBA - 02');
INSERT INTO `subconjunto` VALUES ('154', 'SISTEMA HIDRÁULICO - BOMBA - 02\r\n', 'RODA DE CAÇAMBAS', 'EMBARQUE', 'MÁQUINAS DO PÁTIO', 'RC-3140NA-01', '38', 'EMBARQUEMÁQUINAS DO PÁTIORC-3140NA-01RODA DE CAÇAMBASACIONAMENTO');
INSERT INTO `subconjunto` VALUES ('155', 'ACIONAMENTO\r\n', 'RODA DE CAÇAMBAS', 'EMBARQUE', 'MÁQUINAS DO PÁTIO', 'RC-3140NA-01', '38', 'EMBARQUEMÁQUINAS DO PÁTIORC-3140NA-01RODA DE CAÇAMBASMANCAL DA RODA');
INSERT INTO `subconjunto` VALUES ('156', 'MANCAL DA RODA\r\n', 'GIRO', 'EMBARQUE', 'MÁQUINAS DO PÁTIO', 'RC-3140NA-01', '39', 'EMBARQUEMÁQUINAS DO PÁTIORC-3140NA-01GIROACIONAMENTO - 01');
INSERT INTO `subconjunto` VALUES ('157', 'ACIONAMENTO - 01\r\n', 'GIRO', 'EMBARQUE', 'MÁQUINAS DO PÁTIO', 'RC-3140NA-01', '39', 'EMBARQUEMÁQUINAS DO PÁTIORC-3140NA-01GIROACIONAMENTO - 02');
INSERT INTO `subconjunto` VALUES ('158', 'ACIONAMENTO - 02\r\n', 'GIRO', 'EMBARQUE', 'MÁQUINAS DO PÁTIO', 'RC-3140NA-01', '39', 'EMBARQUEMÁQUINAS DO PÁTIORC-3140NA-01GIROACIONAMENTO - 03');
INSERT INTO `subconjunto` VALUES ('159', 'ACIONAMENTO - 03\r\n', 'MANGUEIRA DE ÁGUA', 'EMBARQUE', 'MÁQUINAS DO PÁTIO', 'RC-3140NA-01', '40', 'EMBARQUEMÁQUINAS DO PÁTIORC-3140NA-01MANGUEIRA DE ÁGUAACIONAMENTO');
INSERT INTO `subconjunto` VALUES ('160', 'ACIONAMENTO\r\n', 'MANGUERA ELÉTRICA', 'EMBARQUE', 'MÁQUINAS DO PÁTIO', 'RC-3140NA-01', '41', 'EMBARQUEMÁQUINAS DO PÁTIORC-3140NA-01MANGUERA ELÉTRICAACIONAMENTO');
INSERT INTO `subconjunto` VALUES ('161', 'ACIONAMENTO\r\n', 'TRANSPORTADOR DA LANÇA', 'EMBARQUE', 'MÁQUINAS DO PÁTIO', 'RC-3140NA-01', '42', 'EMBARQUEMÁQUINAS DO PÁTIORC-3140NA-01TRANSPORTADOR DA LANÇAACIONAMENTO');
INSERT INTO `subconjunto` VALUES ('162', 'ACIONAMENTO\r\n', 'TRANSPORTADOR DA LANÇA', 'EMBARQUE', 'MÁQUINAS DO PÁTIO', 'RC-3140NA-01', '42', 'EMBARQUEMÁQUINAS DO PÁTIORC-3140NA-01TRANSPORTADOR DA LANÇATAMBOR DE ACIONAMENTO');
INSERT INTO `subconjunto` VALUES ('163', 'TAMBOR DE ACIONAMENTO\r\n', 'TRANSPORTADOR DA LANÇA', 'EMBARQUE', 'MÁQUINAS DO PÁTIO', 'RC-3140NA-01', '42', 'EMBARQUEMÁQUINAS DO PÁTIORC-3140NA-01TRANSPORTADOR DA LANÇATAMBOR DE RETORNO');
INSERT INTO `subconjunto` VALUES ('164', 'TAMBOR DE RETORNO\r\n', 'TROCADOR DE CALOR', 'EMBARQUE', 'MÁQUINAS DO PÁTIO', 'RC-3140NA-01', '43', 'EMBARQUEMÁQUINAS DO PÁTIORC-3140NA-01TROCADOR DE CALORVENTILADOR');
INSERT INTO `subconjunto` VALUES ('165', 'VENTILADOR\r\n', 'ELEVAÇÃO DA LANÇA', 'EMBARQUE', 'MÁQUINAS DO PÁTIO', 'RC-3140NA-02', '44', 'EMBARQUEMÁQUINAS DO PÁTIORC-3140NA-02ELEVAÇÃO DA LANÇASISTEMA HIDRÁULICO - BOMBA - 01');
INSERT INTO `subconjunto` VALUES ('166', 'SISTEMA HIDRÁULICO - BOMBA - 01\r\n', 'ELEVAÇÃO DA LANÇA', 'EMBARQUE', 'MÁQUINAS DO PÁTIO', 'RC-3140NA-02', '44', 'EMBARQUEMÁQUINAS DO PÁTIORC-3140NA-02ELEVAÇÃO DA LANÇASISTEMA HIDRÁULICO - BOMBA - 02');
INSERT INTO `subconjunto` VALUES ('167', 'SISTEMA HIDRÁULICO - BOMBA - 02\r\n', 'RODA DE CAÇAMBAS', 'EMBARQUE', 'MÁQUINAS DO PÁTIO', 'RC-3140NA-02', '45', 'EMBARQUEMÁQUINAS DO PÁTIORC-3140NA-02RODA DE CAÇAMBASSISTEMA HIDRÁULICO - BOMBA - 01');
INSERT INTO `subconjunto` VALUES ('168', 'SISTEMA HIDRÁULICO - BOMBA - 01\r\n', 'RODA DE CAÇAMBAS', 'EMBARQUE', 'MÁQUINAS DO PÁTIO', 'RC-3140NA-02', '45', 'EMBARQUEMÁQUINAS DO PÁTIORC-3140NA-02RODA DE CAÇAMBASSISTEMA HIDRÁULICO - BOMBA - 02');
INSERT INTO `subconjunto` VALUES ('169', 'SISTEMA HIDRÁULICO - BOMBA - 02\r\n', 'RODA DE CAÇAMBAS', 'EMBARQUE', 'MÁQUINAS DO PÁTIO', 'RC-3140NA-02', '45', 'EMBARQUEMÁQUINAS DO PÁTIORC-3140NA-02RODA DE CAÇAMBASACIONAMENTO');
INSERT INTO `subconjunto` VALUES ('170', 'ACIONAMENTO\r\n', 'RODA DE CAÇAMBAS', 'EMBARQUE', 'MÁQUINAS DO PÁTIO', 'RC-3140NA-02', '45', 'EMBARQUEMÁQUINAS DO PÁTIORC-3140NA-02RODA DE CAÇAMBASMANCAL DA RODA');
INSERT INTO `subconjunto` VALUES ('171', 'MANCAL DA RODA\r\n', 'GIRO', 'EMBARQUE', 'MÁQUINAS DO PÁTIO', 'RC-3140NA-02', '46', 'EMBARQUEMÁQUINAS DO PÁTIORC-3140NA-02GIROACIONAMENTO - 01');
INSERT INTO `subconjunto` VALUES ('172', 'ACIONAMENTO - 01\r\n', 'GIRO', 'EMBARQUE', 'MÁQUINAS DO PÁTIO', 'RC-3140NA-02', '46', 'EMBARQUEMÁQUINAS DO PÁTIORC-3140NA-02GIROACIONAMENTO - 02');
INSERT INTO `subconjunto` VALUES ('173', 'ACIONAMENTO - 02\r\n', 'GIRO', 'EMBARQUE', 'MÁQUINAS DO PÁTIO', 'RC-3140NA-02', '46', 'EMBARQUEMÁQUINAS DO PÁTIORC-3140NA-02GIROACIONAMENTO - 03');
INSERT INTO `subconjunto` VALUES ('174', 'ACIONAMENTO - 03\r\n', 'MANGUEIRA DE ÁGUA', 'EMBARQUE', 'MÁQUINAS DO PÁTIO', 'RC-3140NA-02', '47', 'EMBARQUEMÁQUINAS DO PÁTIORC-3140NA-02MANGUEIRA DE ÁGUAACIONAMENTO');
INSERT INTO `subconjunto` VALUES ('177', 'TAMBOR DE ACIONAMENTO\r\n', 'MANGUERA ELÉTRICA', 'EMBARQUE', 'MÁQUINAS DO PÁTIO', 'RC-3140NA-02', '48', 'EMBARQUEMÁQUINAS DO PÁTIORC-3140NA-02MANGUERA ELÉTRICAACIONAMENTO');
INSERT INTO `subconjunto` VALUES ('178', 'TAMBOR DE RETORNO\r\n', 'TRANSPORTADOR DA LANÇA', 'EMBARQUE', 'MÁQUINAS DO PÁTIO', 'RC-3140NA-02', '49', 'EMBARQUEMÁQUINAS DO PÁTIORC-3140NA-02TRANSPORTADOR DA LANÇAACIONAMENTO');
INSERT INTO `subconjunto` VALUES ('179', 'VENTILADOR\r\n', 'TRANSPORTADOR DA LANÇA', 'EMBARQUE', 'MÁQUINAS DO PÁTIO', 'RC-3140NA-02', '49', 'EMBARQUEMÁQUINAS DO PÁTIORC-3140NA-02TRANSPORTADOR DA LANÇATAMBOR DE ACIONAMENTO');
INSERT INTO `subconjunto` VALUES ('180', 'ACIONAMENTO - 01\r\n', 'TRANSPORTADOR DA LANÇA', 'EMBARQUE', 'MÁQUINAS DO PÁTIO', 'RC-3140NA-02', '49', 'EMBARQUEMÁQUINAS DO PÁTIORC-3140NA-02TRANSPORTADOR DA LANÇATAMBOR DE RETORNO');
INSERT INTO `subconjunto` VALUES ('181', 'ACIONAMENTO - 02\r\n', 'TROCADOR DE CALOR', 'EMBARQUE', 'MÁQUINAS DO PÁTIO', 'RC-3140NA-02', '50', 'EMBARQUEMÁQUINAS DO PÁTIORC-3140NA-02TROCADOR DE CALORVENTILADOR');
INSERT INTO `subconjunto` VALUES ('182', 'ACIONAMENTO - 03\r\n', 'GIRO', 'EMBARQUE', 'PÍER', 'CN-3220NA-01', '51', 'EMBARQUEPÍERCN-3220NA-01GIROACIONAMENTO - 01');
INSERT INTO `subconjunto` VALUES ('183', 'ACIONAMENTO - 04\r\n', 'GIRO', 'EMBARQUE', 'PÍER', 'CN-3220NA-01', '51', 'EMBARQUEPÍERCN-3220NA-01GIROACIONAMENTO - 02');
INSERT INTO `subconjunto` VALUES ('184', 'SISTEMA HIDRÁULICO - BOMBA - 01\r\n', 'GIRO', 'EMBARQUE', 'PÍER', 'CN-3220NA-01', '51', 'EMBARQUEPÍERCN-3220NA-01GIROACIONAMENTO - 03');
INSERT INTO `subconjunto` VALUES ('185', 'SISTEMA HIDRÁULICO - BOMBA - 02\r\n', 'GIRO', 'EMBARQUE', 'PÍER', 'CN-3220NA-01', '51', 'EMBARQUEPÍERCN-3220NA-01GIROACIONAMENTO - 04');
INSERT INTO `subconjunto` VALUES ('186', 'TROCADOR DE CALOR\r\n', 'ELEVAÇÃO DA LANÇA', 'EMBARQUE', 'PÍER', 'CN-3220NA-01', '52', 'EMBARQUEPÍERCN-3220NA-01ELEVAÇÃO DA LANÇASISTEMA HIDRÁULICO - BOMBA - 01');
INSERT INTO `subconjunto` VALUES ('187', 'ACIONAMENTO 1\r\n', 'ELEVAÇÃO DA LANÇA', 'EMBARQUE', 'PÍER', 'CN-3220NA-01', '52', 'EMBARQUEPÍERCN-3220NA-01ELEVAÇÃO DA LANÇASISTEMA HIDRÁULICO - BOMBA - 02');
INSERT INTO `subconjunto` VALUES ('188', 'ACIONAMENTO 2\r\n', 'ELEVAÇÃO DA LANÇA', 'EMBARQUE', 'PÍER', 'CN-3220NA-01', '52', 'EMBARQUEPÍERCN-3220NA-01ELEVAÇÃO DA LANÇATROCADOR DE CALOR');
INSERT INTO `subconjunto` VALUES ('189', 'TAMBOR DE ACIONAMENTO\r\n', 'TRANSPORTADOR DA LANÇA', 'EMBARQUE', 'PÍER', 'CN-3220NA-01', '53', 'EMBARQUEPÍERCN-3220NA-01TRANSPORTADOR DA LANÇAACIONAMENTO 1');
INSERT INTO `subconjunto` VALUES ('190', 'TAMBOR DE DESCARGA\r\n', 'TRANSPORTADOR DA LANÇA', 'EMBARQUE', 'PÍER', 'CN-3220NA-01', '53', 'EMBARQUEPÍERCN-3220NA-01TRANSPORTADOR DA LANÇAACIONAMENTO 2');
INSERT INTO `subconjunto` VALUES ('191', 'TAMBOR DE ESTICAMENTO\r\n', 'TRANSPORTADOR DA LANÇA', 'EMBARQUE', 'PÍER', 'CN-3220NA-01', '53', 'EMBARQUEPÍERCN-3220NA-01TRANSPORTADOR DA LANÇATAMBOR DE ACIONAMENTO');
INSERT INTO `subconjunto` VALUES ('192', 'TAMBOR DE DESVIO\r\n', 'TRANSPORTADOR DA LANÇA', 'EMBARQUE', 'PÍER', 'CN-3220NA-01', '53', 'EMBARQUEPÍERCN-3220NA-01TRANSPORTADOR DA LANÇATAMBOR DE DESCARGA');
INSERT INTO `subconjunto` VALUES ('193', 'ACIONAMENTO 1 - BOMBA HIDRÁULICA LE\r\n', 'TRANSPORTADOR DA LANÇA', 'EMBARQUE', 'PÍER', 'CN-3220NA-01', '53', 'EMBARQUEPÍERCN-3220NA-01TRANSPORTADOR DA LANÇATAMBOR DE ESTICAMENTO');
INSERT INTO `subconjunto` VALUES ('194', 'ACIONAMENTO 2 - BOMBA HIDRÁULICA LD\r\n', 'TRANSPORTADOR DA LANÇA', 'EMBARQUE', 'PÍER', 'CN-3220NA-01', '53', 'EMBARQUEPÍERCN-3220NA-01TRANSPORTADOR DA LANÇATAMBOR DE DESVIO');
INSERT INTO `subconjunto` VALUES ('195', 'BOMBA DE ASPERSÃO\r\n', 'TRANSPORTADOR DA LANÇA', 'EMBARQUE', 'PÍER', 'CN-3220NA-01', '53', 'EMBARQUEPÍERCN-3220NA-01TRANSPORTADOR DA LANÇAACIONAMENTO 1 - BOMBA HIDRÁULICA LE');
INSERT INTO `subconjunto` VALUES ('196', 'ACIONAMENTO - 01\r\n', 'TRANSPORTADOR DA LANÇA', 'EMBARQUE', 'PÍER', 'CN-3220NA-01', '53', 'EMBARQUEPÍERCN-3220NA-01TRANSPORTADOR DA LANÇAACIONAMENTO 2 - BOMBA HIDRÁULICA LD');
INSERT INTO `subconjunto` VALUES ('197', 'ACIONAMENTO - 02\r\n', 'SISTEMA DE ASPERSÃO', 'EMBARQUE', 'PÍER', 'CN-3220NA-01', '54', 'EMBARQUEPÍERCN-3220NA-01SISTEMA DE ASPERSÃOBOMBA DE ASPERSÃO');
INSERT INTO `subconjunto` VALUES ('198', 'ACIONAMENTO - 03\r\n', 'LANÇA', 'EMBARQUE', 'PÍER', 'CN-3220NA-01', '55', 'EMBARQUEPÍERCN-3220NA-01LANÇAACIONAMENTO - 01');
INSERT INTO `subconjunto` VALUES ('199', 'ACIONAMENTO - 04\r\n', 'LANÇA', 'EMBARQUE', 'PÍER', 'CN-3220NA-01', '55', 'EMBARQUEPÍERCN-3220NA-01LANÇAACIONAMENTO - 02');
INSERT INTO `subconjunto` VALUES ('200', 'ACIONAMENTO - 01\r\n', 'LANÇA', 'EMBARQUE', 'PÍER', 'CN-3220NA-01', '55', 'EMBARQUEPÍERCN-3220NA-01LANÇAACIONAMENTO - 03');
INSERT INTO `subconjunto` VALUES ('201', 'ACIONAMENTO - 02\r\n', 'LANÇA', 'EMBARQUE', 'PÍER', 'CN-3220NA-01', '55', 'EMBARQUEPÍERCN-3220NA-01LANÇAACIONAMENTO - 04');
INSERT INTO `subconjunto` VALUES ('202', 'ACIONAMENTO - 03\r\n', 'GIRO', 'EMBARQUE', 'PÍER', 'CN-3220NA-02', '56', 'EMBARQUEPÍERCN-3220NA-02GIROACIONAMENTO - 01');
INSERT INTO `subconjunto` VALUES ('203', 'ACIONAMENTO - 04\r\n', 'GIRO', 'EMBARQUE', 'PÍER', 'CN-3220NA-02', '56', 'EMBARQUEPÍERCN-3220NA-02GIROACIONAMENTO - 02');
INSERT INTO `subconjunto` VALUES ('204', 'SISTEMA HIDRÁULICO - BOMBA - 01\r\n', 'GIRO', 'EMBARQUE', 'PÍER', 'CN-3220NA-02', '56', 'EMBARQUEPÍERCN-3220NA-02GIROACIONAMENTO - 03');
INSERT INTO `subconjunto` VALUES ('205', 'SISTEMA HIDRÁULICO - BOMBA - 02\r\n', 'GIRO', 'EMBARQUE', 'PÍER', 'CN-3220NA-02', '56', 'EMBARQUEPÍERCN-3220NA-02GIROACIONAMENTO - 04');
INSERT INTO `subconjunto` VALUES ('206', 'TROCADOR DE CALOR\r\n', 'ELEVAÇÃO DA LANÇA', 'EMBARQUE', 'PÍER', 'CN-3220NA-02', '57', 'EMBARQUEPÍERCN-3220NA-02ELEVAÇÃO DA LANÇASISTEMA HIDRÁULICO - BOMBA - 01');
INSERT INTO `subconjunto` VALUES ('207', 'ACIONAMENTO 1\r\n', 'ELEVAÇÃO DA LANÇA', 'EMBARQUE', 'PÍER', 'CN-3220NA-02', '57', 'EMBARQUEPÍERCN-3220NA-02ELEVAÇÃO DA LANÇASISTEMA HIDRÁULICO - BOMBA - 02');
INSERT INTO `subconjunto` VALUES ('208', 'ACIONAMENTO 2\r\n', 'ELEVAÇÃO DA LANÇA', 'EMBARQUE', 'PÍER', 'CN-3220NA-02', '57', 'EMBARQUEPÍERCN-3220NA-02ELEVAÇÃO DA LANÇATROCADOR DE CALOR');
INSERT INTO `subconjunto` VALUES ('209', 'TAMBOR DE ACIONAMENTO\r\n', 'TRANSPORTADOR DA LANÇA', 'EMBARQUE', 'PÍER', 'CN-3220NA-02', '58', 'EMBARQUEPÍERCN-3220NA-02TRANSPORTADOR DA LANÇAACIONAMENTO 1');
INSERT INTO `subconjunto` VALUES ('210', 'TAMBOR DE DESCARGA\r\n', 'TRANSPORTADOR DA LANÇA', 'EMBARQUE', 'PÍER', 'CN-3220NA-02', '58', 'EMBARQUEPÍERCN-3220NA-02TRANSPORTADOR DA LANÇAACIONAMENTO 2');
INSERT INTO `subconjunto` VALUES ('211', 'TAMBOR DE ESTICAMENTO\r\n', 'TRANSPORTADOR DA LANÇA', 'EMBARQUE', 'PÍER', 'CN-3220NA-02', '58', 'EMBARQUEPÍERCN-3220NA-02TRANSPORTADOR DA LANÇATAMBOR DE ACIONAMENTO');
INSERT INTO `subconjunto` VALUES ('212', 'TAMBOR DE DESVIO\r\n', 'TRANSPORTADOR DA LANÇA', 'EMBARQUE', 'PÍER', 'CN-3220NA-02', '58', 'EMBARQUEPÍERCN-3220NA-02TRANSPORTADOR DA LANÇATAMBOR DE DESCARGA');
INSERT INTO `subconjunto` VALUES ('213', 'ACIONAMENTO 1 - BOMBA HIDRÁULICA LE\r\n', 'TRANSPORTADOR DA LANÇA', 'EMBARQUE', 'PÍER', 'CN-3220NA-02', '58', 'EMBARQUEPÍERCN-3220NA-02TRANSPORTADOR DA LANÇATAMBOR DE ESTICAMENTO');
INSERT INTO `subconjunto` VALUES ('214', 'ACIONAMENTO 2 - BOMBA HIDRÁULICA LD\r\n', 'TRANSPORTADOR DA LANÇA', 'EMBARQUE', 'PÍER', 'CN-3220NA-02', '58', 'EMBARQUEPÍERCN-3220NA-02TRANSPORTADOR DA LANÇATAMBOR DE DESVIO');
INSERT INTO `subconjunto` VALUES ('215', 'BOMBA DE ASPERSÃO\r\n', 'TRANSPORTADOR DA LANÇA', 'EMBARQUE', 'PÍER', 'CN-3220NA-02', '58', 'EMBARQUEPÍERCN-3220NA-02TRANSPORTADOR DA LANÇAACIONAMENTO 1 - BOMBA HIDRÁULICA LE');
INSERT INTO `subconjunto` VALUES ('216', 'ACIONAMENTO - 01\r\n', 'TRANSPORTADOR DA LANÇA', 'EMBARQUE', 'PÍER', 'CN-3220NA-02', '58', 'EMBARQUEPÍERCN-3220NA-02TRANSPORTADOR DA LANÇAACIONAMENTO 2 - BOMBA HIDRÁULICA LD');
INSERT INTO `subconjunto` VALUES ('217', 'ACIONAMENTO - 02\r\n', 'SISTEMA DE ASPERSÃO', 'EMBARQUE', 'PÍER', 'CN-3220NA-02', '59', 'EMBARQUEPÍERCN-3220NA-02SISTEMA DE ASPERSÃOBOMBA DE ASPERSÃO');
INSERT INTO `subconjunto` VALUES ('218', 'ACIONAMENTO - 03\r\n', 'LANÇA', 'EMBARQUE', 'PÍER', 'CN-3220NA-02', '60', 'EMBARQUEPÍERCN-3220NA-02LANÇAACIONAMENTO - 01');
INSERT INTO `subconjunto` VALUES ('219', 'ACIONAMENTO - 04\r\n', 'LANÇA', 'EMBARQUE', 'PÍER', 'CN-3220NA-02', '60', 'EMBARQUEPÍERCN-3220NA-02LANÇAACIONAMENTO - 02');
INSERT INTO `subconjunto` VALUES ('220', 'AGITADOR\r\n', 'LANÇA', 'EMBARQUE', 'PÍER', 'CN-3220NA-02', '60', 'EMBARQUEPÍERCN-3220NA-02LANÇAACIONAMENTO - 03');
INSERT INTO `subconjunto` VALUES ('221', 'AGITADOR\r\n', 'LANÇA', 'EMBARQUE', 'PÍER', 'CN-3220NA-02', '60', 'EMBARQUEPÍERCN-3220NA-02LANÇAACIONAMENTO - 04');
INSERT INTO `subconjunto` VALUES ('222', 'AGITADOR\r\n', 'AGITADOR', 'TRATAMENTO DE ÁGUA', 'DESSALINIZAÇÃO', 'AGITADOR 01', '61', 'TRATAMENTO DE ÁGUADESSALINIZAÇÃOAGITADOR 01AGITADORAGITADOR');
INSERT INTO `subconjunto` VALUES ('223', 'BOMBA DE ÁGUA\r\n', 'AGITADOR', 'TRATAMENTO DE ÁGUA', 'DESSALINIZAÇÃO', 'AGITADOR 02', '62', 'TRATAMENTO DE ÁGUADESSALINIZAÇÃOAGITADOR 02AGITADORAGITADOR');
INSERT INTO `subconjunto` VALUES ('224', 'BOMBA DE ÁGUA\r\n', 'AGITADOR', 'TRATAMENTO DE ÁGUA', 'DESSALINIZAÇÃO', 'AGITADOR 03', '63', 'TRATAMENTO DE ÁGUADESSALINIZAÇÃOAGITADOR 03AGITADORAGITADOR');
INSERT INTO `subconjunto` VALUES ('225', 'BOMBA DE ÁGUA\r\n', 'BOMBA DE ÁGUA', 'TRATAMENTO DE ÁGUA', 'DESSALINIZAÇÃO', 'BOMBA DE ALTA PRESSÃO 01', '64', 'TRATAMENTO DE ÁGUADESSALINIZAÇÃOBOMBA DE ALTA PRESSÃO 01BOMBA DE ÁGUABOMBA DE ÁGUA');
INSERT INTO `subconjunto` VALUES ('226', 'BOMBA DE ÁGUA\r\n', 'BOMBA DE ÁGUA', 'TRATAMENTO DE ÁGUA', 'DESSALINIZAÇÃO', 'BOMBA DE ALTA PRESSÃO 02', '65', 'TRATAMENTO DE ÁGUADESSALINIZAÇÃOBOMBA DE ALTA PRESSÃO 02BOMBA DE ÁGUABOMBA DE ÁGUA');
INSERT INTO `subconjunto` VALUES ('227', 'BOMBA DE ÁGUA\r\n', 'BOMBA DE ÁGUA', 'TRATAMENTO DE ÁGUA', 'DESSALINIZAÇÃO', 'BOMBA BACKWASH 01', '66', 'TRATAMENTO DE ÁGUADESSALINIZAÇÃOBOMBA BACKWASH 01BOMBA DE ÁGUABOMBA DE ÁGUA');
INSERT INTO `subconjunto` VALUES ('228', 'BOMBA DE ÁGUA\r\n', 'BOMBA DE ÁGUA', 'TRATAMENTO DE ÁGUA', 'DESSALINIZAÇÃO', 'BOMBA BACKWASH 02', '67', 'TRATAMENTO DE ÁGUADESSALINIZAÇÃOBOMBA BACKWASH 02BOMBA DE ÁGUABOMBA DE ÁGUA');
INSERT INTO `subconjunto` VALUES ('229', 'BOMBA DE ÁGUA\r\n', 'BOMBA DE ÁGUA', 'TRATAMENTO DE ÁGUA', 'DESSALINIZAÇÃO', 'BOMBA BACKWASH 03', '68', 'TRATAMENTO DE ÁGUADESSALINIZAÇÃOBOMBA BACKWASH 03BOMBA DE ÁGUABOMBA DE ÁGUA');
INSERT INTO `subconjunto` VALUES ('230', 'BOMBA DE ÁGUA\r\n', 'BOMBA DE ÁGUA', 'TRATAMENTO DE ÁGUA', 'DESSALINIZAÇÃO', 'BOMBA BACKWASH 04', '69', 'TRATAMENTO DE ÁGUADESSALINIZAÇÃOBOMBA BACKWASH 04BOMBA DE ÁGUABOMBA DE ÁGUA');
INSERT INTO `subconjunto` VALUES ('231', 'BOMBA DE ÁGUA\r\n', 'BOMBA DE ÁGUA', 'TRATAMENTO DE ÁGUA', 'DESSALINIZAÇÃO', 'BOMBA BOOSTER 01', '70', 'TRATAMENTO DE ÁGUADESSALINIZAÇÃOBOMBA BOOSTER 01BOMBA DE ÁGUABOMBA DE ÁGUA');
INSERT INTO `subconjunto` VALUES ('232', 'BOMBA DE ÁGUA\r\n', 'BOMBA DE ÁGUA', 'TRATAMENTO DE ÁGUA', 'DESSALINIZAÇÃO', 'BOMBA BOOSTER 02', '71', 'TRATAMENTO DE ÁGUADESSALINIZAÇÃOBOMBA BOOSTER 02BOMBA DE ÁGUABOMBA DE ÁGUA');
INSERT INTO `subconjunto` VALUES ('233', 'BOMBA DE ÁGUA\r\n', 'BOMBA DE ÁGUA', 'TRATAMENTO DE ÁGUA', 'DESSALINIZAÇÃO', 'BB 01 - ÁGUA FILTRADA', '72', 'TRATAMENTO DE ÁGUADESSALINIZAÇÃOBB 01 - ÁGUA FILTRADABOMBA DE ÁGUABOMBA DE ÁGUA');
INSERT INTO `subconjunto` VALUES ('234', 'BOMBA DE ÁGUA\r\n', 'BOMBA DE ÁGUA', 'TRATAMENTO DE ÁGUA', 'DESSALINIZAÇÃO', 'BB 02 - ÁGUA FILTRADA', '73', 'TRATAMENTO DE ÁGUADESSALINIZAÇÃOBB 02 - ÁGUA FILTRADABOMBA DE ÁGUABOMBA DE ÁGUA');
INSERT INTO `subconjunto` VALUES ('235', 'BOMBA DE ÁGUA\r\n', 'BOMBA DE ÁGUA', 'TRATAMENTO DE ÁGUA', 'DESSALINIZAÇÃO', 'BB 03 - ÁGUA FILTRADA', '74', 'TRATAMENTO DE ÁGUADESSALINIZAÇÃOBB 03 - ÁGUA FILTRADABOMBA DE ÁGUABOMBA DE ÁGUA');
INSERT INTO `subconjunto` VALUES ('236', 'BOMBA DE ÁGUA\r\n', 'BOMBA DE ÁGUA', 'TRATAMENTO DE ÁGUA', 'DESSALINIZAÇÃO', 'FLUSH PUMP', '75', 'TRATAMENTO DE ÁGUADESSALINIZAÇÃOFLUSH PUMPBOMBA DE ÁGUABOMBA DE ÁGUA');
INSERT INTO `subconjunto` VALUES ('237', 'BOMBA DE ÁGUA\r\n', 'BOMBA DE ÁGUA', 'TRATAMENTO DE ÁGUA', 'DESSALINIZAÇÃO', 'CIP PUMP', '76', 'TRATAMENTO DE ÁGUADESSALINIZAÇÃOCIP PUMPBOMBA DE ÁGUABOMBA DE ÁGUA');
INSERT INTO `subconjunto` VALUES ('238', 'BOMBA DE ÁGUA\r\n', 'BOMBA DE ÁGUA', 'TRATAMENTO DE ÁGUA', 'DESSALINIZAÇÃO', 'BA-3180NA-01', '77', 'TRATAMENTO DE ÁGUADESSALINIZAÇÃOBA-3180NA-01BOMBA DE ÁGUABOMBA DE ÁGUA');
INSERT INTO `subconjunto` VALUES ('239', 'BOMBA DE ÁGUA\r\n', 'BOMBA DE ÁGUA', 'TRATAMENTO DE ÁGUA', 'DESSALINIZAÇÃO', 'BA-3180NA-02', '78', 'TRATAMENTO DE ÁGUADESSALINIZAÇÃOBA-3180NA-02BOMBA DE ÁGUABOMBA DE ÁGUA');
INSERT INTO `subconjunto` VALUES ('240', 'BOMBA DE ÁGUA\r\n', 'BOMBA DE ÁGUA', 'TRATAMENTO DE ÁGUA', 'DESSALINIZAÇÃO', 'BB 01 - ÁGUA TRATADA', '79', 'TRATAMENTO DE ÁGUADESSALINIZAÇÃOBB 01 - ÁGUA TRATADABOMBA DE ÁGUABOMBA DE ÁGUA');
INSERT INTO `subconjunto` VALUES ('241', 'BOMBA DE ÁGUA\r\n', 'BOMBA DE ÁGUA', 'TRATAMENTO DE ÁGUA', 'DESSALINIZAÇÃO', 'BB 02 - ÁGUA TRATADA', '80', 'TRATAMENTO DE ÁGUADESSALINIZAÇÃOBB 02 - ÁGUA TRATADABOMBA DE ÁGUABOMBA DE ÁGUA');
INSERT INTO `subconjunto` VALUES ('242', 'BOMBA DE ÁGUA\r\n', 'BOMBA DE ÁGUA', 'TRATAMENTO DE ÁGUA', 'DESSALINIZAÇÃO', 'BB 03 - ÁGUA TRATADA', '81', 'TRATAMENTO DE ÁGUADESSALINIZAÇÃOBB 03 - ÁGUA TRATADABOMBA DE ÁGUABOMBA DE ÁGUA');
INSERT INTO `subconjunto` VALUES ('243', 'BOMBA DE ÁGUA\r\n', 'BOMBA DE ÁGUA', 'TRATAMENTO DE ÁGUA', 'DECANTAÇÃO', 'BOMBA 007', '82', 'TRATAMENTO DE ÁGUADECANTAÇÃOBOMBA 007BOMBA DE ÁGUABOMBA DE ÁGUA');
INSERT INTO `subconjunto` VALUES ('244', 'BOMBA DE ÁGUA\r\n', 'BOMBA DE ÁGUA', 'TRATAMENTO DE ÁGUA', 'DECANTAÇÃO', 'BOMBA 008', '83', 'TRATAMENTO DE ÁGUADECANTAÇÃOBOMBA 008BOMBA DE ÁGUABOMBA DE ÁGUA');
INSERT INTO `subconjunto` VALUES ('245', 'BOMBA DE ÁGUA\r\n', 'BOMBA DE ÁGUA', 'TRATAMENTO DE ÁGUA', 'DECANTAÇÃO', 'BOMBA 009', '84', 'TRATAMENTO DE ÁGUADECANTAÇÃOBOMBA 009BOMBA DE ÁGUABOMBA DE ÁGUA');
INSERT INTO `subconjunto` VALUES ('246', 'BOMBA DE ÁGUA\r\n', 'BOMBA DE ÁGUA', 'TRATAMENTO DE ÁGUA', 'DECANTAÇÃO', 'BOMBA 010', '85', 'TRATAMENTO DE ÁGUADECANTAÇÃOBOMBA 010BOMBA DE ÁGUABOMBA DE ÁGUA');
INSERT INTO `subconjunto` VALUES ('247', 'BOMBA DE ÁGUA\r\n', 'BOMBA DE ÁGUA', 'TRATAMENTO DE ÁGUA', 'COMBATE A INCÊNDIO', 'BOMBA 01', '86', 'TRATAMENTO DE ÁGUACOMBATE A INCÊNDIOBOMBA 01BOMBA DE ÁGUABOMBA DE ÁGUA');
INSERT INTO `subconjunto` VALUES ('248', 'BOMBA DE ÁGUA\r\n', 'BOMBA DE ÁGUA', 'TRATAMENTO DE ÁGUA', 'COMBATE A INCÊNDIO', 'BOMBA DE COMPENSAÇÃO', '87', 'TRATAMENTO DE ÁGUACOMBATE A INCÊNDIOBOMBA DE COMPENSAÇÃOBOMBA DE ÁGUABOMBA DE ÁGUA');
INSERT INTO `subconjunto` VALUES ('249', 'BOMBA DE ÁGUA\r\n', 'BOMBA DE ÁGUA', 'TRATAMENTO DE ÁGUA', 'COMBATE A INCÊNDIO', 'BOMBA DIESEL', '88', 'TRATAMENTO DE ÁGUACOMBATE A INCÊNDIOBOMBA DIESELBOMBA DE ÁGUABOMBA DE ÁGUA');
INSERT INTO `subconjunto` VALUES ('250', 'BOMBA DE ÁGUA\r\n', 'BOMBA DE ÁGUA', 'TRATAMENTO DE ÁGUA', 'DECANTAÇÃO', 'BA-3180NA-09', '89', 'TRATAMENTO DE ÁGUADECANTAÇÃOBA-3180NA-09BOMBA DE ÁGUABOMBA DE ÁGUA');
INSERT INTO `subconjunto` VALUES ('251', 'BOMBA DE ÁGUA\r\n', 'BOMBA DE ÁGUA', 'TRATAMENTO DE ÁGUA', 'DECANTAÇÃO', 'BA-3180NA-10', '90', 'TRATAMENTO DE ÁGUADECANTAÇÃOBA-3180NA-10BOMBA DE ÁGUABOMBA DE ÁGUA');
INSERT INTO `subconjunto` VALUES ('252', 'BOMBA DE ÁGUA\r\n', 'BOMBA DE ÁGUA', 'TRATAMENTO DE ÁGUA', 'DECANTAÇÃO', 'BA-3180NA-11', '91', 'TRATAMENTO DE ÁGUADECANTAÇÃOBA-3180NA-11BOMBA DE ÁGUABOMBA DE ÁGUA');
INSERT INTO `subconjunto` VALUES ('253', 'BOMBA DE ÁGUA\r\n', 'BOMBA DE ÁGUA', 'TRATAMENTO DE ÁGUA', 'DECANTAÇÃO', 'BA-3180NA-12', '92', 'TRATAMENTO DE ÁGUADECANTAÇÃOBA-3180NA-12BOMBA DE ÁGUABOMBA DE ÁGUA');
INSERT INTO `subconjunto` VALUES ('254', 'BOMBA DE ÁGUA\r\n', 'BOMBA DE ÁGUA', 'TRATAMENTO DE ÁGUA', 'BOMBAS PIER', 'BA-3210NA-01', '93', 'TRATAMENTO DE ÁGUABOMBAS PIERBA-3210NA-01BOMBA DE ÁGUABOMBA DE ÁGUA');
INSERT INTO `subconjunto` VALUES ('255', 'BOMBA DE ÁGUA\r\n', 'BOMBA DE ÁGUA', 'TRATAMENTO DE ÁGUA', 'BOMBAS PIER', 'BA-3210NA-02', '94', 'TRATAMENTO DE ÁGUABOMBAS PIERBA-3210NA-02BOMBA DE ÁGUABOMBA DE ÁGUA');
INSERT INTO `subconjunto` VALUES ('256', 'BOMBA DE ÁGUA\r\n', 'BOMBA DE ÁGUA', 'TRATAMENTO DE ÁGUA', 'BOMBAS PIER', 'BA-3210NA-03', '95', 'TRATAMENTO DE ÁGUABOMBAS PIERBA-3210NA-03BOMBA DE ÁGUABOMBA DE ÁGUA');
INSERT INTO `subconjunto` VALUES ('257', 'BOMBA DE ÁGUA\r\n', 'BOMBA DE ÁGUA', 'TRATAMENTO DE ÁGUA', 'BOMBAS PIER', 'BA-3210NA-04', '96', 'TRATAMENTO DE ÁGUABOMBAS PIERBA-3210NA-04BOMBA DE ÁGUABOMBA DE ÁGUA');
INSERT INTO `subconjunto` VALUES ('258', 'ACIONAMENTO\r\n', 'BOMBA DE ÁGUA', 'TRATAMENTO DE ÁGUA', 'BOMBAS PIER', 'BA-3210NA-05', '97', 'TRATAMENTO DE ÁGUABOMBAS PIERBA-3210NA-05BOMBA DE ÁGUABOMBA DE ÁGUA');
INSERT INTO `subconjunto` VALUES ('259', 'TAMBOR DE ACIONAMENTO\r\n', 'BOMBA DE ÁGUA', 'TRATAMENTO DE ÁGUA', 'BOMBAS PIER', 'BA-3210NA-06', '98', 'TRATAMENTO DE ÁGUABOMBAS PIERBA-3210NA-06BOMBA DE ÁGUABOMBA DE ÁGUA');
INSERT INTO `subconjunto` VALUES ('260', 'TAMBOR DE DESVIO\r\n', 'EXTRATOR DE SUCATA', 'DESCARGA', 'TRANSPORTADORES DESCARGA', 'TR-3120NA-01', '99', 'DESCARGATRANSPORTADORES DESCARGATR-3120NA-01EXTRATOR DE SUCATAACIONAMENTO');
INSERT INTO `subconjunto` VALUES ('261', 'ACIONAMENTO\r\n', 'EXTRATOR DE SUCATA', 'DESCARGA', 'TRANSPORTADORES DESCARGA', 'TR-3120NA-01', '99', 'DESCARGATRANSPORTADORES DESCARGATR-3120NA-01EXTRATOR DE SUCATATAMBOR DE ACIONAMENTO');
INSERT INTO `subconjunto` VALUES ('262', 'TAMBOR DE ACIONAMENTO\r\n', 'EXTRATOR DE SUCATA', 'DESCARGA', 'TRANSPORTADORES DESCARGA', 'TR-3120NA-01', '99', 'DESCARGATRANSPORTADORES DESCARGATR-3120NA-01EXTRATOR DE SUCATATAMBOR DE DESVIO');
INSERT INTO `subconjunto` VALUES ('263', 'TAMBOR DE DESVIO\r\n', 'EXTRATOR DE SUCATA', 'EMBARQUE', 'TRANSPORTADORES PÁTIO', 'TR-3140NA-05', '100', 'EMBARQUETRANSPORTADORES PÁTIOTR-3140NA-05EXTRATOR DE SUCATAACIONAMENTO');
INSERT INTO `subconjunto` VALUES ('264', 'ACIONAMENTO\r\n', 'EXTRATOR DE SUCATA', 'EMBARQUE', 'TRANSPORTADORES PÁTIO', 'TR-3140NA-05', '100', 'EMBARQUETRANSPORTADORES PÁTIOTR-3140NA-05EXTRATOR DE SUCATATAMBOR DE ACIONAMENTO');
INSERT INTO `subconjunto` VALUES ('265', 'TAMBOR DE ACIONAMENTO\r\n', 'EXTRATOR DE SUCATA', 'EMBARQUE', 'TRANSPORTADORES PÁTIO', 'TR-3140NA-05', '100', 'EMBARQUETRANSPORTADORES PÁTIOTR-3140NA-05EXTRATOR DE SUCATATAMBOR DE DESVIO');
INSERT INTO `subconjunto` VALUES ('266', 'TAMBOR DE DESVIO\r\n', 'EXTRATOR DE SUCATA', 'EMBARQUE', 'TRANSPORTADORES PÁTIO', 'TR-3140NA-07', '101', 'EMBARQUETRANSPORTADORES PÁTIOTR-3140NA-07EXTRATOR DE SUCATAACIONAMENTO');
INSERT INTO `subconjunto` VALUES ('267', 'ACIONAMENTO\r\n', 'EXTRATOR DE SUCATA', 'EMBARQUE', 'TRANSPORTADORES PÁTIO', 'TR-3140NA-07', '101', 'EMBARQUETRANSPORTADORES PÁTIOTR-3140NA-07EXTRATOR DE SUCATATAMBOR DE ACIONAMENTO');
INSERT INTO `subconjunto` VALUES ('268', 'TAMBOR DE ACIONAMENTO\r\n', 'EXTRATOR DE SUCATA', 'EMBARQUE', 'TRANSPORTADORES PÁTIO', 'TR-3140NA-07', '101', 'EMBARQUETRANSPORTADORES PÁTIOTR-3140NA-07EXTRATOR DE SUCATATAMBOR DE DESVIO');
INSERT INTO `subconjunto` VALUES ('269', 'TAMBOR DE RETORNO\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'AMOSTRAGEM 01', 'TR-3145NA-01', '102', 'EMBARQUEAMOSTRAGEM 01TR-3145NA-01TRANSPORTADORACIONAMENTO');
INSERT INTO `subconjunto` VALUES ('270', 'ACIONAMENTO\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'AMOSTRAGEM 01', 'TR-3145NA-01', '102', 'EMBARQUEAMOSTRAGEM 01TR-3145NA-01TRANSPORTADORTAMBOR DE ACIONAMENTO');
INSERT INTO `subconjunto` VALUES ('271', 'TAMBOR DE ACIONAMENTO\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'AMOSTRAGEM 01', 'TR-3145NA-01', '102', 'EMBARQUEAMOSTRAGEM 01TR-3145NA-01TRANSPORTADORTAMBOR DE RETORNO');
INSERT INTO `subconjunto` VALUES ('272', 'TAMBOR DE RETORNO\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'AMOSTRAGEM 01', 'TR-3145NA-03', '103', 'EMBARQUEAMOSTRAGEM 01TR-3145NA-03TRANSPORTADORACIONAMENTO');
INSERT INTO `subconjunto` VALUES ('273', 'ACIONAMENTO\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'AMOSTRAGEM 01', 'TR-3145NA-03', '103', 'EMBARQUEAMOSTRAGEM 01TR-3145NA-03TRANSPORTADORTAMBOR DE ACIONAMENTO');
INSERT INTO `subconjunto` VALUES ('274', 'MARTELOS\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'AMOSTRAGEM 01', 'TR-3145NA-03', '103', 'EMBARQUEAMOSTRAGEM 01TR-3145NA-03TRANSPORTADORTAMBOR DE RETORNO');
INSERT INTO `subconjunto` VALUES ('275', 'ACIONAMENTO\r\n', 'BRITADOR', 'EMBARQUE', 'AMOSTRAGEM 01', 'BR-3145NA-01', '104', 'EMBARQUEAMOSTRAGEM 01BR-3145NA-01BRITADORACIONAMENTO');
INSERT INTO `subconjunto` VALUES ('276', 'TAMBOR DE ACIONAMENTO\r\n', 'BRITADOR', 'EMBARQUE', 'AMOSTRAGEM 01', 'BR-3145NA-01', '104', 'EMBARQUEAMOSTRAGEM 01BR-3145NA-01BRITADORMARTELOS');
INSERT INTO `subconjunto` VALUES ('277', 'TAMBOR DE RETORNO\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'AMOSTRAGEM 02', 'TR-3145NA-02', '105', 'EMBARQUEAMOSTRAGEM 02TR-3145NA-02TRANSPORTADORACIONAMENTO');
INSERT INTO `subconjunto` VALUES ('278', 'ACIONAMENTO\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'AMOSTRAGEM 02', 'TR-3145NA-02', '105', 'EMBARQUEAMOSTRAGEM 02TR-3145NA-02TRANSPORTADORTAMBOR DE ACIONAMENTO');
INSERT INTO `subconjunto` VALUES ('279', 'TAMBOR DE ACIONAMENTO\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'AMOSTRAGEM 02', 'TR-3145NA-02', '105', 'EMBARQUEAMOSTRAGEM 02TR-3145NA-02TRANSPORTADORTAMBOR DE RETORNO');
INSERT INTO `subconjunto` VALUES ('280', 'TAMBOR DE RETORNO\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'AMOSTRAGEM 02', 'TR-3145NA-04', '106', 'EMBARQUEAMOSTRAGEM 02TR-3145NA-04TRANSPORTADORACIONAMENTO');
INSERT INTO `subconjunto` VALUES ('281', 'ACIONAMENTO\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'AMOSTRAGEM 02', 'TR-3145NA-04', '106', 'EMBARQUEAMOSTRAGEM 02TR-3145NA-04TRANSPORTADORTAMBOR DE ACIONAMENTO');
INSERT INTO `subconjunto` VALUES ('282', 'MARTELOS\r\n', 'TRANSPORTADOR', 'EMBARQUE', 'AMOSTRAGEM 02', 'TR-3145NA-04', '106', 'EMBARQUEAMOSTRAGEM 02TR-3145NA-04TRANSPORTADORTAMBOR DE RETORNO');
