<?php
require_once 'models/Area.php';

class AreaDaoSql implements AreaDao{
    private $pdo;
    public function __construct(PDO $driver)
    {
        $this->pdo=$driver;
    }
    public function add(Area $a){
        $sql=$this->pdo->prepare("INSERT INTO area(area_nome,id_circuito)VALUES(:area_nome,:id_circuito)");
        $sql->bindValue(':id',$a->getId());
        $sql->bindValue(':area_nome',$a->getAreaNome());
        $sql->bindValue(':id_circuito',$a->getIdCircuito());
        $sql->execute();

        $a->setId( $this->pdo->lastInsertId() );
        return $a;
    }
    public function update(Area $a){
        $sql=$this->pdo->prepare("UPDATE circuito SET area_nome=:area_nome,id_circuito=:id_circuito WHERE id=:id");
        $sql->bindValue(':id',$a->getId());
        $sql->bindValue(':area_nome',$a->getAreaNome());
        $sql->bindValue(':id_circuito',$a->getIdCircuito());
        $sql->execute();

        return true;
    }
    public function delete($id){
        $sql=$this->pdo->prepare("DELETE FROM area WHERE id=:id");
        $sql->bindValue(':id',$id);
        $sql->execute();
    }
    public function findAll(){
        $array=[];
        $sql=$this->pdo->query("SELECT * FROM area");
        if($sql->rowCount() > 0){
            $data=$sql->fetchAll();

            foreach($data as $item){
               $a = new Area();
               $a->setId($item['id']);
               $a->setAreaNome($item['area_nome']);
               $a->setIdCircuito($item['id_circuito']);

                $array[] =$a;
            }
        }
        return $array;
    }
    public function findById($id){
        $sql=$this->pdo->prepare("SELECT * FROM area WHERE id =:id");
        $sql->bindValue(':id',$id);
        $sql->execute();
        if($sql->rowCount() > 0){
            $data=$sql->fetch();

            $a = new Area();
            $a->setId($data['id']);
            $a->setAreaNome($data['circ_nome']);
            $a->setIdCircuito($data['id_circuito']);

            return $a;
        }else{
            return false;
        }
    }
    public function findByAreaNome($area_nome){
        $sql=$this->pdo->prepare("SELECT * FROM area WHERE area_nome =:area_nome");
        $sql->bindValue(':area_nome',$area_nome);
        $sql->execute();
        if($sql->rowCount() > 0){
            $data=$sql->fetch();

            $a = new Area();
            $a->setId($data['id']);
            $a->setAreaNome($data['area_nome']);
            $a->setIdCircuito($data['id_circuito']);

            return $a;
        }else{
            return false;
        }
    }
    public function findByCircuito($Circuito){

    }
}
?>