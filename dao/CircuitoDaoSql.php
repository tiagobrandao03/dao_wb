<?php
// PATH.
require_once "models/Circuito.php";

class CircuitoDaoSql implements CircuitoDao{
    private $pdo;
    public function __construct(PDO $driver)
    {
        $this->pdo=$driver;
    }
    //,WHERE area(SELECT circuito.id_local, circuito.circuito, local.local, local.id_circuito FROM local
    //INNER JOIN circuito ON local.id_circuito = circuito.id_local)
    public function add(Circuito $c){
        $sql=$this->pdo->prepare("INSERT INTO circuito(circ_nome,id_local)VALUES(:circ_nome,:id_local) ");
        $sql->bindValue(':id',$c->getId());
        $sql->bindValue(':circ_nome',$c->getCircuitoNome());
        $sql->bindValue(':id_local',$c->getIdLocal());
        $sql->execute();

        $c->setId( $this->pdo->lastInsertId() );
        return $c;
    }
    public function update(Circuito $c){
        $sql=$this->pdo->prepare("UPDATE circuito SET circ_nome=:circ_nome,id_local=:id_local WHERE id=:id");
        $sql->bindValue(':id',$c->getId());
        $sql->bindValue(':circ_nome',$c->getCircuitoNome());
        $sql->bindValue(':id_local',$c->getIdLocal());
        $sql->execute();

        return true;
    }
    public function delete($id){
        $sql=$this->pdo->prepare("DELETE FROM circuito WHERE id=:id");
        $sql->bindValue(':id',$id);
        $sql->execute();
    }
    public function findAll(){
        $array=[];
        $sql=$this->pdo->query("SELECT * FROM circuito");
        if($sql->rowCount() > 0){
            $data=$sql->fetchAll();

            foreach($data as $item){
               $c = new Circuito();
               $c->setId($item['id']);
               $c->setCircuitoNome($item['circ_nome']);
               $c->setIdLocal($item['id_local']);

                $array[] =$c;
            }
        }
        return $array;
    }
    public function findById($id){
        $sql=$this->pdo->prepare("SELECT * FROM circuito WHERE id =:id");
        $sql->bindValue(':id',$id);
        $sql->execute();
        if($sql->rowCount() > 0){
            $data=$sql->fetch();

            $c = new Circuito();
            $c->setId($data['id']);
            $c->setCircuitoNome($data['circ_nome']);
            $c->setIdLocal($data['id_local']);

            return $c;
        }else{
            return false;
        }
    }
    public function findByCircuitoNome($circ_nome){
        $sql=$this->pdo->prepare("SELECT * FROM circuito WHERE circ_nome =:circ_nome");
        $sql->bindValue(':circ_nome',$circ_nome);
        $sql->execute();
        if($sql->rowCount() > 0){
            $data=$sql->fetch();

            $c = new Circuito();
            $c->setId($data['id']);
            $c->setCircuitoNome($data['circuito']);
            $c->setIdLocal($data['id_local']);

            return $c;
        }else{
            return false;
        }
    }
}
?>