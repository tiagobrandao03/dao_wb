<?php

require_once 'models/Componente.php';

class ComponenteDaoMySql implements ComponenteDao{
    private $pdo;
    public function __construct(PDO $driver)
    {
        $this->pdo=$driver;
    }
    public function add(Componente $cm){
        $sql=$this->pdo->prepare("INSERT INTO componente(componente,id_subconjunto)VALUES(:componente,:id_subconjunto)");
        $sql->bindValue(':id',$cm->getId());
        $sql->bindValue(':componente',$cm->getComponente());
        $sql->bindValue(':id_subconjunto',$cm->getIdSubconjunto());
        $sql->execute();

        $cm->setId( $this->pdo->lastInsertId() );
        return $cm;
    }
    public function update(Componente $cm){
        $sql=$this->pdo->prepare("UPDATE componente SET componente=:componente,id_subconjunto=:id_subconjunto WHERE id=:id");
        $sql->bindValue(':id',$cm->getId());
        $sql->bindValue(':componente',$cm->getComponente());
        $sql->bindValue(':id_subconjunto',$cm->getIdSubconjunto());
        $sql->execute();

        return true;
    }
    public function delete($id){
        $sql=$this->pdo->prepare("DELETE FROM componente WHERE id=:id");
        $sql->bindValue(':id',$id);
        $sql->execute();
    }
    public function findAll(){
        $array=[];
        $sql=$this->pdo->query("SELECT * FROM componente");
        if($sql->rowCount() > 0){
            $data=$sql->fetchAll();

            foreach($data as $item){
               $e = new componente();
               $e->setId($item['id']);
               $e->setComponente($item['componente']);
               $e->setIdSubconjunto($item['id_subconjunto']);

                $array[] =$e;
            }
        }
        return $array;
    }
    public function findById($id){
        $sql=$this->pdo->prepare("SELECT * FROM componente WHERE id =:id");
        $sql->bindValue(':id',$id);
        $sql->execute();
        if($sql->rowCount() > 0){
            $data=$sql->fetch();

            $c = new Componente();
            $c->setId($data['id']);
            $c->setComponente($data['componente']);
            $c->setIdSubconjunto($data['id_subconjunto']);

            return $c;
        }else{
            return false;
        }
    }
    public function findByComponente($Componente){

    }
    public function findBySubconjunto($Subconjunto){

    }

}
?>
