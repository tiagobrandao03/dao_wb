<?php

require_once 'models/Conjunto.php';

class ConjuntoDaoMySql implements ConjuntoDao{

    private $pdo;

    public function __construct(PDO $driver)
    {
        $this->pdo=$driver;
    }

    public function add(Conjunto $co){
        $sql=$this->pdo->prepare("INSERT INTO conjunto(conj_nome,id_equip)VALUES(:conj_nome,:id_equip)");
        $sql->bindValue(':id',$co->getId());
        $sql->bindValue(':conj_nome',$co->getConjNome());
        $sql->bindValue(':id_equip',$co->getIdEquip());
        $sql->execute();

        $co->setId( $this->pdo->lastInsertId() );
        return $co;
    }
    public function update(Conjunto $co){
        $sql=$this->pdo->prepare("UPDATE conjunto SET conj_nome=:conj_nome,id_equip=:id_equip WHERE id=:id");
        $sql->bindValue(':id',$co->getId());
        $sql->bindValue(':conj_nome',$co->getConjNome());
        $sql->bindValue(':id_equip',$co->getIdEquip());
        $sql->execute();

        return true;
    }
    public function delete($id){
        $sql=$this->pdo->prepare("DELETE FROM conjunto WHERE id=:id");
        $sql->bindValue(':id',$id);
        $sql->execute();
    }
    public function findAll(){
        $array=[];
        $sql=$this->pdo->query("SELECT * FROM conjunto");
        If($sql->rowCount()>0){
            $data=$sql->fetchAll();

            foreach($data as $item){
                $co=new Conjunto();
                $co->setId($item['id']);
                $co->setConjNome($item['conj_nome']);
                $co->setIdEquip($item['id_equip']);

                $array[]=$co;
            }
        }
    }
    public function findById($id){
        $sql=$this->pdo->prepare("SELECT * FROM circuito WHERE id =:id");
        $sql->bindValue(':id',$id);
        $sql->execute();
        if($sql->rowCount() > 0){
            $data=$sql->fetch();

           $co = new Conjunto();
           $co->setId($data['id']);
           $co->setConjNome($data['conj_nome']);
           $co->setIdEquip($data['id_equip']);

            return $co;
        }else{
            return false;
        }
    }
    public function findByConjNome($conj_nome){
        $sql=$this->pdo->prepare("SELECT * FROM conjunto WHERE conj_nome =:conj_nome");
        $sql->bindValue(':conj_nome',$conj_nome);
        $sql->execute();
        if($sql->rowCount() > 0){
            $data=$sql->fetch();

            $co = new Conjunto();
            $co->setId($data['id']);
            $co->setConjNome($data['conj_nome']);
            $co->setIdEquip($data['id_equip']);

            return $co;
        }else{
            return false;
        }
    }
}
?>