<?php
require_once 'models/Equipamento.php';

class EquipamentoDaoMysql implements EquipamentoDao{
    private $pdo;
    public function __construct(PDO $driver)
    {
        $this->pdo=$driver;
    }
    public function add(Equipamento $e){
        $sql=$this->pdo->prepare("INSERT INTO equipamento(equip_nome,id_area)VALUES(:equip_nome,:id_area)");
        $sql->bindValue(':id',$e->getId());
        $sql->bindValue(':equip_nome',$e->getEquipNome());
        $sql->bindValue(':id_area',$e->getIdArea());
        $sql->execute();

        $e->setId( $this->pdo->lastInsertId() );
        return $e;
    }
    public function update(Equipamento $e){
        $sql=$this->pdo->prepare("UPDATE equipamento SET equip_nome=:equip_nome,id_area=:id_area WHERE id=:id");
        $sql->bindValue(':id',$e->getId());
        $sql->bindValue(':equip_nome',$e->getEquipNome());
        $sql->bindValue(':id_area',$e->getIdArea());
        $sql->execute();

        return true;
    }
    public function delete($id){
        $sql=$this->pdo->prepare("DELETE FROM equipamento WHERE id=:id");
        $sql->bindValue(':id',$id);
        $sql->execute();
    }
    public function findAll(){
        $array=[];
        $sql=$this->pdo->query("SELECT * FROM equipamento");
        if($sql->rowCount() > 0){
            $data=$sql->fetchAll();

            foreach($data as $item){
               $e = new Equipamento();
               $e->setId($item['id']);
               $e->setEquipNome($item['equip_nome']);
               $e->setIdArea($item['id_area']);

                $array[] =$e;
            }
        }
        return $array;
    }
    public function findById($id){
        $sql=$this->pdo->prepare("SELECT * FROM equipamento WHERE id =:id");
        $sql->bindValue(':id',$id);
        $sql->execute();
        if($sql->rowCount() > 0){
            $data=$sql->fetch();

            $c = new Equipamento();
            $c->setId($data['id']);
            $c->setEquipNome($data['equip_nome']);
            $c->setIdArea($data['id_area']);

            return $c;
        }else{
            return false;
        }
    }
    public function findByEquipamentoNome($equip_nome){
        $sql=$this->pdo->prepare("SELECT * FROM circuito WHERE equip_nome =:equip_nome");
        $sql->bindValue(':equip_nome',$equip_nome);
        $sql->execute();
        if($sql->rowCount() > 0){
            $data=$sql->fetch();

            $e = new Equipamento();
            $e->setId($data['id']);
            $e->setEquipNome($data['equip_nome']);
            $e->setIdArea($data['id_area']);

            return $e;
        }else{
            return false;
        }
    }
} 
?>