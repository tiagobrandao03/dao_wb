<?php

require_once 'models/Subconjunto.php';

class SubconjuntoDaoMySql implements SubconjuntoDao{
    private $pdo;
    public function __construct(PDO $driver)
    {
        $this->pdo=$driver;
    }
    public function add(Subconjunto $sb){
        $sql=$this->pdo->prepare("INSERT INTO subconjunto(subconjunto,id_local,circuito,area,equipamento,compenente)VALUES(:subconjunto,:id_local,:circuito,:area,:equipamento,:compenente)");
        $sql->bindValue(':id',$sb->getId());
        $sql->bindValue(':subconjunto',$sb->getSubconjunto());
        $sql->bindValue(':id_conjunto',$sb->getIdConjunto());
        $sql->bindValue(':circuito',$sb->getCircuito());
        $sql->bindValue(':area',$sb->getArea());
        $sql->bindValue(':equipamento',$sb->getEquipamento());
        $sql->bindValue(':compenente',$sb->getComponente());
        $sql->execute();

        $sb->setId( $this->pdo->lastInsertId() );
        return $sb;
    }
    public function update(Subconjunto $sb){
        $sql=$this->pdo->prepare("UPDATE subconjunto SET subconjunto=:subconjunto,id_conjunto=:id_conjunto,circuito=:circuito,area=:area,equipamento=:equipamento,componente=:componente' WHERE id=:id");
        $sql->bindValue(':id',$sb->getId());
        $sql->bindValue(':subconjunto',$sb->getSubconjunto());
        $sql->bindValue(':circuito',$sb->getCircuito());
        $sql->bindValue(':area',$sb->getArea());
        $sql->bindValue(':equipamento',$sb->getEquipamento());
        $sql->bindValue(':componente',$sb->getComponente());
        $sql->execute();

        return true;
    }
    public function delete($id){
        $sql=$this->pdo->prepare("DELETE FROM subconjunto WHERE id=:id");
        $sql->bindValue(':id',$id);
        $sql->execute();
    }
    public function findAll(){
        $array=[];
        $sql=$this->pdo->query("SELECT * FROM subconjunto");
        if($sql->rowCount() > 0){
            $data=$sql->fetchAll();

            foreach($data as $item){
               $sb = new Subconjunto();
               $sb->setId($item['id']);
               $sb->setSubconjunto($item['subconjunto']);
               $sb->setIdConjunto($item['id_conjunto']);
               $sb->setCircuito($item['circuito']);
               $sb->setArea($item['area']);
               $sb->setEquipamento($item['equipamento']);
               $sb->setComponente($item['componente']);

                $array[] =$sb;
            }
        }
        return $array;
    }
    public function findById($id){
        $sql=$this->pdo->prepare("SELECT * FROM subconjunto WHERE id =:id");
        $sql->bindValue(':id',$id);
        $sql->execute();
        if($sql->rowCount() > 0){
            $data=$sql->fetch();

            $sb = new Subconjunto();
            $sb->setId($data['id']);
            $sb->setSubconjunto($data['subconjunto']);
            $sb->setIdConjunto($data['id_conjunto']);
            $sb->setCircuito($data['circuito']);
            $sb->setArea($data['area']);
            $sb->setEquipamento($data['equipamento']);
            $sb->setComponente($data['componente']);

            return $sb;
        }else{
            return false;
        }
    }
    public function findBySubconjunto($subconjunto){
         $sql=$this->pdo->prepare("SELECT * FROM subconjunto WHERE subconjunto =:subconjunto");
        $sql->bindValue(':subconjunto',$subconjunto);
        $sql->execute();
        if($sql->rowCount() > 0){
            $data=$sql->fetch();

            $sb = new Subconjunto();
            $sb->setId($data['id']);
            $sb->setSubconjunto($data['subconjunto']);
            $sb->setIdConjunto($data['id_conjunto']);
            $sb->setCircuito($data['circuito']);
            $sb->setArea($data['area']);
            $sb->setEquipamento($data['equipamento']);
            $sb->setComponente($data['componente']);

            return $sb;
        }else{
            return false;
        }
    }
    public function findByCircuito($circuito){
        $sql=$this->pdo->prepare("SELECT * FROM subconjunto WHERE circuito =:circuito");
        $sql->bindValue(':circuito',$circuito);
        $sql->execute();
        if($sql->rowCount() > 0){
            $data=$sql->fetch();

            $sb = new Subconjunto();
            $sb->setId($data['id']);
            $sb->setSubconjunto($data['circuito']);
            $sb->setIdConjunto($data['id_conjunto']);
            $sb->setCircuito($data['circuito']);
            $sb->setArea($data['area']);
            $sb->setEquipamento($data['equipamento']);
            $sb->setComponente($data['componente']);

            return $sb;
        }else{
            return false;
        }
    }
    public function findByArea($area){
        $sql=$this->pdo->prepare("SELECT * FROM subconjunto WHERE area =:area");
        $sql->bindValue(':area',$area);
        $sql->execute();
        if($sql->rowCount() > 0){
            $data=$sql->fetch();

            $sb = new Subconjunto();
            $sb->setId($data['id']);
            $sb->setSubconjunto($data['circuito']);
            $sb->setIdConjunto($data['id_conjunto']);
            $sb->setCircuito($data['circuito']);
            $sb->setArea($data['area']);
            $sb->setEquipamento($data['equipamento']);
            $sb->setComponente($data['componente']);

            return $sb;
        }else{
            return false;
        }
    }
    public function findByEquipamento($equipamento){
        $sql=$this->pdo->prepare("SELECT * FROM subconjunto WHERE equipamento =:equipamento");
        $sql->bindValue(':equipamento',$equipamento);
        $sql->execute();
        if($sql->rowCount() > 0){
            $data=$sql->fetch();

            $sb = new Subconjunto();
            $sb->setId($data['id']);
            $sb->setSubconjunto($data['circuito']);
            $sb->setIdConjunto($data['id_conjunto']);
            $sb->setCircuito($data['circuito']);
            $sb->setArea($data['area']);
            $sb->setEquipamento($data['equipamento']);
            $sb->setComponente($data['componente']);

            return $sb;
        }else{
            return false;
        }
    }
    public function findByComponente($componente){

    }
}
?>