<?php

include 'header.php';
require 'config.php';
require_once 'dao/TblocalDaoMySql.php';
require_once 'dao/CircuitoDaoSql.php';
require_once 'dao/AreaDaoSql.php';
require_once 'dao/EquipamentoMySql.php';
require_once 'dao/ConjuntoDaoMySql.php';
require_once 'dao/SubconjuntoMySql.php';
require_once 'dao/ComponenteMySql.php';


$tblocalDao= new TblocalDaoMysql($pdo);

$info=false;
$id = filter_input(INPUT_GET,'id');
if($id){
    $tblocal=$tblocalDao->findById($id);
}
if($tblocal === false){
    header("Location: index.php");
    exit;
}

$areaDao= new AreaDaoSql($pdo);

$info=false;
$id = filter_input(INPUT_GET,'id');
if($id){
    $tblocal=$tblocalDao->findById($id);
}
if($tblocal === false){
    header("Location: index.php");
    exit;
}
?>

<main class="white">
<section style="width:900px;margin:10px auto;">
<div class="row">
    <div class="col">
    <h1>EDITAR LOCAL</h1>
    </div>
    </div>
    <div class="container">
        <div class="row">
            <form method="POST" action="editar_action.php?2">
            <input type="text" name="id" value="<?=$tblocal->getId();?>"/>
                <label >
                    LOCAL: </br>
                    <input type="text" name="nome" value="<?=$tblocal->getNome();?>"/>
                </label><br/><br/>
                <input class="btn" type="submit" value="Salvar"/>
            </form>
        </div>
    </div>
</section>
</main>
<?php
include 'footer.php';
?>