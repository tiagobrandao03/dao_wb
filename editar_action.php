<?php
require_once 'config.php';
require_once 'dao/TblocalDaoMySql.php';
require_once 'dao/CircuitoDaoSql.php';
require_once 'dao/AreaDaoSql.php';
require_once 'dao/EquipamentoMySql.php';
require_once 'dao/ConjuntoDaoMySql.php';
require_once 'dao/SubconjuntoMySql.php';
require_once 'dao/ComponenteMySql.php';

$tblocalDao= new TblocalDaoMySql($pdo);
$circuitoDao= new CircuitoDaoSql($pdo);
$areaDao= new AreaDaoSql($pdo);
$equipamentoDao= new EquipamentoDaoMySql($pdo);
$conjuntoDao= new ConjuntoDaoMySql($pdo);
$subconjuntoDao= new SubconjuntoDaoMySql($pdo);
$componenteDao= new ComponenteDaoMySql($pdo);

$id= filter_input(INPUT_POST,'id');
$area_nome = filter_input(INPUT_POST, 'nome');

if($id && $nome){
    $tblocal=$tblocalDao->findById($id);
    $tblocal->setNome($nome);


    $tblocalDao->update($tblocal);


    header("Location: index.php");
    exit;
}else{
    header("Location: editar.php?id=".$id);
    exit;
}

// circuito
$id= filter_input(INPUT_POST,'id');
$circ_nome = filter_input(INPUT_POST, 'nome');
$id_local= filter_input(INPUT_POST,'id_local');

if($id && $nome && $id_local){
    $circuito=$circuitoDao->findById($id);
    $circuito->setCircuitoNome($circuito);
    


    $circuitoDao->update($circuito);


    header("Location: index.php");
    exit;
}else{
    header("Location: editar.php?id=".$id);
    exit;
}
//area
$id= filter_input(INPUT_POST,'id');
$area_nome = filter_input(INPUT_POST, 'area_nome');

if($id && $area_nome){
    $area=$areaDao->findById($id);
    $area->setAreaNome($area_nome);


    $areaDao->update($area);


    header("Location: index.php");
    exit;
}else{
    header("Location: editar.php?id=".$id);
    exit;
}
//equipamento
$id= filter_input(INPUT_POST,'id');
$equip_nome = filter_input(INPUT_POST, 'equip_nome');

if($id && $equip_nome){
    $equipamento=$equipamentoDao->findById($id);
    $equipamento->setEquipNome($equip_nome);


    $equipamentoDao->update($equipamento);


    header("Location: index.php");
    exit;
}else{
    header("Location: editar.php?id=".$id);
    exit;
}
// conjunto
$id= filter_input(INPUT_POST,'id');
$conj_nome = filter_input(INPUT_POST, 'conj_nome');

if($id && $conj_nome){
    $conjunto=$conjuntoDao->findById($id);
    $conjunto->setConjNome($conj_nome);
    $conjunto->setIdEquip($conj_nome);

    $conjuntoDao->update($conjunto);


    header("Location: index.php");
    exit;
}else{
    header("Location: editar.php?id=".$id);
    exit;
}
//subconjunto

$id= filter_input(INPUT_POST,'id');
$subconjunto = filter_input(INPUT_POST, 'subconjunto');

if($id && $subconjunto){
    $subconjunto=$subconjuntoDao->findById($id);
    $subconjunto->setSubconjunto($subconjunto);


    $subconjuntoDao->update($subconjunto);


    header("Location: index.php");
    exit;
}else{
    header("Location: editar.php?id=".$id);
    exit;
}
// }
// $id= filter_input(INPUT_POST,'id');
// $nome = filter_input(INPUT_POST, 'nome');

// if($id && $nome){
//     $tblocal=$tblocalDao->findById($id);
//     $tblocal->setNome($nome);


//     $tblocalDao->update($tblocal);


//     header("Location: index.php");
//     exit;
// }else{
//     header("Location: editar.php?id=".$id);
//     exit;
// }
//componente


