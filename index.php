<?php
    require_once 'config.php';
    require_once 'dao/TblocalDaoMySql.php';
    require_once 'dao/CircuitoDaoSql.php';
    require_once 'dao/AreaDaoSql.php';
    require_once 'dao/EquipamentoDaoMySql.php';

    require_once 'dao/SubconjuntoDaoMySql.php';
    require_once 'dao/ComponenteDaoMySql.php';
    include 'header.php';

    $tblocalDao=new TblocalDaoMysql($pdo);
    $lista=$tblocalDao->findAll();
    $circuitoDao=new CircuitoDaoSql($pdo);
    $listaCircuito=$circuitoDao->findAll();
    $areaDao=new AreaDaoSql($pdo);
    $listaArea=$areaDao->findAll();
    $equipamentoDao=new EquipamentoDaoMySql($pdo);
    $listaEquip= $equipamentoDao->findAll();

    $subconjuntoDao=new SubconjuntoDaoMySql($pdo);
    $listaSubconj= $subconjuntoDao->findAll();
    $componenteDao=new ComponenteDaoMySql($pdo);
    $listaComponente= $componenteDao->findAll();

?>
<main>
    <section class="width:900px;">
        <div class="container" style="width:100%!important;min-width: 1200px">
            <div class="row"></div>
            <div class="container">
                <div class="row" style="margin-top: 40px;">
                    <div class="col s12 m10">
                        <a href="adicionar.php" class="btn">ADICIONAR Dados</a>
                        <!-- <a href="circuito/adicionar.php" class="btn">ADICIONAR CIRCUITOS</a> -->
                    </div>
                </div>
            </div>
            </div>
            <div class="container">
                <div style="">
                    <div class="row" style="">
                        <div class="col s12 m8" >
                        <hr style="background-color:rgb(230, 230, 230);">
                        </div>
                    </div>
                </div>
            <br>
            <!-- local -->
            <div class="row">
                <div class="col s12 m10 z-depth-2">
                    <table border="1" width="100%">
                        <tr>
                            <th>ID</th>
                            <th>LOCAL</th>
                            <th>AÇÔES</th>
                        </tr>
                        <?php
                    foreach($lista as $tblocalDao):?>
                        <tr>
                            <td><?=$tblocalDao->getId();?></td>
                            <td><?=$tblocalDao->getNome();?></td>
                           
                            <td>
                                <a href="editar.php?id=<?=$tblocalDao->getId();?>">[Editar]</a>
                                <a href="excluir.php?id=<?=$tblocalDao->getId();?>" onclick="return confirm('Tem certeza que deseja excluir?')">[Excluir]</a>
                                </td>
                        </tr>
                    <?php endforeach
                        ?>
                    </table>
                </div>
            </div>
        <br>
        <!-- circuito -->
        <div class="row">
                <div class="col s12 m10 z-depth-2">
                    <table border="1" width="100%">
                        <tr>
                            <th>ID</th>
                            <th>CIRCUITO</th>
                            <th>ID LOCAL</th>
                            <th>AÇÔES</th>
                        </tr>
                        <?php
                    foreach($listaCircuito as $circuitoDao):?>
                        <tr>
                            <td><?=$circuitoDao->getId();?></td>
                            <td><?=$circuitoDao->getCircuitoNome();?></td>
                            <td><?=$circuitoDao->getIdLocal();?></td>
                            <td>
                                <a href="editar.php?id=<?=$circuitoDao->getId();?>">[Editar]</a>
                                <a href="excluir_c.php?id=<?=$circuitoDao->getId();?>" onclick="return confirm('Tem certeza que deseja excluir?')">[Excluir]</a>
                                </td>
                        </tr>
                    <?php endforeach
                        ?>
                    </table>
                </div>
            </div>
            <!-- area -->
            <div class="row">
                <div class="col s12 m10 z-depth-2">
                    <table border="1" width="100%">
                        <tr>
                            <th>ID</th>
                            <th>AREA</th>
                            <th>ID CIRCUITO</th>
                            <th>AÇÔES</th>
                        </tr>
                        <?php
                    foreach($listaArea as $areaDao):?>
                        <tr>
                            <td><?=$areaDao->getId();?></td>
                            <td><?=$areaDao->getAreaNome();?></td>
                            <td><?=$areaDao->getIdCircuito();?></td>
                            <td>
                                <a href="editar.php?id=<?=$areaDao->getId();?>">[Editar]</a>
                                <a href="excluir_a.php?id=<?=$areaDao->getId();?>" onclick="return confirm('Tem certeza que deseja excluir?')">[Excluir]</a>
                                </td>
                        </tr>
                    <?php endforeach
                        ?>
                    </table>
                </div>
            </div>
            <!-- equipamento -->
            <div class="row">
                <div class="col s12 m10 z-depth-2">
                    <table border="1" width="100%">
                        <tr>
                            <th>ID</th>
                            <th>EQUIPAMENTO</th>
                            <th>ID AREA</th>
                            <th>AÇÔES</th>
                        </tr>
                        <?php
                    foreach($listaEquip as $equipamentoDao):?>
                        <tr>
                            <td><?=$equipamentoDao->getId();?></td>
                            <td><?=$equipamentoDao->getEquipNome();?></td>
                            <td><?=$equipamentoDao->getIdArea();?></td>
                            <td>
                                <a href="editar.php?id=<?=$equipamentoDao->getId();?>">[Editar]</a>
                                <a href="excluir_e.php?id=<?=$equipamentoDao->getId();?>" onclick="return confirm('Tem certeza que deseja excluir?')">[Excluir]</a>
                                </td>
                        </tr>
                    <?php endforeach
                        ?>
                    </table>
                </div>
            </div>
            <!-- conjunto  -->
            
            <!-- componente -->
            <div class="row">
                <div class="col s12 m10 z-depth-2">
                    <table border="1" width="100%">
                        <tr>
                            <th>ID</th>
                            <th>COMPONENTE</th>
                            <th>ID SUBCONJUNTO</th>
                            <th>AÇÔES</th>
                        </tr>
                        <?php
                    foreach($listaCircuito as $circ_nome):?>
                        <tr>
                            <td><?=$circ_nome->getId();?></td>
                            <td><?=$circ_nome->getComponente();?></td>
                            <td><?=$circ_nome->getIdSubconjunto();?></td>
                            <td>
                                <a href="editar.php?id=<?=$circ_nome->getId();?>">[Editar]</a>
                                <a href="excluir_cm.php?id=<?=$circ_nome->getId();?>" onclick="return confirm('Tem certeza que deseja excluir?')">[Excluir]</a>
                                </td>
                        </tr>
                    <?php endforeach
                        ?>
                    </table>
                </div>
            </div>
            <!-- subconjuntoe -->
            <div class="row">
                <div class="col s12 m10 z-depth-2">
                    <table border="1" width="100%">
                        <tr>
                            <th>ID</th>
                            <th>SUBCONJUNTO</th>
                            <th>ID </th>
                            <th>AÇÔES</th>
                        </tr>
                        <?php
                    foreach($listaSubconj as $subconjuntoDao):?>
                        <tr>
                            <td><?=$subconjuntoDao->getId();?></td>
                            <td><?=$subconjuntoDao->getSubconjNome();?></td>
                            <td><?=$subconjuntoDao->getIdConjunto();?></td>
                            <td>
                                <a href="editar.php?id=<?=$subconjuntoDao->getId();?>">[Editar]</a>
                                <a href="excluir_sb.php?id=<?=$subconjuntoDao->getId();?>" onclick="return confirm('Tem certeza que deseja excluir?')">[Excluir]</a>
                                </td>
                        </tr>
                    <?php endforeach
                        ?>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col s12 m10">
                <hr style="background-color:rgb(230, 230, 230);">
                </div>
            </div>
            </div>
     
    </section>
       <!-- z-depth-2 fa fa-reply-->
</main>
<?php
    include PATH.'/footer.php';
?>