<?php

class Area
{
    
    private $id;
    private $area_nome;
    private $id_circuito;

    public function getId(){
        return $this->id;
    }
    public function setId($i){
        $this->id=trim($i);
    }
    public function getAreaNome(){
        return $this->area_nome;
    }
    public function setAreaNome($i){
        $this->area_nome=trim($i);
    }
    public function getIdCircuito(){
        return $this->id_circuito;
    }
    public function setIdCircuito($i){
        $this->id_circuito=trim($i);
    }
}
interface AreaDao{
    public function add(Area $a);
    public function update(Area $a);
    public function delete($id);
    public function findAll();
    public function findById($id);
    public function findByAreaNome($area_nome);
    public function findByCircuito($Circuito);
}
?>