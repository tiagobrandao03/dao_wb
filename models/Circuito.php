<?php

class Circuito
{
    
    private $id;
    private $circ_nome;
    private $id_local;

    public function getId(){
        return $this->id;
    }
    public function setId($i){
        $this->id=trim($i);
    }
    public function getCircuitoNome(){
        return $this->circ_nome;
    }
    public function setCircuitoNome($i){
        $this->circ_nome=trim($i);
    }
    public function getIdLocal(){
        return $this->id_local;
    }
    public function setIdLocal($i){
        $this->id_local=trim($i);
    }
}
interface CircuitoDao{
    public function add(Circuito $c);
    public function update(Circuito $c);
    public function delete($id);
    public function findAll();
    public function findById($id);
    public function findByCircuitoNome($circ_nome);
}
?>