<?php

class Componente{
    private $id;
    private $componente;
    private $id_subconjunto;

    public function getId(){
        return $this->id;
    }
    public function setId($i){
        $this->id=trim($i);
    }
    public function getComponente(){
        return $this->componente;
    }
    public function setComponente($i){
        $this->componente=trim($i);
    }
    public function getIdSubconjunto(){
        return $this->id_subconjunto;
    }
    public function setIdSubconjunto($i){
        $this->id_subconjunto=trim($i);
    }
}
interface ComponenteDao{
    public function add(Componente $cm);
    public function update(Componente $cm);
    public function delete($id);
    public function findAll();
    public function findById($id);
    public function findByComponente($Componente);
    public function findBySubconjunto($Subconjunto);
}


?>
