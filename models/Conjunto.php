<?php

class Conjunto
{
    
    private $id;
    private $conj_nome;
    private $id_equip;

    public function getId(){
        return $this->id;
    }
    public function setId($i){
        $this->id=trim($i);
    }
    public function getConjNome(){
        return $this->conj_nome;
    }
    public function setConjNome($i){
        $this->conj_nome=trim($i);
    }
    public function getIdEquip(){
        return $this->id_equip;
    }
    public function setIdEquip($i){
        $this->id_equip=trim($i);
    }
}
interface ConjuntoDao{
    public function add(Conjunto $co);
    public function update(Conjunto $co);
    public function delete($id);
    public function findAll();
    public function findById($id);
    public function findByConjNome($conj_nome);
}
?>