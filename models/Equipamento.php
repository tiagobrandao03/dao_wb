<?php

class Equipamento
{
    
    private $id;
    private $equip_nome;
    private $id_area;

    public function getId(){
        return $this->id;
    }
    public function setId($i){
        $this->id=trim($i);
    }
    public function getEquipNome(){
        return $this->equip_nome;
    }
    public function setEquipNome($i){
        $this->equip_nome=trim($i);
    }
    public function getIdArea(){
        return $this->id_area;
    }
    public function setIdArea($i){
        $this->id_area=trim($i);
    }
}
interface EquipamentoDao{
    public function add(Equipamento $e);
    public function update(Equipamento $e);
    public function delete($id);
    public function findAll();
    public function findById($id);
    public function findByEquipamentoNome($circ_nome);
}
?>