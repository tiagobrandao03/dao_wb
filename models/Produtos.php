<?php



class Produtos
{
    
    private $id;
    private $nome;
    private $quantidade;
    private $valor;
    private $campo_categoria;

    public function getId(){
        return $this->id;
    }
    public function setId($i){
        $this->id=trim($i);
    }
    public function getNome(){
        return $this->nome;
    }
    public function setNome($n){
        $this->nome=ucwords(trim($n));
    }
    public function getQuantidade(){
        return $this->quantidade;
    }
    public function setQuantidade($i){
        $this->quantidade=trim($i);
    }
    public function getValor(){
        return $this->valor;
    }
    public function setValor($i){
        $this->valor=trim($i);
    }
    public function getCampoCategoria(){
        return $this->campo_categoria;
    }
    public function setCampoCategoria($n){
        $this->campo_categoria=ucwords(trim($n));
    }
}
interface ProdutosDao{
    public function add(Produtos $p);
    public function update(Produtos $p);
    public function delete($id);
    public function findAll();
    public function findById($id);
    public function findByCampoCategoria($campo_categoria);
  
}
?>

