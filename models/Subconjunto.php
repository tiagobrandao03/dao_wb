<?php

class Subconjunto
{
    
    private $id;
    private $subconjunto;
    private $circuito;
    private $area;
    private $equipamento;
    private $id_conjunto;
    private $componente;

    public function getId(){
        return $this->id;
    }
    public function setId($i){
        $this->id=trim($i);
    }
    public function getSubconjunto(){
        return $this->subconjunto;
    }
    public function setSubconjunto($i){
        $this->subconjunto=trim($i);
    }
    public function getIdConjunto(){
        return $this->id_conjunto;
    }
    public function setIdConjunto($i){
        $this->id_conjunto=trim($i);
    }
    public function getCircuito(){
        return $this->circuito;
    }
    public function setCircuito($i){
        $this->circuito=trim($i);
    }
    public function getArea(){
        return $this->area;
    }
    public function setArea($i){
        $this->area=trim($i);
    }
    public function getEquipamento(){
        return $this->equipamento;
    }
    public function setEquipamento($i){
        $this->equipamento=trim($i);
    }

    public function getComponente(){
        return $this->componente;
    }
    public function setComponente($i){
        $this->componente=trim($i);
    }
}
interface SubconjuntoDao{
    public function add(Subconjunto $sb);
    public function update(Subconjunto $sb);
    public function delete($id);
    public function findAll();
    public function findById($id);
    public function findBySubconjunto($subconjunto);
    public function findByCircuito($Circuito);
    public function findByArea($area);
    public function findByEquipamento($equipamento);
    public function findByComponente($componente);

}

?>